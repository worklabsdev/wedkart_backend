const Joi = require("@hapi/joi");

const { validationHandler } = require("../../utils/responseHandler");
const { requestLogger } = require("../logging/consoleLogger");

let valOptions = {
	allowUnknown: false,
	abortEarly: true
};

// ///       Common Validate JOI Function
exports.joiValidate = (apiRefFull, response, schema, data, opts = {}) => {
	requestLogger(apiRefFull, data, opts);//	Logger For Requests

	return Joi.validate(data, schema, valOptions, (error, value) => {
		if (error) {
			return validationHandler(
				response,
				error.details[0].message,
				{
					apiRefFull
				}
			);
		}
		return true;
	});
};



//  ///       Admin Validator 
exports.joiValidateOpen = (apiRefFull, response, schema, data, opts={}) => {
    
    requestLogger(apiRefFull, data, opts);//	Logger For Requests

    let valOptions = {
        allowUnknown: true,
        abortEarly: true
    }

	return Joi.validate(data, schema, valOptions, (error, value) => {
		if (error) {
			return validationHandler(
				response,
				error.details[0].message,
				{
					apiRefFull
				}
			);
		}
		return true;
	});
};
const Razorpay = require("razorpay");

let razorPayConfigs = appConfig.payment.razorPay;

//console.log(razorConfig);
const razorConfig = {  
	key_id: razorPayConfigs.public,  
	key_secret: razorPayConfigs.secret
};
exports.razorConfig = razorConfig;

var razorInstance = new Razorpay(razorConfig);

//  Create Order
const createRazorPayOrder = (response, options) => {

    return new Promise(async (resolve, reject) => {
		try {
				let order = await razorInstance.orders.create(options);

				return resolve(order);
			} catch (error) {
				return reject(error);
			}
	});

};
exports.createRazorPayOrder = createRazorPayOrder;

//	Get Payment Details
const getPaymentDetails = (paymentId) => {

	return new Promise(async (resolve, reject) => {
		try {
			let payment = await razorInstance.payments.fetch(paymentId);

			return resolve(payment);
		} catch (error) {
			return reject(error);
		}
	});

	// request('https://<YOUR_KEY_ID>:<YOUR_KEY_SECRET>@api.razorpay.com/v1/payments/pay_29QQoUBi66xm2f', function (error, response, body) {
	// 	console.log('Response:', body);
	// });

};
exports.getPaymentDetails = getPaymentDetails;
const logConfig = appConfig.errorSettings.log;

const { currentUTC } = require("../../utils/commonFuns");

const requestLogger = (apiRefFull, data, opts) => {
	if (!logConfig.request) return;

	const utcNow = currentUTC();
	console.log(
		`-------> ${utcNow} - ${apiRefFull}, opts--${JSON.stringify(opts)}, data--${JSON.stringify(data)}`
	);
};
exports.requestLogger = requestLogger;

exports.logger = (apiRefFull, data) => {

	//   var logger = {
	//     log: function(level, message) {
	//         winstonLogger.log(level, formatMessage(message));
	//     },
	//     error: function(message) {
	//         winstonLogger.error(formatMessage(message));
	//     },
	//     warn: function(message) {
	//         winstonLogger.warn(formatMessage(message));
	//     },
	//     verbose: function(message) {
	//         winstonLogger.verbose(formatMessage(message));
	//     },
	//     info: function(message) {
	//         winstonLogger.info(formatMessage(message));
	//     },
	//     debug: function(message) {
	//         winstonLogger.debug(formatMessage(message));
	//     },
	//     silly: function(message) {
	//         winstonLogger.silly(formatMessage(message));
	//     }
	// };
	// module.exports = logger;

	// if(logConfig)

};
// var winston = require("winston");
// require("winston-daily-rotate-file");
// var moment = require("moment");

// const currentDt = moment().format("YYYY-mm-dd");

// var logLevel = 'info';// set default log level.

// // Set up logger
// var colors = {
//   trace: 'white',
//   debug: 'green',
//   info: 'blue',
//   warn: 'yellow',
//   crit: 'red',
//   fatal: 'red'
// };
// const levels = {
//     fatal: 0,
//     crit: 1,
//     warn: 2,
//     info: 3,
//     debug: 4,
//     trace: 5
// };


// const myCustomLevels = {
//     levels,
//     colors
// };

// var transport = new (winston.transports.DailyRotateFile)({
//   filename: 'application-%DATE%.log',
//   datePattern: 'YYYY-MM-DD',
//   zippedArchive: true,
//   maxSize: '20m',
//   maxFiles: '14d'
// });

// var logger = winston.createLogger({
//   levels: myCustomLevels.levels,
//   transports: [
//     transport,
//     new (winston.transports.Console)({
//       colorize: true,
//       timestamp: true
//     }),
//     new (winston.transports.File)({ filename: `${currentDt}.log` })
//   ]
// });

// winston.addColors(colors);

// // Extend logger object to properly log 'Error' types
// var origLog = logger.log

// logger.log = function (level, msg) {
//   if (msg instanceof Error) {
//     var args = Array.prototype.slice.call(arguments);
//     args[1] = msg.stack;
//     origLog.apply(logger, args);
//   } else {
//     origLog.apply(logger, arguments);
//   }
// }


/* LOGGER EXAMPLES
  var log = require('./log.js');
  log.trace('testing');
  log.debug('testing');
  log.info('testing');
  log.warn('testing');
  log.crit('testing');
  log.fatal('testing');
 */

// module.exports.logValidation = (apiRefFull, data) => {

//     try {
//         data = JSON.stringify(data);
//     }
//     catch(exception) {
//     }

//     log.info(apiRefFull, data);
// }

//      Info Loggers       //
const socketInfoLogger = (reference=null, requestId=null, data) => {
	if (!logConfig.socket.info) return;

	console.info(`===SocketINFO===>`, data, `reference = ${reference}`, `requestId = ${requestId}`, "<===SocketINFO===");
};
exports.socketInfoLogger = socketInfoLogger;

const infoLogger = (reference=null, requestId=null, data) => {
	if (!logConfig.info) return;

	console.info(`===INFO===>`, data, "<===Info===", `reference = ${reference}`, `requestId = ${requestId}`);
};
exports.infoLogger = infoLogger;


//      Socket Loggers       //
const socketErrorLogger = (reference=null, requestId=null, error) => {
    if(!logConfig.socket.error) return;

    console.error(`===ERROR===>`,  error, `reference = ${reference}`, `requestId = ${requestId}`, "<===ERROR===");
};
exports.socketErrorLogger = socketErrorLogger;

const errorLogger = (reference=null, requestId=null, error) => {
    if(!logConfig.error) return;

    console.error(`===ERROR===>`,  error, `reference = ${reference}`, `requestId = ${requestId}`, "<===ERROR===");
};
exports.errorLogger = errorLogger;
//      Socket Loggers       //

//      Mail Info Loggers
const mailInfoLogger = (reference=null, requestId=null, data) => {
	if (!logConfig.mail.info) return;

	console.info(
		`===MailInfo===>`,
		`reference = ${reference}`,
		`requestId = ${requestId}`,
		data,
		"<===MailInfo==="
	);
};
exports.mailInfoLogger = mailInfoLogger;

//      Mail Error Loggers
const mailErrorLogger = (reference=null, requestId=null, error=null, data) => {
	if(!logConfig.mail.error) return;

	console.error(
		`===MailError===>`, 
		`reference = ${reference}`,
		`requestId = ${requestId}`,
		error,
		data,
		"<===MailError==="
	);
};
exports.mailErrorLogger = mailErrorLogger;
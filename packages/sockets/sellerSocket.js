const namespace = "/seller";

const { globalSocketEvents, globalSocketResponses } = require("./constantSockets");
const { socketErrorLogger, socketInfoLogger } = require("../logging/consoleLogger");

var sellerMdl = require("../../models/seller");

const sellerNamespace = socketIO.of(namespace);

//    Connection Event Handler
sellerNamespace.on("connection", async (socket) => {
try {

    //  Auth    //
    if(!socket.handshake.query || !socket.handshake.query.token)
      return socket.emit(globalSocketEvents.authErrorEvent, globalSocketResponses.tokenMissing);

    let token = socket.handshake.query.token;

    let seller = await sellerMdl.aggregate([
        { $match: { "logins.token": token, isDeleted: false  } },
        { $unwind: "$logins" },
        { $match: { "logins.token": token  } }
    ]);

    if(!seller.length) {
      return socket.emit(globalSocketEvents.authErrorEvent, globalSocketResponses.invalidToken);
    }

    let detailId = seller[0]._id;
    let detailLoginId = seller[0].logins._id;

    await sellerMdl.update(
      {
        _id: detailId,
        "logins._id": detailLoginId
      },
      {
          //$push: { "logins.$.socketIds": socket.id },
          $set: { 
            "logins.$.socketId": socket.id,
            "logins.$.updatedAt": new Date() 
          }
      },
      { multi: false }
    );

    socketInfoLogger(namespace, null, {
      msg: `Someone connected with Socket Id = ${socket.id}`,
      id: detailId
    });
    //  Auth    //


    //  Disconnect  //
    socket.on("disconnect", async () => {
    try{

        await sellerMdl.update(
          {
            _id: detailId,
            "logins._id": detailLoginId
          },
          {
            //$pull: { "logins.$.socketIds": socket.id },
            $set: { 
              "logins.$.socketId": null,
              "logins.$.updatedAt": new Date() 
            }
          },
          {
            multi: false
          }
        );

        socketInfoLogger(namespace, null, {
            msg: `Someone Disconnect with Socket Id = ${socket.id}`,
            id: detailId
        });

    }
    catch(error) {
        socketErrorLogger(namespace, null, error);
    }
    });
    //  Disconnect  //

  }
  catch(error) {
    socketErrorLogger(namespace, null, error);
  }
});

module.exports.sellerNamespace = sellerNamespace;
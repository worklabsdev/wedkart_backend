module.exports = {
    adminSocket: require("./adminSocket"),
    buyerSocket: require("./buyerSocket"),
    sellerSocket: require("./sellerSocket")
};
const namespace = "/admin";

const { globalSocketEvents, globalSocketResponses } = require("./constantSockets");
const { socketErrorLogger, socketInfoLogger } = require("../logging/consoleLogger");

var adminMdl = require("../../models/admin");

const adminNamespace = socketIO.of(namespace);

//    Connection Event Handler
adminNamespace.on("connection", async (socket) => {
try {

    //  Auth    //
    if(!socket.handshake.query || !socket.handshake.query.token)
      return socket.emit(globalSocketEvents.authErrorEvent, globalSocketResponses.tokenMissing);

    let token = socket.handshake.query.token;

    let admin = await adminMdl.findOne(
        { token: token, isDeleted: false },
        { _id: 1, socketId: 1 }
    ).lean();

    if(!admin) {
        return socket.emit(globalSocketEvents.authErrorEvent, globalSocketResponses.invalidToken);
    }

    await adminMdl.findByIdAndUpdate(
        admin._id,
        {
            $set: { 
                "socketId": socket.id,
                "updatedAt": new Date() 
            }
        }
    );

    socketInfoLogger(namespace, null, {
      msg: `Someone connected with Socket Id = ${socket.id}`,
      id: admin._id
    });
    //  Auth    //


    //  Disconnect  //
    socket.on("disconnect", async () => {
    try{

        await adminMdl.findByIdAndUpdate(
            admin._id,
            {
                $set: { 
                    "socketId": null,
                    "updatedAt": new Date() 
                }
            }
        );

        socketInfoLogger(namespace, null, {
            msg: `Someone Disconnect with Socket Id = ${socket.id}`,
            id: admin._id
        });

    }
    catch(error) {
        socketErrorLogger(namespace, null, error);
    }
    });
    //  Disconnect  //

    //socket.emit(globalSocketEvents.commonEvent, globalSocketResponses.invalidToken);
    //socketIO.emit(globalSocketEvents.commonEvent, {message: "success"});

    // socketIO.of(namespace).to(socket.id).emit(globalSocketEvents.commonEvent,"data");
    // socketIO.of(namespace).emit(globalSocketEvents.commonEvent,"data");

  }
  catch(error) {
    socketErrorLogger(namespace, null, error);
  }
});

module.exports.adminNamespace = adminNamespace;
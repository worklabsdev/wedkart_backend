const { globalSocketNameSpaces } = require("./constantSockets");

const { socketErrorLogger, socketInfoLogger } = require("../logging/consoleLogger");

const { adminSocketsIdsExtract } = require("../../services/adminServices");
const { buyerGet } = require("../../services/buyerServices");
const { sellerGet } = require("../../services/sellerServices");
const { extractUserSocketIds } = require("../../services/commonServices");

//  Emitting To Id
const socketSingleEmit = (eventName, socketData={}, namespace=null, socketId) => {
try {

    if(namespace)
        socketIO.of(namespace).to(socketId).emit(eventName, socketData);
    else
        socketIO.to(socketId).emit(eventName, socketData);

}
catch(error) {
    socketErrorLogger(eventName, null, error);
}
};
exports.socketSingleEmit = socketSingleEmit;


//      Socket Seller Emit
//const socketSellerEmit = async ()


//      Socket Admin Emit
const socketAdminEmit = async (eventName, socketData) => {
try {

    let adminSocketIds = await adminSocketsIdsExtract();

    adminSocketIds.forEach((socketId, key) => {
    try {
            socketIO.of(globalSocketNameSpaces.admin).to(socketId).emit(eventName, socketData);
    }
    catch(error) {
        socketErrorLogger(eventName, null, error);
    }
    });

    socketInfoLogger(null, null, {
        msg: `Socket Notification Sent - socketAdminEmit`,
        eventName,
        socketData,
        adminSocketIds
    });

}
catch(error) {
    socketErrorLogger(eventName, null, error);
}
};
exports.socketAdminEmit =socketAdminEmit;

//      Socket Seller Buyer Send Notification
const socketSellerBuyerEmit = async (eventName, socketData, namespace, odata) => {
try{

    if(!odata.details) {
        if(!odata.detailId)
            return;

        if(namespace === globalSocketNameSpaces.seller)
            odata.details = await sellerGet({_id: odata.detailId, isDeleted: false, isBlocked: false});
        else 
            odata.details = await buyerGet({_id: odata.detailId, isDeleted: false, isBlocked: false});
    }

    if(!odata.details)
        return;

    odata.details = JSON.parse(JSON.stringify(odata.details));

    let socketIds = extractUserSocketIds(odata.details.logins);

    socketIds.forEach((socketId, key) => {
        try {
            socketIO.of(namespace).to(socketId).emit(eventName, socketData);
        }
        catch(error) {
            socketErrorLogger(eventName, null, error);
        }
    });

    delete odata.details;
    socketInfoLogger(namespace, null, {
        msg: `Socket Notification Sent - socketSellerBuyerEmit`,
        eventName,
        socketData,
        odata,
        socketIds
    });

}
catch(error) {
    socketErrorLogger(eventName, null, error);
}
};
exports.socketSellerBuyerEmit = socketSellerBuyerEmit;
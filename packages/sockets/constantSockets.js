const { statusCodes, successCodes } = require("../../config/properties/constants");
const { AuthHeaderMissing, AuthFailedMsg, ErrorMsg } = require("../../config/properties/constants").globalMessages.responseMessages;

const globalSocketNameSpaces = {
    admin: "/admin",
    seller: "/seller",
    buyer: "/buyer"
};
exports.globalSocketNameSpaces = globalSocketNameSpaces;

//  Global Socket Events
const globalSocketEvents = {
    commonEvent: "Common",
    authErrorEvent: "AuthError",
    errorEvent: "Error",

    notification: "Notification"
};
exports.globalSocketEvents = globalSocketEvents;

//  Global Event Types
const globalSocketActionTypes = {
    productPurchase: "ProductPurchased",
    enquiryCreated: "EnquiryCreated",
    enquiryUpdated: "EnquiryUpdated"
};
exports.globalSocketActionTypes = globalSocketActionTypes;

//  Socket Global Responses JSON
const globalSocketResponses = {
    tokenMissing: { success: successCodes.failed, statusCodes: statusCodes.authFailed, message: AuthHeaderMissing },
    invalidToken: { success: successCodes.failed, statusCodes: statusCodes.authFailed, message: AuthFailedMsg },
    error: { success: successCodes.failed, statusCodes: statusCodes.error, message: ErrorMsg }
};
exports.globalSocketResponses = globalSocketResponses;
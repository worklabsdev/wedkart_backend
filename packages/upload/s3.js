const aws = require("aws-sdk");
const fs = require("fs");

let { responseMessages } = require("../../config/properties/constants").globalMessages;
let { generateRandStr } = require("../../utils/commonFuns");

let s3Config = appConfig.upload.spaces;

aws.config.update({
	accessKeyId: s3Config.accessKeyId,
	secretAccessKey: s3Config.secretAccessKey
});
// Create an S3 client setting the Endpoint to DigitalOcean Spaces
const s3 = new aws.S3();

exports.uploadFile = (file, elementName, opts={}) => {
	//const [fileName, fileExt] = file.name.split('.');

	let mimeType = file.type;
	let [fileType, fileExt] = mimeType.split("/");
	let fileName = generateRandStr(65)+`.${fileExt}`;

	return new Promise((resolve, reject) => {

		fs.readFile(file.path, function (error, file_buffer) {
            if (error) {
                return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
            }
            var params   = {
                Bucket     : s3Config.bucket,
                Key        : `${s3Config.folder}/${fileName}`,
                Body       : file_buffer,
                ACL        : 'public-read',
                ContentType: mimeType
			};

            s3.putObject(params, function (err, data) {
                if (err) {
                    return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
                }
                else {
					return resolve({
                        file: `${spaceConfig.filesURL}${fileName}`,
                        fileType: fileType,
                        fileExt: fileExt,
                        fileSize: file.size/1000//In KB
                    });
                }
            });
        });

	});
}
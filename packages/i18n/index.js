var i18n = require("i18n");// Language

// ///////////////// Localization Added    /////////////////////////////////////////
i18n.configure({

	"lng": "en",
	"defaultLocale": "en",
	"directory": `${__dirname}/locales`,
	"register": global,
	"locales": ["en"],
	"preload": ["en"],
	"fallbackLng": "en",
	"saveMissing": true,
	"sendMissingTo": "en",
	"useCookie": false,
	"detectLngFromHeaders": false,
	"api": {
		"__": "trans", // now req.__ becomes req.trans
		"__n": "tn" // and req.__n can be called as req.tn
	}
});

app.use(i18n.init);

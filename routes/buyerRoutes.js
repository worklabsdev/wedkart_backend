const express = require('express')

const buyerCtrl = require('../controllers/buyer/buyerCtrl')
const productCtrl = require('../controllers/buyer/productCtrl')
const cartCtrl = require('../controllers/buyer/cartCtrl')
const orderCtrl = require('../controllers/buyer/orderCtrl')
const messageCtrl = require('../controllers/buyer/messageCtrl')
const reviewCtrl = require('../controllers/buyer/reviewCtrl')
const wishlistCtrl = require('../controllers/buyer/wishlistCtrl')

const router = express.Router()
var multer = require('multer');

const storage = multer.diskStorage({
   destination: function(req, file, cb){
       cb(null, './uploads');
   },
   filename: function(req, file, cb){
       cb(null, new Date().getTime()+'-' + file.originalname)
   }
});

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg'){
        cb(null, true);
    } else{
        cb(null, false);
    }
}
var upload = multer({
    storage,
    limits: {
        fileSize: 1024 * 1024 * 5,
        fileFilter   
    }   
})

router.post('/login', buyerCtrl.login);//
router.get('/details', buyerCtrl.details);//
router.put('/update-account', buyerCtrl.edit);
router.get('/confirm-account/:code', buyerCtrl.confirmAccount);
router.get('/confirm-token', buyerCtrl.confirmToken);
router.put('/change-password', buyerCtrl.changePassword);//
router.post('/forget-password', buyerCtrl.forgetPassword);//
router.post('/confirm-code', buyerCtrl.confirmCode);//
router.post('/update-password', buyerCtrl.updatePassword);//
router.put('/profile-image', upload.single('avtar') ,buyerCtrl.profileImage);
router.delete('/deleteprofileimage',buyerCtrl.deleteprofileimage);

router.get('/products', productCtrl.allProduct);
router.get('/product/:id', productCtrl.getProduct);
router.get('/product/slug/:slug', productCtrl.getProductBySlug);
// router.get('/getsubcategory/:catid',productCtrl.getsubcategory)
router.post('/product/subcategory',productCtrl.getsubcategoryProd);
router.post('/product/category', productCtrl.getCategoryProd);

router.post("/search", productCtrl.buyersearch);

router.get('/cart', cartCtrl.getCart);
router.put('/cart/empty', cartCtrl.emptyCart);
router.post('/cart/add', cartCtrl.addProductCart);
router.post('/cart/remove', cartCtrl.removeProductCart);
// router.post('/cart/change-quantity', cartCtrl.changeQuantityCart)



router.post('/buy', orderCtrl.buyOrder);
router.post('/order', orderCtrl.placeOrder);
router.put('/order/cancel/:id', orderCtrl.cancelOrder);
router.get('/order/:id', orderCtrl.detailOrder);

router.post('/orders', orderCtrl.allOrders);
router.get('/orders/cancelled',orderCtrl.cancelledOrders);
router.get('/orders/completed', orderCtrl.completedOrders);
router.get('/orders/not-completed', orderCtrl.nonCompletedOrders);


router.get('/messages', messageCtrl.viewMsg);
router.post('/message', messageCtrl.sendMsg);
router.post('/singleConvo', messageCtrl.singleConvo);

router.post('/review-ability', reviewCtrl.canReview);
router.post('/review', reviewCtrl.makeReview);
router.post('/reviews', reviewCtrl.getReviews);

router.post('/wishlist', wishlistCtrl.get);
router.post('/wishlist/add', wishlistCtrl.add);
router.post('/wishlist/remove', wishlistCtrl.remove);
router.post('/wishlist/empty', wishlistCtrl.empty);
router.get('/checkemailpresence/:email', buyerCtrl.checkemailpresence);
router.post('/dashboard', productCtrl.dashboard);


module.exports = router;
const sellerCtrl = require('../controllers/seller/sellerCtrl')
const productCtrl = require('../controllers/seller/productCtrl')
const orderCtrl = require('../controllers/seller/orderCtrl')

const router = express.Router();

const { sellerVerifyMiddleware } = require("../utils/auth/middlewares");

var multer = require('multer');

const storage = multer.diskStorage({
   destination: function(req, file, cb){
       cb(null, './uploads');
   },
   filename: function(req, file, cb){
       console.log('abccccccccccccccccccccccccccccccccc',file)
       console.log('dddddddddddddddddddddddddd',file.originalname)

       cb(null, new Date().getTime()+'-' + file.originalname)
   }
});

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg'){
        cb(null, true);
    } else{
        cb(null, false);
    }
}
var upload = multer({
    storage,
    limits: {
        fileSize: 1024 * 1024 * 5,
        fileFilter   
    } 
    
})

router.post('/login', sellerCtrl.login);//
router.get('/details', sellerCtrl.details);
router.put('/update-account', sellerCtrl.edit);
router.get('/confirm-account/:code', sellerCtrl.confirmAccount);
router.get('/confirm-token', sellerCtrl.confirmToken);
router.put('/change-password', sellerCtrl.changePassword);//
router.post('/forget-password', sellerCtrl.forgetPassword);//
router.post('/confirm-code', sellerCtrl.confirmCode);//
router.post('/update-password', sellerCtrl.updatePassword);//
router.put('/profile-image', upload.single('avtar') ,sellerCtrl.profileImage);
router.delete('/delete-profile-image',sellerCtrl.deleteprofileimage);

// router.post('/addShop',upload.array('shopManagementPic', 5), productCtrl.addShop)
router.post('/addShop',upload.array('shopManagementPic',8), sellerCtrl.addShop);
router.post('/shops', sellerCtrl.allShop);
router.get('/shop/:id', sellerCtrl.getShop);
router.put('/shop/:id',upload.array('shopManagementPic',5), sellerCtrl.editShop);
router.delete('/shop/:id', sellerCtrl.deleteShop);

// router.post('/product', upload.single('productPic'), productCtrl.addProduct);

router.get('/products', productCtrl.allProduct);
router.get('/product/:id', productCtrl.getProduct);
//router.put('/product/:id', productCtrl.editProduct);

router.post('/product/:id/spec', productCtrl.addSpecification);
router.post('/product/:id/spec/remove', productCtrl.deleteSpecification);

router.get('/getsubcategory/:catid',productCtrl.getsubcategory);
router.get('/getcategory', productCtrl.getCategory);

router.post('/product/subcategory',productCtrl.getsubcategoryProd);
router.post('/product/category', productCtrl.getCategoryProd);

router.delete('/product/:id', productCtrl.deleteProduct);

router.post('/orders',orderCtrl.allOrders);
router.get('/orders/cancel-request',orderCtrl.cancelRequestOrders);

router.get('/order/:id',orderCtrl.orderDetails);
router.post('/change-order-status',orderCtrl.changeStatus);
router.get('/analytics',sellerCtrl.analytics);
router.get('/messages',sellerCtrl.messages);
router.post('/singleConvo', sellerCtrl.singleConvo);


module.exports = router;
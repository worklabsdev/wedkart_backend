var multer = require("multer")
var upload = multer({ dest: "uploads/" });

var router = express.Router()

const { adminVerifyMiddleware } = require("../utils/auth/middlewares");

var adminCtrl = require("../controllers/admin/adminCtrl");
var userCtrl = require("../controllers/admin/userCtrl");
var sellerCtrl = require("../controllers/admin/sellerCtrl");

router.post("/",function(req,res){
	res.status(200).render("admin/dist/index");
}),

//	Auth
router.post("/login",adminCtrl.login);
router.post("/forgetpassword",adminCtrl.forgetPassword);
router.post("/logout",adminVerifyMiddleware, adminCtrl.logout);//
router.post("/info", adminVerifyMiddleware, adminCtrl.info);//
router.post("/auth", adminVerifyMiddleware, adminCtrl.auth);
router.post("/changepassword", adminVerifyMiddleware, adminCtrl.changePassword);

router.post("/user/add",userCtrl.add)
router.post("/user/update",userCtrl.update)
router.post("/user/delete",userCtrl.delete)
router.post("/user/activate",userCtrl.activate)
router.post("/user/inactivate",userCtrl.inactivate)
router.post("/users",userCtrl.list)
router.post("/user",userCtrl.get)

router.post("/seller/add",sellerCtrl.add)
router.post("/seller/update",sellerCtrl.update)
router.post("/seller/delete",sellerCtrl.delete)
router.post("/seller/activate",sellerCtrl.activate)
router.post("/seller/inactivate",sellerCtrl.inactivate)
router.post("/sellers",sellerCtrl.list)
router.post("/seller",sellerCtrl.get)

module.exports = router





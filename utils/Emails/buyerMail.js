const smtpTransport = require("../../config/email");

const buyerMailTypes = {
    "buyerRegisterEmailVerify": "BuyerRegisterEmailVerify",
    "buyerRegisterPassword": "BuyerRegisterPassword"
};
exports.buyerMailTypes = buyerMailTypes;

const { mailInfoLogger, mailErrorLogger } = require("../../packages/logging/consoleLogger");

const buyerSendMailFunc = (mailType, data) => {

    let mailOptions = null;
    switch(mailType) {
        case buyerMailTypes.buyerRegisterEmailVerify:
            mailOptions = buyerRegisterEmailVerify(data);    
        break;

        case buyerMailTypes.buyerRegisterPassword:
            mailOptions = buyerRegisterPassword(data);    
        break;
    };

    if(!mailOptions)
        return;

    smtpTransport.sendMail(mailOptions, (err, response) => {
        if(err) {
            mailErrorLogger(data.reference, data.requestId, err, {
                data,
                mailOptions
            });
        }
        else {
            mailInfoLogger(data.reference, data.requestId, {
                data,
                mailOptions,
                response
            });
        }
    });

};
exports.buyerSendMailFunc = buyerSendMailFunc;

const buyerRegisterEmailVerify = (data) => {

    return {
        to: data.email,
        from: appConfig.appName,
        subject: "Buyer Registration, Do not Reply",
        text: `${`You are receiving this because you (or someone else) have requested the create an account on ${appConfig.appName} with this email.\n\n`
      + "Please click on the following link, or paste this into your browser to complete the process:\n\n"
      + `${appConfig.baseURL}/api/v1/buyer/confirmAccount/`}${data.confirmCode
      }\n\n`
      + "If you did not request this, please ignore this email.\n",
    };

};

const buyerRegisterPassword = (data) => {
    return {
        to: data.email,
        from: appConfig.appName,
        subject: "Buyer Registration Password",
        text: `You are receiving this mail because your account has been created on ${appConfig.appName} with email: ${data.email}. Please use following password: ${data.password}`
    };
};
const smtpTransport = require("../../config/email");

const sellerMailTypes = {
    "sellerRegisterEmailVerify": "BuyerRegisterEmailVerify"
};
exports.sellerMailTypes = sellerMailTypes;

const { mailInfoLogger, mailErrorLogger } = require("../../packages/logging/consoleLogger");

const sellerSendMailFunc = (mailType, data) => {

    let mailOptions = null;
    switch(mailType) {
        case sellerMailTypes.sellerRegisterEmailVerify:
            mailOptions = sellerRegisterEmailVerify(data);    
        break;
    };

    if(!mailOptions)
        return;

    smtpTransport.sendMail(mailOptions, (err, response) => {
        if(err) {
            mailErrorLogger(data.reference, data.requestId, err, {
                data,
                mailOptions
            });
        }
        else {
            mailInfoLogger(data.reference, data.requestId, {
                data,
                mailOptions,
                response
            });
        }
    });

};
exports.sellerSendMailFunc = sellerSendMailFunc;

const sellerRegisterEmailVerify = (data) => {

    return {
        to: data.email,
        from: appConfig.appName,
        subject: "Seller Registration, Do not Reply",
        text: `${`You are receiving this because you (or someone else) have requested the create an account on ${appConfig.appName} with this email.\n\n`
      + "Please click on the following link, or paste this into your browser to complete the process:\n\n"
      + `${appConfig.baseURL}/api/v1/seller/confirmAccount/`}${data.confirmCode
      }\n\n`
      + "If you did not request this, please ignore this email.\n",
    };

};
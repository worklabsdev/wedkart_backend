//  Save Data
const saveData = (model, data) => {
    return model.create(data);
};
exports.saveData = saveData;

//  Get Data
const getData = (model, query, projection, options,) => {
    return model.find(query, projection, options);
};
exports.getData = getData;

//  Update One
const findAndUpdate = (model, conditions, update, options) => {
    return model.findOneAndUpdate(conditions, update, options);
};
exports.findAndUpdate = findAndUpdate;

//  Find One
const findOne = (model, query, projection, options) => {
    return model.findOne(query, projection, options);
};
exports.findOne = findOne;

//  Update Many
const findMultipleAndUpdate = (model, conditions, update, options={multi: true}) => {
	return model.update(conditions, update, options);
};
exports.findMultipleAndUpdate = findMultipleAndUpdate;

//  Aggregate Data
const aggregateData = (model, group) => {
	return model.aggregate(group);
};
exports.aggregateData = aggregateData;

//  Count Document
const count = (model, condition) => {
	return model.count(condition);
};
exports.count = count;

/*------------------------------------------------------------------------
 * FIND WITH REFERENCE
 * -----------------------------------------------------------------------*/
const onepopulateData = (model, query, projection, options, collectionOptions) => {
	return model.findOne(query, projection, options).populate(collectionOptions);
};
exports.onepopulateData = onepopulateData;

const populateData = (model, query, projection, options, collectionOptions) => {
	return model.find(query, projection, options).populate(collectionOptions);
};
exports.populateData = populateData;

const deepPopulate = (model, criteria, projectionQuery, options, populateModel, nestedModel, callback) => {
	model.find(criteria, projectionQuery, options).populate(populateModel)
		.exec( (err, docs) => {
			if (err) return callback(err);

			model.populate(docs, nestedModel, (err, populatedDocs) => {
				if (err) return callback(err);
				callback(null, populatedDocs);// This object should now be populated accordingly.
			});
		});
};
exports.deepPopulate = deepPopulate;
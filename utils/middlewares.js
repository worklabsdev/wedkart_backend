const { buyerVerifyMiddleware } = require("../services/buyerServices");

const commonBuyerMiddleware = async (request, response, next) => {
try {

    buyerVerifyMiddleware();

    next();

}
catch (error) {
    return errorHandler(response, error, { data: request.body, apiRef });
}

}
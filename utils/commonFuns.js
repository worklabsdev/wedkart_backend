const randomstring = require("randomstring");
const moment = require("moment");
const slug = require("slug");
const uuidv1 = require("uuid/v1");


const generateFutureDt = (minutes) => {
	//console.log(moment().utc().format("YYYY-MM-DD hh:mm:ss"), moment().utc().add(minutes, "minutes").format("YYYY-MM-DD hh:mm:ss"));
	return moment().utc().add(minutes, "minutes");//.format("YYYY-MM-DD hh:mm:ss");
};

const currentUTC = () => {
	return moment().utc();
	//return Date.now();// moment().utc().format("YYYY-MM-DD hh:mm:ss");
};

const futureDtCheck = (dt) => {
	return moment.utc(dt).isAfter(currentUTC());
};

const generateRandStr = (len) => {
	return randomstring.generate(len);
};

const utcConvertor = (dt, timezone, format="YYYY-MM-DD hh:mm:ss") => {
	return moment.tz(dt, format, timezone).utc().format(format);
}

const generateOTP = (length = 4) => {
	let { otpValidityMinutes } = appConfig.sms;

	let otp = 4444;

	// let otp = randomstring.generate({
	//   	length: length,
	//   	charset: "numeric"
	// });

	let otpValidity = generateFutureDt(otpValidityMinutes);

	return { otp, otpValidity };
};

//	Slug Creator
const slugCreator = (text) => {

	let slugGen = slug(text);
	let randGen = generateRandStr(20);

	return `${slugGen}-${randGen}`;

}

//	Generate Unique Code
const generateUniqueCode = () => {
	return uuidv1();
}

module.exports = {
	utcConvertor,
	generateFutureDt,
	currentUTC,
	futureDtCheck,
	generateRandStr,
	generateOTP,
	slugCreator,
	generateUniqueCode
};

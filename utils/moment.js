const moment = require("moment");

const generateFutureDt = (minutes) => {
	//console.log(moment().utc().format("YYYY-MM-DD hh:mm:ss"), moment().utc().add(minutes, "minutes").format("YYYY-MM-DD hh:mm:ss"));
	return moment().utc().add(minutes, "minutes");//.format("YYYY-MM-DD hh:mm:ss");
};
exports.generateFutureDt = generateFutureDt;

const currentUTC = () => {
	return moment().utc();
};
exports.currentUTC = currentUTC;


const futureDtCheck = (dt) => {
	return moment.utc(dt).isAfter(currentUTC());
};
exports.futureDtCheck = futureDtCheck

//  UTC Convertor
const utcConvertor = (dt, timezone, format="YYYY-MM-DD hh:mm:ss") => {
	return moment.tz(dt, format, timezone).utc().format(format);
};
exports.utcConvertor = utcConvertor;

//  Start of Date Time
const startOfDt = (dt) => {
    var start = new Date(dt);
    start.setHours(0,0,0,0);

    return start;//.toISOString();
};
exports.startOfDt = startOfDt;

//  End of Date Time
const endOfDt = (dt) => {
    var end = new Date(dt);
    end.setHours(23,59,59,999);

    return end;//.toISOString();
};
exports.endOfDt = endOfDt;

//      Diff in Dts
const diffInDts = (start=Date.now(), end, units="days") => {
    end = moment(end);
    start = moment(start);
    
    return end.diff(start, units);
};
exports.diffInDts = diffInDts;
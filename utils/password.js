const bcrypt = require("bcrypt");

const saltRounds = 10;

exports.generatePassword = (password) => {
	var salt = bcrypt.genSaltSync(saltRounds);
	var hash = bcrypt.hashSync(password, salt);

	return { salt, hash };
};

exports.verifyPassword = (password, hashed) => {
	return new Promise((resolve, reject) => {
		bcrypt.compare(password, hashed, (err, res) => {
			if (err || !res) return resolve(false);
			return resolve(true);
		});
	});
};
let errorConfig = appConfig.errorSettings;

let { statusCodes, successCodes, globalMessages } = require("../config/properties/constants");
const { authCaseTypes } = require("./auth/constants");

//let logger = require("../packages/logging");

// ///       Error Handler
exports.errorHandler = (response, error, data = {}) => {
	// logger.fatal("fatal", error);
	console.error("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~", error);
	console.log("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~");

	let success = successCodes.error;

	let message = response.trans(globalMessages.responseMessages.ErrorMsg);
	if (errorConfig.showError) message = error.message;

	return response.status(statusCodes.error).json({
		success,
		message,
		data
	});
};

// ///       Error Handler View
exports.errorHandlerView = (response, error, data = {}) => {
	console.error("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~", error);
	console.log("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~");

	let message = response.trans(globalMessages.responseMessages.ErrorMsg);
	if (errorConfig.showError) message = error.message;

	return response.render('Common/error',{ message, appName: appConfig.appName });
};


// ///       Success Handler
exports.successHandler = (response, msg = null, data = {}) => {
	let { success } = successCodes;
	let message = response.trans(msg || globalMessages.responseMessages.SuccessMsg);

	return response.status(statusCodes.success).json({
		success,
		message,
		data
	});
};
// ///       Success Handler View
exports.successHandlerView = (response, msg=null, data = {}) => {
	let message = response.trans(msg || globalMessages.responseMessages.SuccessMsg);

	return response.render('Common/success',{ message, appName: appConfig.appName });
};

// ///       Created Handler
exports.createdHandler = (response, msg = null, data = {}) => {
	let success = successCodes.created;
	let message = response.trans(msg || globalMessages.responseMessages.SuccessMsg);

	return response.status(statusCodes.created).json({
		success,
		message,
		data
	});
};

// ///       Validation Handler
exports.validationHandler = (response, msg = null, data = {}) => {
	let success = successCodes.validationFailed;
	let message = msg || globalMessages.responseMessages.SuccessMsg;
	// response.trans(msg || globalMessages.responseMessages.successMsg);

	return response.status(statusCodes.validationFailed).json({
		success,
		message,
		data
	});
};

// ///       Failed Action Handler
exports.failedActionHandler = (response, msg = null, data = {}) => {
	let success = successCodes.actionFailed;
	let message = response.trans(msg);

	return response.status(statusCodes.actionFailed).json({
		success,
		message,
		data
	});
};

// ///       Auth Handler
exports.authHandler = (response, type = 2) => {
	let success = successCodes.authFailed;

	let message;

	switch (type) {
	case authCaseTypes.NO_TOKEN:
		message = response.trans(globalMessages.responseMessages.AuthHeaderMissing);
		break;
	case authCaseTypes.NOT_VALID_TOKEN:
		message = response.trans(globalMessages.responseMessages.AuthFailedMsg);
		break;
	case authCaseTypes.ACCOUNT_SUSPENDED:
		message = response.trans(globalMessages.responseMessages.AccountSuspended);
		break;
	case 4:
		message = response.trans(globalMessages.responseMessages.AccountNotFound);
		break;
	case 5:
		message = response.trans(globalMessages.responseMessages.OtpVerificationRequired);
		break;
	default:
		message = response.trans(globalMessages.responseMessages.AuthFailedMsg);
		break;
	}

	return response.status(statusCodes.authFailed).json({
		success,
		message
	});
};

// ///		Handle 404
exports.pageNotFound = (response) => {
	let success = successCodes.pageNotFound;
	let message = response.trans(globalMessages.responseMessages.PageNotFound);

	return response.status(statusCodes.pageNotFound).json({
		success,
		message
	});
};

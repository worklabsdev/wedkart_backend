const authCaseTypes = {
    "NO_TOKEN": "NO_TOKEN",
    "NOT_VALID_TOKEN": "NOT_VALID_TOKEN",
    "ACCOUNT_SUSPENDED": "ACCOUNT_SUSPENDED"
};
exports.authCaseTypes = authCaseTypes;
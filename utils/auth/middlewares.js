const { authHandler, errorHandler } = require("../responseHandler");

const { buyerSignInDetailsAuth } = require("../../services/buyerServices");
const { adminSignInDetailsAuth } = require("../../services/adminServices");
const { sellerSignInDetailsAuth } = require("../../services/sellerServices");

const { authCaseTypes } = require("./constants");

const apiRef = "authMiddleware";



// ///       Verify Token Buyer
const buyerVerifyMiddleware = async (request, response, next) => {
	try {
		let token = request.headers["x-access-token"] || request.headers["authorization"] || request.headers["x-auth-token"];

		if (!token) return authHandler(response, authCaseTypes.NO_TOKEN);

		if (token.startsWith("Bearer ")) {
			token = token.slice(7, token.length); // Remove Bearer from string
		}

		let buyer = await buyerSignInDetailsAuth(response, token);

		request.currentUrl = request.originalUrl;

        request.buyerDetails = buyer;

		return next();
	} catch (error) {
		return errorHandler(response, error, { data: request.body, apiRef });
	}
};
exports.buyerVerifyMiddleware = buyerVerifyMiddleware;

//	///	Verify Token Socket Buyer
const buyerVerifySocketMiddleware = async (token, next) => {
try {

	next();
}
catch (error) {
	return next(error);
	//return errorHandler(response, error, { data: request.body, apiRef });
}
}
exports.buyerVerifySocketMiddleware = buyerVerifySocketMiddleware;


//	///	Skip  Verify Buyer Token Check
const buyerVerifyMiddlewareSkip = async (request, response, next) => {

	let token = request.headers["x-access-token"] || request.headers["authorization"] || request.headers["x-auth-token"];

	if(token) {
		await buyerVerifyMiddleware(request, response, next);
	}
	else {
		next();
	};

};
exports.buyerVerifyMiddlewareSkip = buyerVerifyMiddlewareSkip;
///


const sellerVerifyMiddleware = async (request, response, next) => {
	try {
		let token = request.headers["x-access-token"] || request.headers["authorization"] || request.headers["x-auth-token"];

		if (!token) return authHandler(response, authCaseTypes.NO_TOKEN);

		if (token.startsWith("Bearer ")) {
			token = token.slice(7, token.length); // Remove Bearer from string
		}

		let seller = await sellerSignInDetailsAuth(response, token);

		request.currentUrl = request.originalUrl;

        request.sellerDetails = seller;

		return next();
	} catch (error) {
		return errorHandler(response, error, { data: request.body, apiRef });
	}
};
exports.sellerVerifyMiddleware = sellerVerifyMiddleware;

// ///       Verify Admin Token
const adminVerifyMiddleware = async (request, response, next) => {
	try {
		let token = request.headers["x-access-token"] || request.headers["authorization"]|| request.body.token || request.headers["x-auth-token"];

		if (!token) return authHandler(response, authCaseTypes.NO_TOKEN);

		if (token.startsWith("Bearer ")) {
			token = token.slice(7, token.length); // Remove Bearer from string
		}

		let admin = await adminSignInDetailsAuth(response, token);

		request.currentUrl = request.originalUrl;

        request.adminDetails = admin;

		return next();
	} catch (error) {
		return errorHandler(response, error, { data: request.body, apiRef });
	}
};
exports.adminVerifyMiddleware = adminVerifyMiddleware;
var smtpTransport = require("../../config/email");
require("../../plugins/jsend");

const { verifyPassword, generatePassword } = require("../../utils/password");
const { generateRandStr } = require("../../utils/commonFuns");

var adminMdl = require("../../models/admin.js");

//	Login
const loginAction = async (req, res) => {
try {

	req.checkBody("email", "Invalid Email").exists().isEmail();
	req.checkBody("password", "Invalid Password").exists().isLength({ min: 3 });
  
	var errors = req.validationErrors();
	if (errors) {
	  return res.status(400).failure("Errors", { errors: errors });
	}

	var email = req.body.email.toString().trim();

	let admin = await adminMdl.findOne({email}).lean();
	if(!admin)
		return res.status(400).failure("Incorrect username or password");

	let checkPass = await verifyPassword(req.body.password, admin.password);
	if(!checkPass) return res.status(400).failure("Invalid Username or Password");
	
	var token = generateRandStr(100);

    let newData =  {
		token,
		updatedAt: Date.now()
	};

	admin = await adminMdl.findByIdAndUpdate(admin._id, newData, { new: true });

	return res.success({ "token": token }, "Logged in successfully");

	}
catch(err) {
	return res.status(500).error(err, {});
}
};

//	Forgot Password
const forgetPassword = async (req, res) => {
try {
	req.checkBody("email", "Invalid Email").exists().isEmail();  
	var errors = req.validationErrors();
	if (errors) {
	  return res.status(400).failure("Errors", { errors: errors });
	}

	var email = req.body.email.toString().trim();

	let admin = await adminMdl.findOne({
		email: email,
		isDeleted: false
	});
	if(!admin)
		return res.status(400).failure("No Such Email Id Exists");

	let pass = generateRandStr(10);
	let password = generatePassword(pass);

	req.body.pass = password.hash;
	req.body.salt = password.salt;

	admin = await adminMdl.findByIdAndUpdate(
		admin._id,
		{
			$set: {
				password: req.body.pass,
				salt: req.body.salt,
				token: "",
				updatedAt: Date.now()
			}
		},
		{
			new: true
		}
	);

	var mailOptions = {
		from: "piyushkapoor786@gmail.com",
		to: email,
		subject: "Forget Password | Colispace Admin",
		text: `<p>Someone Requested a password from your Colispace admin account<br>Your Password is <h2>${pass}</h2> <br> If you have not make this request than you can successfully ignore this.<br> Thank You. <br></p>`
	};

    smtpTransport.sendMail(mailOptions, function(err,response) {
		if (err)
			console.log(err);
		
			return;
	});

	console.log("pass", pass);

	return res.success({ "email":email }, "Your password is sent to your mail");

} catch(err) {
	return res.status(500).error(err, {});
}
};

//		Logout
const logoutAction = async (req, res) => {
try {

	let update = await adminMdl.findByIdAndUpdate(
		req.adminDetails._id,
		{
			$set: { 
				"token": ""
			}
		}
	);

	return res.success([],"Successfully logged out");

} catch(err) {
	return res.status(500).error(err, {});
}
};

//		Info
const infoAction = (req, res) => {
try {
	return res.success(req.adminDetails,"Information");
}catch(err) {
	return res.status(500).error(err, {});
}
};

//	Token Check
const authAction = (req, res) => {
try {
	return res.success([],"Valid Token");
}catch(err) {
	return res.status(500).error(err, {});
}
};

//	Password Change
const changePasswordAction = async (req, res) => {
try {

	if (req.body.oldpassword && req.body.oldpassword != "") {
		var oldpassword = req.body.oldpassword.toString();
	}
	else {
		return res.status(400).failure("please Enter old password");
	}

	if (req.body.newpassword && req.body.newpassword != "") {
		var newpassword = req.body.newpassword.toString();
	}
	else {
		return res.status(400).failure("please Enter New password");
	}

	if (req.body.cpassword && req.body.cpassword != "") {
		var cpassword = req.body.cpassword.toString();
	}
	else {
		return res.status(400).failure("please Enter Confirm password");
	}

	let checkPass = await verifyPassword(oldpassword, req.adminDetails.password);
	if(!checkPass) return res.status(400).failure("Invalid old password");

	let password = await generatePassword(newpassword);

	await adminMdl.findByIdAndUpdate(
		req.adminDetails._id,
		{
			$set: {
				password: password.hash,
				salt: password.salt,
				updatedAt: Date.now()
			}
		},
		{ 
			new: true
		}
	);
	
	return res.success({ email: req.adminDetails.email }, "Password updated successfully");

}catch(err) {
	return res.status(500).error(err, {});
}
};

module.exports = {
	"login": loginAction,
	"logout": logoutAction,
	"info": infoAction,
	"auth": authAction,
	"forgetPassword": forgetPassword,
	"changePassword": changePasswordAction
};
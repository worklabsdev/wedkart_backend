var buyerMdl = require('../../models/buyer');
var reviewMdl = require('../../models/reviews');
var orderMdl = require('../../models/order');

function canReviewAction(req, res) {
    console.log(req.body)
    req.checkHeaders('x-auth-token', 'Invalid Token').exists();
    req.checkBody('product_id', 'Invalid Product Id').exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure('Errors', {
            'errors': errors
        })
    }
    var token = req.headers['x-auth-token'].trim();
    buyerMdl.findByToken(token).populate('cart.product_id').exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure('No User found.')
console.log(req.body.product_id)
console.log('aaaaaaaaaa',data['_id'])
        orderMdl.findOne({buyerId:data['_id'], _id:req.body.product_id, status:'Completed'}).exec((err,odata)=>{
            if(err) throw err;
            if(!odata){
               return res.failure('You Cant Review The Product')
            }
            reviewMdl.findOne({buyerId:odata['_id'], product_id:req.body.product_id}).exec((err,data)=>{
                if(err) throw err;
                if(data){
                   return res.success(data,'You Can Edit the older Review of this Product')
                }else {
                    return res.success({},'You Can Review The Product')
                }
            })

        })
    })
}

function getReviewsAction(req, res) {
    req.checkHeaders('x-auth-token', 'Invalid Token').exists();
    req.checkBody('product_id', 'Invalid Product Id').exists();
   
    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure('Errors', {
            'errors': errors
        })
    }
    var token = req.headers['x-auth-token'].trim();
    buyerMdl.findByToken(token).populate('cart.product_id').exec(function(err, udata) {
        if (err) throw err;
        if (!udata) return res.status(400).failure('No User found.')

        reviewMdl.find({ product_id:req.body.product_id}).populate('buyerId').exec((err,data)=>{
            if(err) throw err;
            res.success(data)    
        })
    })
}


function makeReviewAction(req, res) {
    req.checkHeaders('x-auth-token', 'Invalid Token').exists();
    req.checkBody('product_id', 'Invalid Product Id').exists();
    req.checkBody('title', 'Invalid Review Title').exists();
    req.checkBody('message', 'Invalid Review Message').exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure('Errors', {
            'errors': errors
        })
    }
    var token = req.headers['x-auth-token'].trim();
    buyerMdl.findByToken(token).populate('cart.product_id').exec(function(err, udata) {
        if (err) throw err;
        if (!udata) return res.status(400).failure('No User found.')

        reviewMdl.find({buyerId:udata['_id'], product_id:req.body.product_id}).exec((err,data)=>{
            if(err) throw err;
            if(!data.length){
                reviewMdl.remove({buyerId:udata['_id'], product_id:req.body.product_id}).exec((err,result)=>{
                    if(err) throw err
                    let review = reviewMdl({
                        message:req.body.message,
                        title:req.body.title,
                        buyerId:udata['_id'],
                        product_id:req.body.product_id
                    })
                    review.save((err,result)=>{
                        if(err) throw err
                        res.success(review,'Review Successfully Added')    
                    })
                })
            } else {
                let review = reviewMdl({
                    message:req.body.message,
                    title:req.body.title,
                    buyerId:udata['_id'],
                    product_id:req.body.product_id
                })
                review.save((err,result)=>{
                    if(err) throw err
                    res.success(review,'Review Successfully Added')    
                })
            }
        })
    })
}

module.exports = {
    'canReview': canReviewAction,
    'makeReview': makeReviewAction,
    'getReviews': getReviewsAction,
}
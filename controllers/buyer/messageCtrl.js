var messageMdl = require("../../models/messages");
var buyerMdl = require("../../models/buyer");

var sendMsgAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("message", "Message Text Not found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
    var token = req.headers["x-auth-token"].trim();
        let message = new messageMdl({
            message:req.body.message,
            buyerId:data["_id"],
            sender:"buyer",
            sellerId:req.body.sellerId,
            productId:req.body.productId
        })
        message.save()
        res.success({},"message sent Succesfully")
    })
}



var viewMsgAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")

        messageMdl.find({buyerId:data["_id"]}).sort({"createdAt":"desc"}).exec((err,messages)=>{
            if(err) throw err;
            res.success(messages)
        })
    })
}

var singleConvoAction = (req,res) => {
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")

        messageMdl.find({buyerId:data["_id"]}).sort({"createdAt":"desc"}).exec((err,messages)=>{
            if(err) throw err;
            res.success(messages)
        })
    })
}

module.exports = {
    "sendMsg": sendMsgAction,
    "viewMsg": viewMsgAction,
    "singleConvo":singleConvoAction
};
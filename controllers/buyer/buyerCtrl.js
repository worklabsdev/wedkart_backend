var buyerMdl = require("../../models/buyer");
var async = require("async");

const { generateRandStr } = require("../../utils/commonFuns");
const { generatePassword, verifyPassword } = require("../../utils/password");

var smtpTransport = require("../../config/email");

//  Buyer Login
var loginAction = async (req, res) => {
try {

      req.checkBody("email", "Invalid Email or Phone Number").exists();
      req.checkBody("password", "Invalid Password").exists().isLength({ min: 3 });
      var errors = req.validationErrors();
      if (errors) return res.status(400).failure("Errors", { errors: errors });

      var buyer = {};
      buyer["email"] = req.body.email.toLowerCase().trim();
      buyer["password"] = req.body.password;
    
      buyer = await buyerMdl.findExistence(buyer["email"]);
      if(!buyer)
        return res.status(400).failure("Invalid Username or Password");

      let checkPass = await verifyPassword(req.body.password, buyer.password);
      if(!checkPass) return res.status(400).failure("Invalid Username or Password");

      if(!buyer.isConfirmed)
        return res.status(400).failure("Please Confirm Your Account", { email: buyer["email"] });
      else if(buyer.isBlocked)
        return res.status(400).failure("Your Account has Been Blocked");

      var token = generateRandStr(100);

      let newData =  {
        $push: {
          token,
          logins: {
            token: token,
            valid: true,
            otherData: {}
          }
        },
        $set: {
          updatedAt: Date.now()
        }
      };

      buyer = await buyerMdl.findByIdAndUpdate(buyer._id, newData, { new: true });
    
      return res.status(200).success({ token, name: buyer.name, email: buyer.email, _id: buyer._id }, "Buyer Logged In successfully");

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

//  Confirm Account
var confirmAccountAction = function(req, res) {
try {
 
    req.checkParams("code", "No Confirm Code Found").exists();

    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    var buyer = {};
    buyer["confirmCode"] = req.params.code.trim();

    buyerMdl.findOne(buyer).exec(function(err, data) {
      if (err) throw err;

      if (!data) {
        return res.status(400).failure("Invalid Confirm Code");
      }

    buyerMdl.update({ _id: data._id }, { $set: { isConfirmed: true } }).exec();
    return res.status(200).success({}, "Buyer's Account confirmed Successfully");
    });
  
} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

var confirmTokenAction = function(req, res) {
try {

    req.checkHeaders("x-auth-token", "No Token Found").exists();

    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let token = req.headers["x-auth-token"];
    buyerMdl.findByToken(token).exec(function(err, data) {
      if (err) throw err;
      if (!data) {
        return res.failure("Invalid Token");
      } else {
        return res.success({}, "Token Validated");
      }
    });

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

var detailsAction = function(req, res) {
try {
    req.checkHeaders("x-auth-token", "No Token Found").exists();

    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let token = req.headers["x-auth-token"];
    buyerMdl.findOne(
      {token}, 
      { password: 0, token: 0, logins: 0, recoverCode: 0, isDeleted: 0, salt: 0 }
    ).exec(function(err, data) {
      if (err) throw err;
      if (!data) {
        return res.failure("Invalid Token");
      } else {
        return res.success(data);
      }
    });

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

//    Change Password
var changePasswordAction = async (req, res) => {
try {
    req.checkHeaders("x-auth-token", "No Token Found").exists();
    req.checkBody("opassword", "Invalid New Password").exists();
    req.checkBody("npassword", "Invalid New Password").isLength({ min: 3 });

    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let token = req.headers["x-auth-token"];
    let opassword = req.body.opassword;
    let npassword = req.body.npassword;

    let buyer = await buyerMdl.newFindByToken(token);
    if(!buyer)
      return res.failure("Invalid Token");

    let checkPass = await verifyPassword(opassword, buyer.password);
    if(!checkPass) return res.status(400).failure("Incorrect Old Password");

    let password = await generatePassword(npassword);

    await buyerMdl.findByIdAndUpdate(
      buyer._id,
      {
        $set : {
          password: password.hash,
          salt: password.salt,
          updatedAt: Date.now()
        }
      },
      {
        new: true
      }
    );

    return res.success({}, "buyer Password Successfully Changed");
}
catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

//  Forgot Password
var forgetPasswordAction = async (req, res) => {
try {
    req.checkBody("email", "Invalid Email Address").isEmail();

    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let email = req.body.email.toLowerCase().trim();

    let buyer = await buyerMdl.findOne({email: email, isDeleted: false});
    if(!buyer)
      return res.status(400).failure("Email Is not Registered With Us");

    var recoverCode = generateRandStr(10);

    await buyerMdl.findByIdAndUpdate(
      buyer._id,
      {
        $set : {
          recoverCode: recoverCode,
          updatedAt: Date.now()
        }
      },
      {
        new: true
      }
    );

    var mailOptions = {
      to: req.body.email,
      from: "Wedding Kart",
      subject: "Forget Password, Do not Reply",
      html:
        "You are receiving this because you (or someone else) have requested the create an forget password on Colispace with this email.\n\n" +
        "Please use following code to recover your password <h1>" +
        recoverCode +
        "</h1>\n\n" +
        "If you did not request this, please ignore this email.\n"
    };

    smtpTransport.sendMail(mailOptions, function(err) {
      if (err)
        console.error(err, "Buyer forgetPasswordAction");
    });

    return res.success({ email: email }, "buyer Recover Token Sent On Email");
  }
catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

//    Update Password
var updatePasswordAction = async (req, res) => {
try {
    req.checkBody("email", "Invalid Email").isEmail();
    req.checkBody("code", "Invalid Recover Code").exists();
    req.checkBody("password", "Invalid Password").exists().isLength({ min: 3 });
    var errors = req.validationErrors();
    if (errors)
      return res.status(400).failure("Errors", { errors: errors });

    let email = req.body.email.toLowerCase().trim();
    let code = req.body.code.trim();
    let password = req.body.password.trim();

    let buyer = await buyerMdl.findOne({ email: email, recoverCode: code, isDeleted: false });
    if(!buyer)
      return res.failure("Recover Code has Expired");

    password = await generatePassword(password);

    await buyerMdl.findByIdAndUpdate(
        buyer._id,
        {
          $set : {
            password: password.hash,
            salt: password.salt,
            updatedAt: Date.now()
          }
        },
        {
          new: true
        }
      );

      return res.success({}, "Password Successfully Updated, Please Login Now");

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

var editAction = function(req, res) { 
try {

    req.checkHeaders("x-auth-token", "No Token Found").exists();
    req.checkBody("name", "Invalid Person Name").exists();
    req
      .checkBody("phone", "Invalid Phone Number")
      .exists()
      .isLength({ min: 10, max: 12 });
    req.checkBody("address", "Invalid Address").exists();
    req.checkBody("city", "Invalid country").exists();
    req.checkBody("state", "Invalid country").exists();
    req.checkBody("country", "Invalid country").exists();
    req.checkBody("pincode", "Invalid Pincode").exists();
    req.checkBody("gender", "Invalid Gender").exists();

    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    var token = req.headers["x-auth-token"];
    var buyer = {};
    buyer["name"] = req.body.name.trim();
    buyer["phone"] = req.body.phone.trim();
    buyer["address"] = req.body.address.trim();
    buyer["city"] = req.body.city.trim();
    buyer["state"] = req.body.state.trim();
    buyer["country"] = req.body.country.trim();
    buyer["pincode"] = req.body.pincode.trim();
    buyer["gender"] = req.body.gender.trim();
    async.waterfall(
      [
        callback => {
          buyerMdl.findByToken(token).exec((err, data) => {
            if (err) throw err;
            if (!data) return res.status(400).failure("Invalid Token");
            callback(null, data["_id"]);
          });
        },
        (id, callback) => {
          buyerMdl.update({ _id: id }, { $set: buyer }).exec((err, data) => {
            if (err) throw err;
            callback(null);
          });
        }
      ],
      err => {
        if (err) throw err;
        return res.success({}, "buyer Successfully Updated");
      }
    );

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

//  Image Update
var profileImageAction = function(req, res) {
try {

    req.checkHeaders("x-auth-token", "No Token Found").exists();
    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let token = req.headers["x-auth-token"];

    if (req.file) {
      buyerMdl.findByToken(token).exec(function(err, data) {
        if (err) throw err;
        if (!data) {
          return res.status(400).failure("Invalid Token");
        } else {
          let imgurl = req.headers.host + "/" + req.file.path;
          buyerMdl
            .update({ _id: data["_id"] }, { $set: { image: imgurl } })
            .exec();
          return res.success({ url: imgurl }, "buyer Profile Pic Updated");
        }
      });
    } else {
      return res.status(400).failure("Image Not Found");
    }

}
catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

//  Confirm Code
var confirmCodeAction = function(req, res) {
try {

    req.checkBody("email", "Invalid Email").isEmail();
    req.checkBody("code", "Invalid Recover Code").exists();
    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let email = req.body.email;
    let code = req.body.code;

    buyerMdl
      .findOne({ email: email, isDeleted: false })
      .exec(function(err, data) {
        if (err) throw err;
        if (!data) {
          return res.failure("Wrong Email Address");
        } else {
          if (data.recoverCode == code) {
            return res.success({}, "buyer Recover Token Matched");
          } else {
            return res.status(400).failure("Wrong Recover Code");
          }
        }
      });

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};


var deleteprofileImageAction = function(req, res) {
try {

    req.checkHeaders("x-auth-token", "No Token Found").exists();
    var errors = req.validationErrors();
    if (errors) return res.status(400).failure("Errors", { errors: errors });

    let token = req.headers["x-auth-token"];
    buyerMdl.findByToken(token).exec(function(err, data) {
      if (err) throw err;
      if (!data) {
        return res.failure("Invalid Token");
      } else {
          buyerMdl.removeimage({ _id: data["_id"] });
          return res.success({}, "Successfully Removed");
      }
    });

}
catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

var checkemailpresence = function(req,res) {
try {

    var email = req.params.email ? req.params.email : "";
    console.log(email)
    buyerMdl.find({email:req.params.email},(err,result)=> {
      if(err) res.send(err);
      if(result.length){
        return res.status(200).json(result[0]);
      }
      else {
        return res.status(200).json({Message : "New User"});
        // return res.status(400).failure("Email Address Already In Use");

        // res.status(404).send("New User");
      }
    });

} catch(err) {
  console.log(err);
  return res.status(500).failure(err.message);
}
};

module.exports = {
  checkemailpresence:checkemailpresence,
  login: loginAction,
  confirmAccount: confirmAccountAction,
  confirmToken: confirmTokenAction,
  edit: editAction,
  confirmCode: confirmCodeAction,
  details: detailsAction,
  changePassword: changePasswordAction,
  forgetPassword: forgetPasswordAction,
  updatePassword: updatePasswordAction,
  profileImage: profileImageAction,
  deleteprofileimage: deleteprofileImageAction
};
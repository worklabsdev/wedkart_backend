var async = require("async");
var buyerMdl = require("../../models/buyer");
var orderMdl = require("../../models/order");
var productMdl = require("../../models/product");
var smtpTransport = require("../../config/email");

var placeOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("address", "Address Not found").exists();
    req.checkBody("city", "City Not found").exists();
    req.checkBody("state", "State Not found").exists();
    req.checkBody("country", "Country Not found").exists();
    req.checkBody("pincode", "Pincode Not found").exists();
    req.checkBody("contact_no", "Contact No Not found").exists();
    req.checkBody("contact_name", "Contact Person Name Not found").exists();
    req.checkBody("mode_of_payment", "Mode Of Payment Not found").exists();
    req.checkBody("sub_amount", "Sub Amount Not found").exists();
    req.checkBody("shipping_charges", "Shipping Charges Not found").exists();



    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        
        

        async.waterfall([
            (cb)=>{
                sellerOrders={}
                data.cart.forEach(element=>{
                    if(sellerOrders[element.product_id.seller.id]) sellerOrders[element.product_id.seller.id].push(element)
                    else sellerOrders[element.product_id.seller.id] = [element]
                })
        
                console.log(sellerOrders)
                for (key in sellerOrders){
                    console.log("aaaaaaaaaaaa",parseInt(req.body.sub_amount))
                    console.log(parseInt(req.body.shipping_charges))

                    sellerOrder={}            
                    sellerOrder["products"] = sellerOrders[key]
                    sellerOrder["sellerId"] = key
                    sellerOrder["buyerId"] = data["_id"]
                    sellerOrder["address"] = req.body.address
                    sellerOrder["city"] = req.body.city
                    sellerOrder["state"] = req.body.state
                    sellerOrder["country"] = req.body.country
                    sellerOrder["pincode"] = req.body.pincode
                    sellerOrder["contact_no"] = req.body.contact_no
                    sellerOrder["contact_name"] = req.body.contact_name
                    sellerOrder["mode_of_payment"] = req.body.mode_of_payment
                    sellerOrder["sub_amount"] = req.body.sub_amount
                    sellerOrder["shipping_charges"] = req.body.shipping_charges 
                    sellerOrder["total_amount"] =  parseInt(req.body.sub_amount)+parseInt(req.body.shipping_charges)
                    sellerOrder = new orderMdl(sellerOrder)
                    sellerOrder.save()
                }
                // buyerMdl.update({_id:data["_id"]},{$set:{sellerOrder:[]}}).exec()

                buyerMdl.update({_id:data["_id"]},{$set:{cart:[]}}).exec()
                // console.log("cart",cart)
                // console.log("sellerOrder",sellerOrder)

                res.success({},"Order Succesfully Placed")
                cb(null,sellerOrders)
            },
            (products,cb)=>{
                let productsArr = Object.values(products)
                async.eachSeries(productsArr,(product,cb)=>{
                    async.eachSeries(product,(proItem,callback)=>{
                        productMdl.findOne({_id:proItem.product_id}).exec((err,proData)=>{
                            if(err) throw err
                            
                            var mailOptions = {
                            to: data["email"],
                            from: "Wedding Kart",
                            subject: "Order Placed, Do not Reply",
                            text:
                                "Order Placed : Your Order for "+proData["name"]+"has been successfully placed. Please visit our webiste for furthor details" 
                            };
                            
                            smtpTransport.sendMail(mailOptions, function(err) {
                                if (err) throw err;
                                console.log("mail sent : order placed")
                                callback(null)
                            });
                        })
                    },(err)=>{
                        if(err) throw err
                        cb()
                    })
                })
            }
        ],(err)=>{
            if(err) throw err;
        })
    })
}


var buyOrderAction = function(req,res){

    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("product_id", "Product Not found").exists();
    req.checkBody("address", "Address Not found").exists();
    req.checkBody("city", "City Not found").exists();
    req.checkBody("state", "State Not found").exists();
    req.checkBody("country", "Country Not found").exists();
    req.checkBody("pincode", "Pincode Not found").exists();
    req.checkBody("contact_no", "Contact No Not found").exists();
    req.checkBody("contact_name", "Contact Person Name Not found").exists();
    req.checkBody("mode_of_payment", "Mode Of Payment Not found").exists();
    req.checkBody("sub_amount", "Sub Amount Not found").exists();
    req.checkBody("shipping_charges", "Shipping Charges Not found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    buyerMdl.findByToken(token).exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")

        productMdl.findOneAndUpdate({_id:req.body.product_id},{$inc:{quantity:-1}},{new:true}).exec((err,productData)=>{
            if(err) throw err;
            if(!productData) return res.status(400).failure("No Product found.")
            sellerOrder={}
            sellerOrder["products"]=[{
                product_id:req.body.product_id,
                quantity:req.body.quantity,
                size:req.body.size,
                color:req.body.color,
            }]
            sellerOrder["sellerId"] = productData.seller.id
            sellerOrder["buyerId"] = data["_id"]
            sellerOrder["address"] = req.body.address
            sellerOrder["city"] = req.body.city
            sellerOrder["state"] = req.body.state
            sellerOrder["country"] = req.body.country
            sellerOrder["pincode"] = req.body.pincode
            sellerOrder["contact_no"] = req.body.contact_no
            sellerOrder["contact_name"] = req.body.contact_name
            sellerOrder["mode_of_payment"] = req.body.mode_of_payment
            sellerOrder["sub_amount"] = req.body.sub_amount
            sellerOrder["shipping_charges"] = req.body.shipping_charges 
            sellerOrder["total_amount"] =  parseInt(req.body.sub_amount)+parseInt(req.body.shipping_charges)
            sellerOrder = new orderMdl(sellerOrder)
            sellerOrder.save({},(err,result)=>{
                if(err) throw err
            })
            res.success({},"Order Succesfully Placed")

            productMdl.findOne({_id:sellerOrder.products[0].product_id}).exec((err,proData)=>{
                if(err) throw err
                
                var mailOptions = {
                to: data["email"],
                from: "Wedding Kart",
                subject: "Order Placed, Do not Reply",
                text:
                    "Order Placed : Your Order for "+proData["name"]+"has been successfully placed. Please visit our webiste for furthor details" 
                };
                
                smtpTransport.sendMail(mailOptions, function(err) {
                    if (err) throw err;
                    console.log("mail sent : order placed")
                    callback(null)
                });
                
            })
        })
    })
}


var allOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("buyer_id", "Invalid buyer_id").exists();

    // var errors = req.validationErrors();
    // if (errors) {
    //     return res.status(400).failure("Errors", {
    //         "errors": errors
    //     })
    // }
    // var token = req.headers["x-auth-token"].trim();
    // buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
    //     if (err) throw err;
    //     if (!data) return res.status(400).failure("No User found.")
        console.log(req.body.buyer_id)
        orderMdl.find({buyerId:req.body.buyer_id}).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            console.log(data)
            res.success(data)
        })

    // })
}


var cancelledOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        
        orderMdl.find({buyerId:data["_id"],status:"Cancelled"}).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            res.success(data)
        })

    })
}


var completedOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        orderMdl.find({buyerId:data["_id"], status:"Completed"}).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            res.success(data)
        })

    })
}



var nonCompletedOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        
        orderMdl.find({buyerId:data["_id"],status:{$eq:"Pending"}}).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            res.success(data)
        })

    })
}


var cancelOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkParams("id", "Order Id Not found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        
        orderMdl.update({_id:req.params.id},{$set:{status:"Cancel_Request"}}).exec((err,result)=>{
            if(err) throw err;
            res.success({},"Order Succesfully Cancelled>>>")
        })
        
    })
}


var detailOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkParams("id", "Order Id Not found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        
        orderMdl.findOne({_id:req.params.id}).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            res.success(data,"Order Succesfully get")
        })
    })
}

module.exports = {
    "placeOrder":placeOrderAction,
    "buyOrder":buyOrderAction,
    "cancelOrder":cancelOrderAction,
    "detailOrder":detailOrderAction,
    "allOrders":allOrderAction,
    "cancelledOrders":cancelledOrderAction,
    "completedOrders":completedOrderAction,
    "nonCompletedOrders":nonCompletedOrderAction,
};
                
 
 
var buyerMdl = require("../../models/buyer");

var getListAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        });
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("wishlist.product_id").populate({path:"nested"}).exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.");

        return res.success({wishlist:data.wishlist})
    })
}


var emptyListAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    buyerMdl.findByToken(token).lean().exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.");

        buyerMdl.update({"_id":data["_id"]},{$set:{wishlist:[] }}).exec((err,result)=>{
            if(err) throw err;
            res.success({}, "All Products successfully removed from wish list");
        })
    })
}


var addProductListAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("product_id", "Product Id Not Found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).lean().exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.");
        wish={}
        wish["product_id"] = req.body.product_id;

        keyExist = false
        data.wishlist.map(item => {
            if(item.product_id==wish.product_id){
                res.success(data.wishlist, "Product Already added to Wishlist");
            }
            return item
        })

        if(!keyExist){
            data.wishlist.push(wish)
        }

        buyerMdl.update({"_id":data["_id"]},{$set:data}).exec((err,result)=>{
            if(err) throw err;
            res.success(data.wishlist, "Product successfully added to Wishlist");
        })
    })
}




var removeProductListAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("product_id", "Product Id Not Found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    buyerMdl.findByToken(token).lean().exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.");
        wish={}
        wish["product_id"] = req.body.product_id;

        data.wishlist= data.wishlist.filter(item=> item.product_id!=req.body.product_id);

        buyerMdl.update({"_id":data["_id"]},{$set:data}).exec((err,data)=>{
            if(err) throw err;
            res.success(data.wishlist, "Product successfully removed to list");
        })
    })
}

module.exports = {
    "get": getListAction,
    "empty": emptyListAction,
    "add": addProductListAction,
    "remove": removeProductListAction
};
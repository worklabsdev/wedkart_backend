var async = require("async");
var buyerMdl = require("../../models/buyer");
var sellerMdl = require("../../models/seller");

var getCartAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    buyerMdl.findByToken(token).populate("cart.product_id").populate({path:"nested", deepPopulate:"cart.product_id.seller.id"}).exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")

        sub_amount=0
        sellerCount=[]
        async.map(data.cart,(product,callback)=>{
            sub_amount = sub_amount+parseInt(product.product_id.dprice)*product.quantity
            sellerMdl.findOne({_id:product.product_id.seller.id}).lean().exec((err,sellerData)=>{
                if(err) throw err 
                if(!sellerCount.includes(sellerData._id))
                    sellerCount.push(sellerData._id)
                delete sellerData._id                
                delete sellerData.token                
                delete sellerData.token                
                delete sellerData.confirmCode                
                delete sellerData.recoverCode                
                delete sellerData.isDeleted
                delete sellerData.isBlocked
                delete sellerData.isConfirmed
                delete sellerData.isApproved
                delete sellerData.password
                delete sellerData.__v                
                product.product_id.seller["details"]=sellerData
                callback(product)
            })
        },(err)=>{
            charges={}
            charges.sub_amount = sub_amount
            charges.shipping_charges = 0
            charges.total_amount  = charges.sub_amount + charges.shipping_charges
            
            return res.success({cart:data.cart, charges:charges})
        })
    })
}





var emptyCartAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    buyerMdl.findByToken(token).lean().exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")

        buyerMdl.update({"_id":data["_id"]},{$set:{cart:[] }}).exec((err,result)=>{
            if(err) throw err;
            res.success({}, "All Products successfully removed from cart")
        })
    })
}


var addProductCartAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("product_id", "Product Id Not Found").exists();
    req.checkBody("quantity", "Quantity Not Found").exists();
    req.checkBody("size", "Size Not Found").exists();
    req.checkBody("color", "Color Not Found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    buyerMdl.findByToken(token).lean().exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        cart={}
        cart["product_id"] = req.body.product_id
        cart["quantity"] = req.body.quantity
        cart["size"] = req.body.size
        cart["color"] = req.body.color

        keyExist = false
        data.cart.map(item => {
            if(item.product_id==cart.product_id && item.size==cart.size && item.color==cart.color){
                keyExist=true
                item.quantity = parseInt(item.quantity)+parseInt(cart.quantity)
            }
            return item
        })

        if(!keyExist){
            data.cart.push(cart)
        }

        buyerMdl.update({"_id":data["_id"]},{$set:data}).exec((err,result)=>{
            if(err) throw err;
            res.success(data.cart, "Product successfully added to cart")
        })
    })
}




var removeProductCartAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("product_id", "Product Id Not Found").exists();
    req.checkBody("quantity", "Quantity Not Found").exists();
    req.checkBody("size", "Size Not Found").exists();
    req.checkBody("color", "Color Not Found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    buyerMdl.findByToken(token).lean().exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        cart={}
        cart["product_id"] = req.body.product_id
        cart["quantity"] = req.body.quantity
        cart["size"] = req.body.size
        cart["color"] = req.body.color

        data.cart.map(item => {
            if(item.product_id==cart.product_id && item.size==cart.size && item.color==cart.color){
                keyExist=true
                item.quantity = parseInt(item.quantity)-parseInt(cart.quantity)
            }
            return item
        })
        data.cart= data.cart.filter(item=> item.quantity>0)

        buyerMdl.update({"_id":data["_id"]},{$set:data}).exec((err,result)=>{
            if(err) throw err;
            res.success(data.cart, "Product successfully removed to cart")
        })
    })
}

module.exports = {
    "getCart": getCartAction,
    "emptyCart": emptyCartAction,
    "addProductCart": addProductCartAction,
    "removeProductCart": removeProductCartAction,
    //"changeQuantityCart": changeQuantityCart
};
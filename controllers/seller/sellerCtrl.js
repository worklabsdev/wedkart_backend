var sellerMdl = require("../../models/seller");
var async = require("async");
var sellerMdl = require("../../models/seller");
var messageMdl = require("../../models/messages");
var orderMdl = require("../../models/order");
var shopManagementMdl = require("../../models/shop");
const productMdl = require("../../models/product");
const smtpTransport = require("../../config/email");

const { generateRandStr } = require("../../utils/commonFuns");
const { generatePassword, verifyPassword } = require("../../utils/password");

//    Seller Login
var loginAction = async (req, res) => {
  try{
    req.checkBody("email", "Invalid Email or Phone Number").exists();
    req.checkBody("password", "Invalid Password").exists().isLength({ min: 3 });
    var errors = req.validationErrors();
    if (errors) {
      return res.status(400).failure("Errors", { errors: errors });
    }
  
    var seller = {};
    seller["email"] = req.body.email.toLowerCase().trim();
    seller["password"] = req.body.password;  

    seller = await sellerMdl.findExistence(seller["email"]);
    if(!seller)
      return res.status(400).failure("Invalid Username or Password");

    let checkPass = await verifyPassword(req.body.password, seller.password);
    if(!checkPass) return res.status(400).failure("Invalid Username or Password");

    if(!seller.isConfirmed)
      return res.status(400).failure("Please Confirm Your Account", { email: seller["email"] });
    else if(seller.isBlocked)
      return res.status(400).failure("Your Account has Been Blocked");

    var token = generateRandStr(100);

    let newData =  {
      $push: {
        token,
        logins: {
          token: token,
          valid: true,
          otherData: {}
        }
      },
      $set: {
        updatedAt: Date.now()
      }
    };

    seller = await sellerMdl.findByIdAndUpdate(seller._id, newData, { new: true });
  
    return res.status(200).success({ token, name: seller.name, email: seller.email, _id: seller._id }, "Seller Logged In successfully");

  }
  catch(err) {
    console.log(err);
    return res.status(500).failure(err.message);
  }
};

var confirmAccountAction = function (req, res) {
  req.checkParams("code", "No Confirm Code Found").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }

  var seller = {};
  seller["confirmCode"] = req.params.code.trim();

  sellerMdl.findOne(seller).exec(function (err, data) {
    if (err) throw err;
    if (!data) {
      return res
        .status(200)
        .success({}, "Invalid Confirm Code");
    }

    sellerMdl.update({ _id: data._id }, { $set: { isConfirmed: true, confirmCode: null } }).exec();
    return res
      .status(200)
      .success({}, "Seller's Account confirmed Successfully");
  });
};

var confirmTokenAction = function (req, res) {
  req.checkHeaders("x-auth-token", "No Token Found").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }

  let token = req.headers["x-auth-token"];
  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) throw err;
    if (!data) {
      return res.failure("Invalid Token");
    } else {
      return res.success({}, "Token Validated");
    }
  });
};

var detailsAction = function (req, res) {
  req.checkHeaders("x-auth-token", "No Token Found").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }

  let token = req.headers["x-auth-token"];
  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) throw err;
    if (!data) {
      return res.failure("Invalid Token");
    } else {
      delete data["password"];
      delete data["token"];
      delete data["recoverCode"];
      delete data["isDeleted"];
      delete data["isConfirmed"];
      delete data["isBlocked"];
      delete data["isApproved"];
      delete data["confirmCode"];
      delete data["logins"];
      delete data["salt"];
      return res.success(data);
    }
  });
};

//    Change Password
var changePasswordAction = async (req, res) => {
  try {
      req.checkHeaders("x-auth-token", "No Token Found").exists();
      req.checkBody("opassword", "Invalid New Password").exists();
      req.checkBody("npassword", "Invalid New Password").isLength({ min: 3 });
  
      var errors = req.validationErrors();
      if (errors) return res.status(400).failure("Errors", { errors: errors });
  
      let token = req.headers["x-auth-token"];
      let opassword = req.body.opassword;
      let npassword = req.body.npassword;
  
      let seller = await sellerMdl.newFindByToken(token);
      if(!seller)
        return res.failure("Invalid Token");
  
      let checkPass = await verifyPassword(opassword, seller.password);
      if(!checkPass) return res.status(400).failure("Incorrect Old Password");
  
      let password = await generatePassword(npassword);
  
      await sellerMdl.findByIdAndUpdate(
        seller._id,
        {
          $set : {
            password: password.hash,
            salt: password.salt,
            updatedAt: Date.now()
          }
        },
        {
          new: true
        }
      );
  
      return res.success({}, "seller Password Successfully Changed");
  }
  catch(err) {
    console.log(err);
    return res.status(500).failure(err.message);
  }
};

//  Forgot Password
var forgetPasswordAction = async (req, res) => {
  try {
      req.checkBody("email", "Invalid Email Address").isEmail();
  
      var errors = req.validationErrors();
      if (errors) return res.status(400).failure("Errors", { errors: errors });
  
      let email = req.body.email.toLowerCase().trim();
  
      let seller = await sellerMdl.findOne({email: email, isDeleted: false});
      if(!seller)
        return res.status(400).failure("Email Is not Registered With Us");
  
      var recoverCode = generateRandStr(10);
  
      await sellerMdl.findByIdAndUpdate(
        seller._id,
        {
          $set : {
            recoverCode: recoverCode,
            updatedAt: Date.now()
          }
        },
        {
          new: true
        }
      );
  
      var mailOptions = {
        to: req.body.email,
        from: "Wedding Kart",
        subject: "Forget Password, Do not Reply",
        html:
          "You are receiving this because you (or someone else) have requested the create an forget password on Colispace with this email.\n\n" +
          "Please use following code to recover your password <h1>" +
          recoverCode +
          "</h1>\n\n" +
          "If you did not request this, please ignore this email.\n"
      };
  
      smtpTransport.sendMail(mailOptions, function(err) {
        if (err)
          console.error(err, "Seller forgetPasswordAction");
      });
  
      return res.success({ email: email }, "seller Recover Token Sent On Email");
  } catch(err) {
    console.log(err);
    return res.status(500).failure(err.message);
  }
};

//    Update Password
var updatePasswordAction = async (req, res) => {
  try {
      req.checkBody("email", "Invalid Email").isEmail();
      req.checkBody("code", "Invalid Recover Code").exists();
      req.checkBody("password", "Invalid Password").exists().isLength({ min: 3 });
      var errors = req.validationErrors();
      if (errors)
        return res.status(400).failure("Errors", { errors: errors });
  
      let email = req.body.email.toLowerCase().trim();
      let code = req.body.code.trim();
      let password = req.body.password.trim();
  
      let seller = await sellerMdl.findOne({ email: email, recoverCode: code, isDeleted: false });
      if(!seller)
        return res.failure("Recover Code has Expired");
  
      password = await generatePassword(password);
  
      await sellerMdl.findByIdAndUpdate(
        seller._id,
        {
          $set : {
            password: password.hash,
            salt: password.salt,
            updatedAt: Date.now()
          }
        },
        {
          new: true
        }
      );
  
      return res.success({}, "Password Successfully Updated, Please Login Now");
  
  } catch(err) {
    console.log(err);
    return res.status(500).failure(err.message);
  }
};

var editAction = function (req, res) {
  req.checkHeaders("x-auth-token", "No Token Found").exists();
  req.checkBody("name", "Invalid Person Name").exists();
  req.checkBody("phone", "Invalid Phone Number").exists().isLength({ min: 10, max: 12 });
  req.checkBody("address", "Invalid Address").exists();
  req.checkBody("city", "Invalid country").exists();
  req.checkBody("state", "Invalid country").exists();
  req.checkBody("country", "Invalid country").exists();
  req.checkBody("pincode", "Invalid Pincode").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }

  var token = req.headers["x-auth-token"];
  var seller = {};
  seller["name"] = req.body.name.trim();
  seller["phone"] = req.body.phone.trim();
  seller["address"] = req.body.address.trim();
  seller["city"] = req.body.city.trim();
  seller["state"] = req.body.state.trim();
  seller["country"] = req.body.country.trim();
  seller["pincode"] = req.body.pincode.trim();
  async.waterfall(
    [
      callback => {
        sellerMdl.findByToken(token).exec((err, data) => {
          if (err) throw err;
          if (!data) return res.status(400).failure("Invalid Token");
          callback(null, data["_id"]);
        });
      },
      (id, callback) => {
        sellerMdl.update({ _id: id }, { $set: seller }).exec((err, data) => {
          if (err) throw err;
          callback(null);
        });
      }
    ],
    err => {
      if (err) throw err;
      return res.success({}, "seller Successfully Updated");
    }
  );
};

var profileImageAction = function (req, res) {
  req.checkHeaders("x-auth-token", "No Token Found").exists();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure({ errors: errors });
  }

  let token = req.headers["x-auth-token"];

  if (req.file) {
    sellerMdl.findByToken(token).exec(function (err, data) {
      if (err) throw err;
      if (!data) {
        return res.status(400).failure("Invalid Token");
      } else {
        let imgurl = req.headers.host + "/" + req.file.path;
        sellerMdl
          .update({ _id: data["_id"] }, { $set: { image: imgurl } })
          .exec();
        return res.success({ url: imgurl }, "seller Profile Pic Updated");
      }
    });
  } else {
    return res.status(400).failure("Image Not Found");
  }
};

//  Confirm Code
var confirmCodeAction = function(req, res) {
  try {
  
      req.checkBody("email", "Invalid Email").isEmail();
      req.checkBody("code", "Invalid Recover Code").exists();
      var errors = req.validationErrors();
      if (errors) return res.status(400).failure("Errors", { errors: errors });
  
      let email = req.body.email;
      let code = req.body.code;
  
      sellerMdl
        .findOne({ email: email, isDeleted: false })
        .exec(function(err, data) {
          if (err) throw err;
          if (!data) {
            return res.failure("Wrong Email Address");
          } else {
            if (data.recoverCode == code) {
              return res.success({}, "seller Recover Token Matched");
            } else {
              return res.status(400).failure("Wrong Recover Code");
            }
          }
        });
  
  } catch(err) {
    console.log(err);
    return res.status(500).failure(err.message);
  }
  };


var deleteprofileImageAction = function (req, res) {
  req.checkHeaders("x-auth-token", "No Token Found").exists();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure({ errors: errors });
  }
  let token = req.headers["x-auth-token"];
  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) throw err;
    if (!data) {
      return res.failure("Invalid Token");
    } else {
      sellerMdl.removeimage({ _id: data["_id"] });
      return res.success({}, "Successfully Removed");
    }
  });
};



var addShopManagement = function (req, res) {

  req.checkHeaders("x-auth-token", "Invalid Token").exists();
  req.checkBody("name", "Shop Not Found").exists();
  req.checkBody("email", "email Not Found").exists();
  req.checkBody("description", "description Not Found").exists();
  req.checkBody("status", "status Not Found").exists();
  req.checkBody("address", "address not found").exists();
  req.checkBody("country", "country not found").exists();
  req.checkBody("state", "state not found").exists();
  req.checkBody("city", "city not found").exists();
  req.checkBody("postal_code", "postal_code not found").exists();
  req.checkBody("license_no", "license_no not found").exists();
  req.checkBody("gst_no", "gst_no not found").exists();
  req.checkBody("pancard_no", "pancard_no not found").exists();
  req.checkBody("contact_no", "contact_no not found").exists();
  req.checkBody("shopkeeper_name", "shopkeeper_name not found").exists();
  req.checkBody("shopkeeper_no", "shopkeeper_no not found").exists();
  req.checkBody("open_time", "open_time not found").exists();
  req.checkBody("close_time", "close_time not found").exists();

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).failure("Errors", {
      "errors": errors
    })
  }

  var token = req.headers["x-auth-token"]

  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) throw err;
    if (!data) {
      return res.status(400).failure("Invalid Token")
    } else {
      var shopManagement = new shopManagementMdl()
      for (let i in req.files) {
        if (req.files[i]) {

          // if(i==0){
          //    req.files[i]["document_name"]="shopLogo"

          // }else if(i==1){
          //     req.files[i]["document_name"]="gstCertificate"

          // }else if(i==2){
          //     req.files[i]["document_name"]="cancelCheck"

          // }else if(i==3){
          //     req.files[i]["document_name"]="PanCard"

          // }else if(i==4){
          //     req.files[i]["document_name"]="shopCertificate"

          // }
          let imgurl = req.headers.host + "/" + req.files[i].path
          shopManagement["shopManagementPic"][i] = imgurl;
          console.log("aaaaaaaaaaaaa", shopManagement["shopManagementPic"])
        } else {
          return res.status(400).failure("Invalid shop Image")
        }
      }

      shopManagement["user_id"] = data._id
      shopManagement["name"] = req.body.name
      shopManagement["email"] = req.body.email
      shopManagement["description"] = req.body.description
      shopManagement["banner"] = req.body.banner
      shopManagement["status"] = req.body.status
      shopManagement["address"] = req.body.address
      shopManagement["country"] = req.body.country
      shopManagement["state"] = req.body.state
      shopManagement["city"] = req.body.city
      shopManagement["postal_code"] = req.body.postal_code
      shopManagement["license_no"] = req.body.license_no
      shopManagement["gst_no"] = req.body.gst_no
      shopManagement["images"] = []
      shopManagement["pancard_no"] = req.body.pancard_no;
      shopManagement["contact_no"] = req.body.contact_no;
      shopManagement["shopkeeper_name"] = req.body.shopkeeper_name
      shopManagement["shopkeeper_no"] = req.body.shopkeeper_name
      shopManagement["open_time"] = req.body.shopkeeper_name
      shopManagement["close_time"] = req.body.shopkeeper_name
      console.log(shopManagement["shopManagementPic"])

      shopManagement.save()

      return res.status(200).success({}, "shop Succesfully Added")

    }
  })
}
//}


function allShopAction(req, res) {
  // req.checkHeaders("x-auth-token", "Invalid Token").exists();
  // req.checkBody("seller_id","Invalid seller id").exist();
  // var errors = req.validationErrors();
  // if (errors) {
  //     return res.status(400).failure("Errors", {
  //         "errors": errors
  //     })
  // }
  // var token = req.headers["x-auth-token"].trim();

  // sellerMdl.findByToken(token).exec(function(err, data) {
  //     if (err) throw err;
  //     if (!data) return res.status(400).failure("No User found.")
  //     if (data) {
  shopManagementMdl.find({ "user_id": req.body.seller_id }).exec((err, allshops) => {
    if (err) throw err;
    return res.status(200).success(allshops)
  })
  //     }
  // })
}


function getShopAction(req, res) {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();
  req.checkParams("id", "Invalid shop Id").exists();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      "errors": errors
    })
  }

  shopManagementMdl.findOne({
    "_id": req.params.id
  }).exec(function (err, data) {
    if (err) throw err;
    if (!data) return res.status(400).failure("No Such Shop Found")
    return res.status(200).success(data)
  }, (err) => {
    if (err) {
      console.log(err)
    }

  })

}


var editShopAction = function (req, res) {

  var shopManagement = {}
  req.checkHeaders("x-auth-token", "Invalid Token").exists();
  req.checkBody("name", "Shop Not Found").exists();
  req.checkBody("email", "email Not Found").exists();
  req.checkBody("description", "description Not Found").exists();
  req.checkBody("status", "status Not Found").exists();
  req.checkBody("address", "address not found").exists();
  req.checkBody("country", "country not found").exists();
  req.checkBody("state", "state not found").exists();
  req.checkBody("city", "city not found").exists();
  req.checkBody("postal_code", "postal_code not found").exists();
  req.checkBody("license_no", "license_no not found").exists();
  req.checkBody("gst_no", "gst_no not found").exists();
  req.checkBody("pancard_no", "pancard_no not found").exists();
  req.checkBody("contact_no", "contact_no not found").exists();
  req.checkBody("shopkeeper_name", "shopkeeper_name not found").exists();
  req.checkBody("shopkeeper_no", "shopkeeper_no not found").exists();
  req.checkBody("open_time", "open_time not found").exists();
  req.checkBody("close_time", "close_time not found").exists();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      "errors": errors
    })
  }



  var token = req.headers["x-auth-token"]

  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) throw err;
    if (!data) {
      return res.status(400).failure("Invalid Token")
    } else {

      shopManagementMdl.findOne({ "user_id": data["_id"], "_id": req.params.id }).exec((err, shopManagement) => {
        if (err) throw err;
        console.log(shopManagement)
        console.log(req.files.length)
        console.log(req.files[0])
        for (let i in req.files) {
          if (req.files[i]) {

            console.log(i)
            let imgurl = req.headers.host + "/" + req.files[i].path
            console.log(imgurl)
            shopManagement.shopManagementPic[i] = imgurl;
            console.log(shopManagement["shopManagementPic"])
          } else {
            return res.status(400).failure("Invalid shop Image")
          }
        }
        shopManagement["user_id"] = data._id
        shopManagement["name"] = req.body.name
        shopManagement["email"] = req.body.email
        shopManagement["description"] = req.body.description
        shopManagement["banner"] = req.body.banner
        shopManagement["status"] = req.body.status
        shopManagement["address"] = req.body.address
        shopManagement["country"] = req.body.country
        shopManagement["state"] = req.body.state
        shopManagement["city"] = req.body.city
        shopManagement["postal_code"] = req.body.postal_code
        shopManagement["license_no"] = req.body.license_no
        shopManagement["gst_no"] = req.body.gst_no
        shopManagement["images"] = []
        shopManagement["pancard_no"] = req.body.pancard_no;
        shopManagement["contact_no"] = req.body.contact_no;
        shopManagement["shopkeeper_name"] = req.body.shopkeeper_name
        shopManagement["shopkeeper_no"] = req.body.shopkeeper_name
        shopManagement["open_time"] = req.body.shopkeeper_name
        shopManagement["close_time"] = req.body.shopkeeper_name
        shopManagementMdl.update({
          "_id": req.params.id
        }, {
            $set: shopManagement
          }).exec((err, data) => {
            if (err) throw err
            return res.status(200).success({ data }, "Shop Succesfully Updated")
          })
      })
    }
  })

}



function deleteShopAction(req, res) {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();
  req.checkParams("id", "Invalid shop Id").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      "errors": errors
    })
  }

  var token = req.headers["x-auth-token"].trim();

  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) {
      return res.status(400).failure(err)
    } else if (!data) {
      return res.status(400).failure("No Such account Found")
    } else {

      shopManagementMdl.remove({
        "_id": req.params.id
      }).exec(function (err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No Such Shop Found")
        return res.status(200).success({}, "Shop Deleted Succesfully")
      })

    }
  })
}



var analytics = (req, res, next) => {

  req.checkHeaders("x-auth-token", "Invalid Token").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      "errors": errors
    })
  }
  var token = req.headers["x-auth-token"].trim();
  sellerMdl.findByToken(token).exec(function (err, data) {
    if (err) throw err;
    if (!data) return res.status(400).failure("No User found.")
    let filterObj = { sellerId: data["_id"] }
    if (req.body.status) filterObj["status"] = req.body.status
    let id = data["_id"];
    const aggregatorOpts = [{
      $match: { "sellerId": String(id) }
    }, {
      $group: {
        _id: "$status",
        status: { "$first": "$status" },
        count: { $sum: 1 },
        amount: { $sum: "$total_amount" }
      }
    }]
    //get order count and delivered order amount
    orderMdl.aggregate(aggregatorOpts).exec((err, data) => {

      if (err) throw err;
      let obj = {};
      data.forEach((item) => {
        obj[item.status] = {
          "count": item.count,
          "amount": item.amount
        }
      });
      if(!obj.Pending){
        obj.Pending = {
          count:0,
          amount:0
        }
      }
      if(!obj.Completed){
        obj.Completed = {
          count:0,
          amount:0
        }
      }
      //get product count
      productMdl.count({ "seller.id": id }).exec((err, data) => {
        if (err) throw err;
        obj.Products = {"count":data};

        
        

        //get delivered orders grouped by year and month
        const operator = [{
          $match: { "sellerId": String(id) }
        }, {
          $group: {
            _id: { year: { $year : "$createdAt" }, month: { $month : "$createdAt" }},
            count:{ $sum: 1},
            earnings:{$sum:"$total_amount"}
          }
        }]

        orderMdl.aggregate(operator).exec((err, sales) => {

          // get timeline of past 12 months
        let timeline = [];
        let date = new Date();
        for(let i=0;i<12;i++){
          let obj = {
            year:date.getFullYear(),
            month:date.getMonth()+1
          }
          // match each sale result with timeline and append count with it.
          let result = sales.find((sale)=>{
              return (obj.year == sale._id.year && obj.month==sale._id.month);
          })
          if(result){
            obj.count = result.count;
            obj.earnings = result.earnings;
          }else{
            obj.count = 0;
          }
          timeline.push(obj);
          date.setMonth(date.getMonth()-1);
        }
        obj.timeline = timeline;

          res.success(obj);

        });


        
      });
    });


  })

}


var messages = (req, res, next) => {

  req.checkHeaders("x-auth-token", "Invalid Token").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      "errors": errors
    })
  }
  var token = req.headers["x-auth-token"].trim();

  sellerMdl.findByToken(token).exec(function (err, seller) {
    if (err) throw err;
    if (!seller) return res.status(400).failure("No User found.");

    let query = { sellerId: seller["_id"] }

    messageMdl.find(query).populate("buyerId").sort("createdAt").exec(function (err, messages) {
      if (err) throw err;
      return res.status(200).success(messages);
    })

  })



}

var singleConvoAction = (req,res) => {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();

  var errors = req.validationErrors();
  if (errors) {
      return res.status(400).failure("Errors", {
          "errors": errors
      })
  }
  var token = req.headers["x-auth-token"].trim();
  sellerMdl.findByToken(token).exec(function(err, data) {
      if (err) throw err;
      if (!data) return res.status(400).failure("No User found.")

      messageMdl.find({buyerId:req.body.buyerId,sellerId:data["_id"]}).sort({"createdAt":"desc"}).exec((err,messages)=>{
          if(err) throw err;
          res.success(messages)
      })
  })
}



module.exports = {
  addShop: addShopManagement,
  allShop: allShopAction,
  getShop: getShopAction,
  editShop: editShopAction,
  deleteShop: deleteShopAction,
  login: loginAction,
  confirmAccount: confirmAccountAction,
  confirmToken: confirmTokenAction,
  edit: editAction,
  confirmCode: confirmCodeAction,
  details: detailsAction,
  changePassword: changePasswordAction,
  forgetPassword: forgetPasswordAction,
  updatePassword: updatePasswordAction,
  profileImage: profileImageAction,
  deleteprofileimage: deleteprofileImageAction,
  analytics: analytics,
  messages: messages,
  singleConvo:singleConvoAction
};
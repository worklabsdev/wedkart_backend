var sellerMdl = require("../../models/seller");
var buyerMdl = require("../../models/buyer");
var orderMdl = require("../../models/order");

var allOrderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    sellerMdl.findByToken(token).exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        let filterObj = {sellerId:data["_id"]}
        if(req.body.status) filterObj["status"]=req.body.status

        orderMdl.find(filterObj).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            
                res.success(data)
        })
    })
}



var orderAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkParams("id", "Order Id Not Found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    sellerMdl.findByToken(token).exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        orderMdl.findOne({_id:req.params.id}).populate("products.product_id").lean().exec((err,data)=>{
            if(err) throw err;
            buyerMdl.findOne({"_id":data["buyerId"]}).exec((err,buyerData)=>{
                if(err) throw err
                data["buyer"] = buyerData
                res.success(data)
            })
        })
    })
}


var changeStatusAction = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();
    req.checkBody("id", "Order Id Not found").exists();
    req.checkBody("status", "Status Not found").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();

    sellerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        console.log("request status",req.body.status);
        orderMdl.update({_id:req.body.id},{$set:{status:req.body.status}}).exec()
        let message = "";
        if(req.body.status=="Completed"){
            message = "Completed";
        }else if(req.body.status=="Cancelled"){
            message = "Cancelled";
        }
        res.success({},`Order Succesfully ${message}`);
    })
}

var cancelRequestOrders = function(req,res){
    req.checkHeaders("x-auth-token", "Invalid Token").exists();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).failure("Errors", {
            "errors": errors
        })
    }
    var token = req.headers["x-auth-token"].trim();
    sellerMdl.findByToken(token).populate("cart.product_id").exec(function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No User found.")
        
        orderMdl.find({sellerId:data["_id"],status:"Cancel_Request"}).populate("products.product_id").exec((err,data)=>{
            if(err) throw err;
            console.log(data)
            res.success(data)
        })

    })
}

module.exports = {
    "allOrders":allOrderAction,
    "orderDetails":orderAction,
    "changeStatus":changeStatusAction,
    "cancelRequestOrders":cancelRequestOrders
};
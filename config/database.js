const slug = require("mongoose-slug-generator");
const mongoose = require("mongoose");

mongoose.plugin(slug);
mongoose.Promise = global.Promise;
mongoose.connect(appConfig.db.url, { useMongoClient: true });

var db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

db.on("open", err => {
  if (err) {
    return err;
  } else {
    console.log("Database Connected");
  }
});

module.exports.mongoose = mongoose;
module.exports.db = db;

const Joi = require("@hapi/joi");
var mongoose = require("mongoose");

const defaultGeoSchema = {
	"type": "Point",
	"coordinates": [0.0, 0.0]
};
exports.defaultGeoSchema = defaultGeoSchema;

const geoMongooseSchema = new mongoose.Schema({
	type: {
		type: String,
		enum: ["Point"],
		required: true,
		default: "Point"
	},
	coordinates: {
		type: [Number],
		index: "2dsphere",
		required: false,
		default: [0, 0]
	}
});
exports.geoMongooseSchema = geoMongooseSchema;


const geoJoiSchema = Joi.object().keys({
	"type": "Point",
	"coordinates": Joi.array().items(Joi.number().precision(5).required()).min(2).optional()
});
exports.geoJoiSchema = geoJoiSchema;

module.exports = {
	"appName": process.env.APP_NAME,
	"baseURL": process.env.BASE_URL,
	"env": "production",
	"host": "localhost",
	"port": 8080,
	"db": {
        "url": process.env.MONGO_URL
	},
	"emailConfig": {
		"name": "gmail",
		"gmail": {
			"host": 'smtp.gmail.com',
			"port": 587,
			"secure": false,
			"requireTLS": false,
			"auth": {
				"user": process.env.SMTP_EMAIL || "test@worklabstech.com",
				"pass": process.env.SMTP_PASSWORD || "testmail@123"
			}
		}
	},
	"upload": {
		"name": "spaces",
		"spaces": {
			"accessKeyId": process.env.SPACES_ACCESS_KEY_ID,
			"secretAccessKey": process.env.SPACES_SECRET_ACCESS_KEY,
			"endpoint": process.env.SPACES_ENDPOINT,
			"bucket": process.env.SPACES_BUCKET,
			"folder": process.env.SPACES_FOLDER,
			"filesURL": process.env.SPACES_FILES_URL
		},
		"s3": {
			"accessKeyId": process.env.S3_ACCESS_KEY_ID,
			"secretAccessKey": process.env.S3_SECRET_ACCESS_KEY,
			"endpoint": process.env.S3_ENDPOINT,
			"bucket": process.env.S3_BUCKET,
			"folder": process.env.S3_FOLDER,
			"filesURL": process.env.S3_FILES_URL
		}
	},
	"errorSettings": {
		showError: 1,
		mail: 0,
		log: {
			request: 1,
			validation: 1,
			response: 1,
			info: 1,
			error: 1,

			socket: {
				info: 1,
				error: 1
			},

			api: {
				request: 1,
				validation: 1,
				response: 1,
				info: 1,
				error: 1
			},

			push: {
				info: 1,
				error: 1
			},

			cron: {
				info: 1,
				error: 1
			},

			mail: {
				info: 1,
				error: 1
			}
		}
	},
	"auth": {
		"jwtKey": process.env.JWT_KEY || "asdfmnbvc",
		"jwtValidity": "24h"
	},
	"sms": {
		"name": "twilio",
		"otpValidityMinutes": 20,
		"twilio": {
			"accountSid": process.env.TWILIO_ACCOUNT_SID || null,
			"authToken": process.env.TWILIO_AUTH_TOKEN || null,
			"number": process.env.TWILIO_NUMBER || null
		}
	},
	"payment": {
		"name": "razorPay",
		"razorPay": {
			public: process.env.RAZOR_KEY,
			secret: process.env.RAZOR_SECRET,
			url: "https://api.razorpay.com/v1/"
		}
	}
};
const notificationModel = require("../models/notifications");

const { sellerPopProject, buyerPopProject, productPopProject, categoryPopProject, shopPopProject, inquiryPopProject } = require("./commonServices").projectionPopulate;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const defaultSort = {"_id": -1};

const projections = {
	notificationsListProjection: {
		isDeleted: 0,
		__v: 0,
		updatedAt: 0
	},
	detailsProjection: {
		__v: 0
	}
};

//  Messages
let notificationServiceMessages = {
	"NotificationUpdateSuccess": "Notification updated successfully",
};
exports.notificationServiceMessages = notificationServiceMessages;

//  Types
const notTypes = {
  
  admin: {
    "inquiryCreate": "InquiryCreate",
    "paymentCreate": "PaymentCreate"
  },

  seller: {
    "inquiryCreate": "InquiryCreate",
    "paymentCreate": "PaymentCreate"
  },

};
exports.notTypes = notTypes;

//  Sender Receiver Types
const notSenderRecTypes = {
  "seller": "Seller",
  "buyer": "Buyer",
  "admin": "Admin"
};
exports.notSenderRecTypes = notSenderRecTypes;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Listing
const notsList = (whereJSON, projects=projections.notificationsListProjection, opts) => {

	return notificationModel.find(
			whereJSON,
			projects,
			{ skip: opts.skip, limit: opts.limit }
	)
	.sort(opts.sort || defaultSort)
	.populate([
    sellerPopProject,
    buyerPopProject,
    productPopProject,
    categoryPopProject,
    shopPopProject,
    inquiryPopProject
	]);

};
exports.notsList = notsList;

//      Common Filtering Where Creator
const notsCommonWhereCreator = (whereJSON, data) => {

    if(data.seller) {
        whereJSON["$and"].push({
          "seller": data.seller
        }); 
    }
    if(data.buyer) {
        whereJSON["$and"].push({
          "buyer": data.buyer
        }); 
    }

    if(data.admin) {
        whereJSON["$and"].push({
          "admin": data.admin
        }); 
    }

    if(data.shop) {
        whereJSON["$and"].push({
          "shop": data.shop
        }); 
    }
    if(data.product) {
        whereJSON["$and"].push({
          "product": data.product
        }); 
    }

    if(data.inquiry) {
        whereJSON["$and"].push({
          "inquiry": data.inquiry
        }); 
    }

    if(data.category) {
        whereJSON["$and"].push({
          "category": data.category
        }); 
    }
    if(data.subCategory) {
        whereJSON["$and"].push({
          "subCategory": data.subCategory
        }); 
    }

    if(data.senderType) {
        whereJSON["$and"].push({
          "senderType": data.senderType
        }); 
    }
    if(data.receiverType) {
        whereJSON["$and"].push({
          "receiverType": data.receiverType
        }); 
    }
    if(data.notificationTypes) {
        whereJSON["$and"].push({
          "notificationType": { $in: JSON.parse(data.notificationTypes) }
        }); 
    }

    if(data.unreadOnlyFilter) {
      whereJSON["$and"].push({
        "isRead": false
      }); 
    }

    if(data.search) {
      whereJSON["$or"] = [
        { message: { $regex: data.search, $options: "i" } },
        { description: { $regex: data.search, $options: "i" } }
      ]
    }

	return whereJSON;

};
exports.notsCommonWhereCreator = notsCommonWhereCreator;
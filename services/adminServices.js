const { authHandler } = require("../utils/responseHandler");
const { authCaseTypes } = require("../utils/auth/constants");

var adminMdl = require("../models/admin");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const adminFeeTypes = {
	"value": "Value",
	"percent": "Percentage"
};
exports.adminFeeTypes = adminFeeTypes;

const defaultAdminFee = {
	amount: 0,
	value: 0,
	type: adminFeeTypes.value,
	description: "",
};
exports.defaultAdminFee = defaultAdminFee;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Admin Socket Ids Extract   //
const adminSocketsIdsExtract = () => {

	let socketIds = [];
    return new Promise(async (resolve, reject) => {
		try {

			let admins = await adminMdl.find(
				{
					isDeleted: false,
					socketId: { $ne: null }
				},
				{
					socketId: 1
				}
			);

			admins.forEach((admin, key) => {
				socketIds.push(admin.socketId);
			});

			return resolve(socketIds);

		}
		catch(error) {
			return resolve(socketIds);
		}
    });

};
exports.adminSocketsIdsExtract = adminSocketsIdsExtract;

//		Admin Auth Getter
const adminSignInDetailsAuth = (response, token) => {
    return new Promise(async (resolve, reject) => {
		try {

            let admin = await adminMdl.findOne({token});

            if (!admin) return authHandler(response, authCaseTypes.NOT_VALID_TOKEN);
			else if (admin.isBlocked) return authHandler(response, authCaseTypes.ACCOUNT_SUSPENDED);

            return resolve(admin);
		} catch (error) {
			return reject(error);
		}
	});
}
exports.adminSignInDetailsAuth = adminSignInDetailsAuth;

//	Admin Find One
const getOneAdmin = (whereJSON, projections={}, options={}) => {
	return adminMdl.findOne(whereJSON, projections, options);
};
exports.getOneAdmin = getOneAdmin;
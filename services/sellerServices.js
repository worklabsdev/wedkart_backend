const { authHandler, failedActionHandler } = require("../utils/responseHandler");
const { authCaseTypes } = require("../utils/auth/constants");

var sellerMdl = require("../models/seller");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const sellerMessages = {
	"AccountNotFound": "Sorry these credentials are incorrect",
	"SellerNotAvail": "Sorry, this seller is currently not available",
	"VerifyAccountFirst": "Please, confirm your account first",
	"LogInSuccess": "Seller Logged In successfully",
	"EmailAlreadyTaken": "Sorry, this email is already taken",
	"GoogleAlreadyTaken": "Sorry, this google account is already taken",
	"FacebookAlreadyTaken": "Sorry, this facebook account is already taken",
	"SellerRegisterSuccess": "Successfully Registered, Please confirm Account on email",
	"AccountVerifiedSuccess": "Account verified successfully",

	"BussinessInfoUpdatedMsg": "Bussiness info updated successfully"
};
exports.sellerMessages = sellerMessages;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//		Seller Auth Getter
const sellerSignInDetailsAuth = (response, token) => {
    return new Promise(async (resolve, reject) => {
		try {

            let seller = await sellerMdl.newFindByToken(token);

            if (!seller) return authHandler(response, authCaseTypes.NOT_VALID_TOKEN);
			else if (seller.isBlocked) return authHandler(response, authCaseTypes.ACCOUNT_SUSPENDED);

            return resolve(seller);
		} catch (error) {
			return reject(error);
		}
	});
}
exports.sellerSignInDetailsAuth = sellerSignInDetailsAuth;

//  Seller Get
const sellerGetService = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
			let seller = await sellerMdl.findOne(findJSON, projections, options);

			if (!seller)
				return failedActionHandler(response, sellerMessages.SellerNotAvail);

			return resolve(seller);
		} catch (error) {
			return reject(error);
		}
	});
};
exports.sellerGetService = sellerGetService;

//	///			Seller Get
exports.sellerGet = (findJSON, populate=null) => {
	return sellerMdl.findOne(findJSON, populate);
};

//	///			Seller Create
exports.createSeller = (data) => {
	return sellerMdl.create(data);
};

//	///	Update
exports.sellerUpdateSer = (findJSON, newData, options={new: true}) => {
	return sellerMdl.findOneAndUpdate(findJSON, newData, options);
};

//	///	Seller Profile Creator
exports.sellerProfileCreatorSer = (seller) => {
	let { password, salt, socketIds, creator, logins, token, updatedAt, confirmCode, isBlocked, isDeleted, __v, ...rest } = seller;

	return rest;
};
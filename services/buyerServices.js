const { authHandler, failedActionHandler } = require("../utils/responseHandler");
const { authCaseTypes } = require("../utils/auth/constants");
const { generateRandStr } = require("../utils/commonFuns");
const { generatePassword } = require("../utils/password");

var buyerMdl = require("../models/buyer");

const buyerMessages = {
	"AccountNotFound": "Sorry these credentials are incorrect",
	"BuyerNotAvail": "Sorry, this buyer is currently not available",
	"VerifyAccountFirst": "Please, confirm your account first",
	"LogInSuccess": "Buyer Logged In successfully",
	"EmailAlreadyTaken": "Sorry, this email is already taken",
	"GoogleAlreadyTaken": "Sorry, this google account is already taken",
	"FacebookAlreadyTaken": "Sorry, this facebook account is already taken",
	"BuyerRegisterSuccess": "Successfully Registered, Please confirm Account on email",
	"AccountVerifiedSuccess": "Account verified successfully",
	"ProfileUpdateSuccess": "Profile updated successfully",
	"ProfileUpdateNewEmailSuccess": "Profile updated successfully. Please verify your email.",
	
};
exports.buyerMessages = buyerMessages;

const buyerCreatorTypes = {
	self: "Self",
	sellerInquiry: "SellerInquiryCreate",
	buyerInquiry: "BuyerInquiryCreate"
};
exports.buyerCreatorTypes = buyerCreatorTypes;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//		Buyer Auth Getter
const buyerSignInDetailsAuth = (response, token) => {
    return new Promise(async (resolve, reject) => {
		try {
          let buyer = await buyerMdl.newFindByToken(token);

          if (!buyer) return authHandler(response, authCaseTypes.NOT_VALID_TOKEN);
					else if (buyer.isBlocked) return authHandler(response, authCaseTypes.ACCOUNT_SUSPENDED);

          return resolve(buyer);
		} catch (error) {
			return reject(error);
		}
	});
}
exports.buyerSignInDetailsAuth = buyerSignInDetailsAuth;


//  Buyer Get
const buyerGetService = (response, findJSON, projections={}, options={}) => {
    
    return new Promise(async (resolve, reject) => {
		try {
			let buyer = await buyerMdl.findOne(findJSON, projections, options);

			if (!buyer)
				return failedActionHandler(response, buyerMessages.BuyerNotAvail);

			return resolve(buyer);
		} catch (error) {
			return reject(error);
		}
	});
};
exports.buyerGetService = buyerGetService;

//	///			Buyer Get
exports.buyerGet = (findJSON, populate=null, options={lean: true}) => {
	return buyerMdl.findOne(findJSON, populate, options);
};

//	///			Buyer Create
const createBuyer = (data) => {
	return buyerMdl.create(data);
};
exports.createBuyer = createBuyer;

//	///		Buyer Create or Get
exports.buyerGetOrCreatePlusMail = (data, creatorType) => {

	if(!data.options)
		data.options = {
			lean: true
		};
	return buyerMdl.findOne(data.whereJSON, data.projections, data.options)
	.then(async (buyer) => {
	try {
		if(buyer)
			{
				buyer = JSON.parse(JSON.stringify(buyer))
				buyer.newBuyer = false;
				return buyer;
			}

		data.decodedPass = generateRandStr(20);

		const password = await generatePassword(data.decodedPass);

		data.password = password.hash;
		data.salt = password.hash;

		data.creator = {
			type: creatorType,
			otherData: {
				seller: data.seller,
				product: data.product
			}
		};

		data.isConfirmed = true;

		buyer = await createBuyer(data);
		buyer = JSON.parse(JSON.stringify(buyer));

		buyer.newBuyer = true;
		return buyer;

	}
	catch(error) {
		throw error;
	}
	})
	.catch((error) => {
		throw error;
	});

};

//	///	Update
exports.buyerUpdateSer = (findJSON, newData, options={new: true}) => {
	return buyerMdl.findOneAndUpdate(findJSON, newData, options);
};

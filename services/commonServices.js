///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const skip = 0;
const limit = 100;

//  /// Pagination Offset Creator
const pageOffsetCreator = (data) => {

    data.skip = parseInt(data.skip || skip);
    data.limit = parseInt(data.limit || limit);

    return data;
};
exports.pageOffsetCreator = pageOffsetCreator;

const genderTypes = {
	"male": "Male",
	"female": "Female",
	"notDisclosed": "Not Disclosed"
};
exports.genderTypes = genderTypes;


//      Common Populate Projections
const projectionPopulate = {
    sellerPopProject: { path: "seller", select: "_id name email phone image" },
    sellerPopProjectFull: { path: "seller", select: "-token -logins -__v -creator" },
    sellerPopProject4Notifications: { path: "seller" },

    creatorSellerPopProject: { path: "creatorSeller", select: "_id name email phone image" },
    productSellerOpProject: { path: "seller.id", select: "_id name email phone image" },

    buyerPopProject: { path: "buyer", select: "_id name email phone image" },
    buyerPopProjectFull: { path: "buyer", select: "-token -logins -__v -creator -wishlist -cart" },
    buyerPopProject4Notifications: { path: "buyer" },
    creatorBuyerPopProject: { path: "creatorBuyer", select: "_id name email phone image" },

    adminPopProject: { path: "admin", select: "_id firstname lastname email" },
    creatorAdminPopProject: { path: "creatorAdmin", select: "_id firstname lastname email" },

    shopPopProject: { path: "shops", select: "_id name email" },
    productPopProject: { path: "product", select: "_id name slug mainImage isFeatured" },

    categoryPopProject: { path: "category", select: "_id name"},
    subCategoryPopProject: { path: "subCategory", model: "categories.subcategories", select: "_id name"},

    inquiryPopProject: { path: "inquiry", select: "_id email name phoneCode phoneNumber" },

    productSellerOpProjectFull: { path: "seller.id", select: "-logins -token -confirmCode -password -salt -__v -updatedAt -createdAt -creator" },
    categoryPopProjectFull: { path: "category", select: "-subcategories -creator -__v -updatedAt -createdAt" },

};
exports.projectionPopulate = projectionPopulate;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Seller or Buyer Socket Extract   //
const extractUserSocketIds = (logins) => {
    let socketIds = [];

    logins.forEach((login, key) => {
      if(login.valid && login.socketId)
        socketIds.push(login.socketId);
    });

    return socketIds;
}
exports.extractUserSocketIds = extractUserSocketIds;
//  Seller or Buyer Socket Extract   //
const paymentModel = require("../models/payments");

const { failedActionHandler } = require("../utils/responseHandler");

const { sellerPopProject, buyerPopProject, creatorBuyerPopProject, productPopProject, categoryPopProject, shopPopProject } = require("./commonServices").projectionPopulate;

const { projectionPopulate } = require("./commonServices");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const defaultSort = {"_id": -1};

const projections = {
    paymentListProjection: {

        category: 0,
        subCategory: 0,
        shop : 0,
        buyerDetails: 0,

        subAmount: 0,
        subAmountComplete: 0,
        shippingCharges: 0,

        taxes: 0,
        adminFee: 0,
        sellerAmount: 0,
        refund: 0,
        paymentDetails: 0,
        gatewayDetails: 0,
        gatewayFee: 0,
        startEndDiffEnd: 0,
        isDeleted: 0,
        isBlocked: 0,
        gatewayLogs: 0,

        __v: 0
    },
    detailsProjection: {
        gatewayLogs: 0,
        __v: 0,
        buyerDetails: 0,
        paymentDetails: 0
    }
};

//  Payment Statuses
const paymentStatuses = {
    "pending": "Pending",
    "success": "Success",
    "failed": "Failed",
    "cancelled": "Cancelled",
    "disputed": "Disputed",
    "refunded": "Refunded",
    "partialRefunded": "PartialRefunded",
    "failedSignature": "FailedSignature"
};
exports.paymentStatuses = paymentStatuses;

//  Payment Types
const paymentTypes = {
    razorPay: "razorPay",
    cash: "cash"
};
exports.paymentTypes = paymentTypes;

const paymentConstants = {
    minMonths: 1
};
exports.paymentConstants = paymentConstants;

const paymentMessages = {
    "MinMonthError": `Min ${paymentConstants.minMonths} days are required`,
    "PaymentNotAvail": "Sorry, this payment is currently not available",
    "ProductPurchaseSuccess": "Product purchased successfully",
    "ProductPurchaseInitiated": "Product purchase initiated successfully",
    "InvalidSignature": "Invalid Signature provided, payment has failed, Please try gain"
};
exports.paymentMessages = paymentMessages;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Create 
const createPayment = (data) => {
    return paymentModel.create(data);
};
exports.createPayment = createPayment;

//  Payment Count
const countPayments = (whereJSON) => {
    return paymentModel.count(whereJSON);
};
exports.countPayments = countPayments;

//  Payment List
const listPayments = (whereJSON, projects=projections.paymentListProjection, opts) => {

    return paymentModel.find(
        whereJSON,
        projects,
        { skip: opts.skip, limit: opts.limit }
    )
    .sort(opts.sort || defaultSort)
    .populate([
        sellerPopProject,
        buyerPopProject,
        productPopProject
    ]);

};
exports.listPayments = listPayments;

//  Payment Check
const paymentCheck = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
			let payment = await paymentModel.findOne(findJSON, projections, options).lean();

			if (!payment)
				return failedActionHandler(response, paymentMessages.PaymentNotAvail);

			return resolve(payment);
		} catch (error) {
			return reject(error);
		}
	});
};
exports.paymentCheck = paymentCheck;

//  Details Get
const paymentDetails = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
            let payment = await paymentModel.findOne(findJSON, projections, options)
            .populate([
                sellerPopProject,
                buyerPopProject,
                creatorBuyerPopProject,
                productPopProject,
                categoryPopProject,
                shopPopProject
            ]);

			if (!payment)
				return failedActionHandler(response, paymentMessages.PaymentNotAvail);

			return resolve(payment);
		} catch (error) {
			return reject(error);
		}
	});
};
exports.paymentDetails = paymentDetails;

//  Update
const paymentUpdate = (whereJSON, newData, opts={new: true}) => {
    return paymentModel.findOneAndUpdate(whereJSON, newData, opts);
};
exports.paymentUpdate = paymentUpdate;

//      Common Filtering Where Creator
const paymentListingCommonWhereCreator = (whereJSON, data) => {

    if(data.sellers) {
        whereJSON["$and"].push({
            "seller": { $in: JSON.parse(data.sellers) }
        }); 
    }
    if(data.buyers) {
        whereJSON["$and"].push({
            "buyer": { $in: JSON.parse(data.buyers) }
        }); 
    }

    if(data.shops) {
        whereJSON["$and"].push({
            "shop": { $in: JSON.parse(data.shops) }
        }); 
    }
    if(data.products) {
        whereJSON["$and"].push({
            "product": { $in: JSON.parse(data.products) }
        }); 
    }

    if(data.categorys) {
        whereJSON["$and"].push({
            "category": { $in: JSON.parse(data.categorys) }
        }); 
    }
    if(data.subCategorys) {
        whereJSON["$and"].push({
            "subCategory": { $in: JSON.parse(data.subCategorys) }
        }); 
    }

    if(data.statuses) {
        whereJSON["$and"].push({
            "paymentStatus": { $in: JSON.parse(data.statuses) }
        }); 
    }

    if(data.paymentMethods) {
        whereJSON["$and"].push({
            "paymentMethod": { $in: JSON.parse(data.paymentMethods) }
        }); 
    }

    // if(data.search) {
    //     whereJSON["$or"] = [
    //       { name: { $regex: data.search, $options: "i" } },
    //       { email: { $regex: data.search, $options: "i" } },
    //       { phoneNumber: { $regex: data.search, $options: "i" } },
    //       { description: { $regex: data.search, $options: "i" } }
    //     ]
    // }

    return whereJSON;

};
exports.paymentListingCommonWhereCreator = paymentListingCommonWhereCreator;

//  Payment Get Details
const paymentDetailsGet = (whereJSON, opts={}) => {

    let populates;
    if(opts.fullPops) {
        populates = [
            projectionPopulate.categoryPopProject,
            projectionPopulate.sellerPopProjectFull,
            projectionPopulate.buyerPopProjectFull,
            projectionPopulate.productPopProject,
            projectionPopulate.shopPopProject       
        ]
    }
    else {
        populates = [
            projectionPopulate.categoryPopProject,
            projectionPopulate.sellerPopProject,
            projectionPopulate.buyerPopProject,
            projectionPopulate.productPopProject,
            projectionPopulate.shopPopProject
        ]
    }

    return paymentModel.findOne(
        whereJSON,
        projections.detailsProjection
    )
    .populate(populates);
}
exports.paymentDetailsGet = paymentDetailsGet;
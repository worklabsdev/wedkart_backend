const inquiryModel = require("../models/inquirys");

const { failedActionHandler } = require("../utils/responseHandler");

const { sellerPopProject, buyerPopProject, creatorBuyerPopProject, productPopProject, categoryPopProject, shopPopProject, sellerPopProject4Notifications, buyerPopProject4Notifications } = require("./commonServices").projectionPopulate;

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const defaultSort = {"_id": -1};

const projections = {
    inquiryListProjection: {
        isBuyerDeleted: 0,
        isSellerDeleted: 0,
        __v: 0,
        otp: 0,
        jwtToken: 0,
        response: 0
        // seller: 0,
        // product: 0,
        // shop: 0,
        // buyer: 0,
        // admin: 0,
    },
    detailsProjection: {
        __v: 0
    }
};

const inquiryStatuses = {
    "initialStatus": "otpPending",
    "otpVerified": "otpVerified",
    "pendingStatus": "Pending",
    "actionTaken": "actionTaken",
    "onHold": "onHold",
    "cancelled": "Cancelled",
    "completedStatus": "Completed"
};
exports.inquiryStatuses = inquiryStatuses;

const inquiryMessages = {
    "InquiryNotAvail": "Sorry, this inquiry is currently not available",
    "OTPVerifcationRequired": "Please verify the OTP to add the inquiry",
    "InquiryCreateSuccess": "Inquiry created successfully",
    "InquiryUpdateSuccess": "Inquiry updated successfully",
};
exports.inquiryMessages = inquiryMessages;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Create 
const createInquiry = (data) => {
    return inquiryModel.create(data);
};
exports.createInquiry = createInquiry;

//  Inquiry Count
const countInquiry = (whereJSON) => {
    return inquiryModel.count(whereJSON);
};
exports.countInquiry = countInquiry;

//  Inquiry List
const listInquirys = (whereJSON, projects=projections.inquiryListProjection, opts, popsOptions={seller: 0, buyer: 0, product: 1}) => {

    let populateArray = [
        productPopProject
    ];
    if(popsOptions.seller)
        populateArray.push(sellerPopProject);
    if(popsOptions.buyer)
        populateArray.push(buyerPopProject);

    return inquiryModel.find(
        whereJSON,
        projects,
        { skip: opts.skip, limit: opts.limit }
    )
    .sort(opts.sort || defaultSort)
    .populate(populateArray);

};
exports.listInquirys = listInquirys;

//  Inquiry Check
exports.inquiryCheck = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
            let inquiry = await inquiryModel
                .findOne(findJSON, projections, options)
                .populate("seller")
                .lean();

			if (!inquiry)
				return failedActionHandler(response, inquiryMessages.InquiryNotAvail);

			return resolve(inquiry);
		} catch (error) {
			return reject(error);
		}
	});
};

//  Details Get
exports.inquiryDetails = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
            let inquiry = await inquiryModel.findOne(findJSON, projections, options)
            .populate([
                sellerPopProject,
                buyerPopProject,
                creatorBuyerPopProject,
                productPopProject,
                categoryPopProject,
                shopPopProject
            ]);

			if (!inquiry)
				return failedActionHandler(response, inquiryMessages.InquiryNotAvail);

			return resolve(inquiry);
		} catch (error) {
			return reject(error);
		}
	});
};

//  Update
const inquiryUpdate = (whereJSON, newData, opts={new: true}) => {
    return inquiryModel.findOneAndUpdate(whereJSON, newData, opts);
};
exports.inquiryUpdate = inquiryUpdate;

//      Common Filtering Where Creator
const inquiryListingCommonWhereCreator = (whereJSON, data) => {

    if(data.sellers) {
        whereJSON["$and"].push({
            "seller": { $in: JSON.parse(data.sellers) }
        }); 
    }
    if(data.buyers) {
        whereJSON["$and"].push({
            "buyer": { $in: JSON.parse(data.buyers) }
        }); 
    }
    if(data.products) {
        whereJSON["$and"].push({
            "product": { $in: JSON.parse(data.products) }
        }); 
    }
    if(data.shops) {
        whereJSON["$and"].push({
            "shop": { $in: JSON.parse(data.shops) }
        }); 
    }


    if(data.categorys) {
        whereJSON["$and"].push({
            "category": { $in: JSON.parse(data.categorys) }
        }); 
    }
    if(data.subCategorys) {
        whereJSON["$and"].push({
            "subCategory": { $in: JSON.parse(data.subCategorys) }
        }); 
    }

    if(data.statuses) {
        whereJSON["$and"].push({
            "status": { $in: JSON.parse(data.statuses) }
        }); 
    }

    if(data.search) {
        whereJSON["$or"] = [
          { name: { $regex: data.search, $options: "i" } },
          { email: { $regex: data.search, $options: "i" } },
          { phoneNumber: { $regex: data.search, $options: "i" } },
          { description: { $regex: data.search, $options: "i" } }
        ]
    }

    return whereJSON;

};
exports.inquiryListingCommonWhereCreator = inquiryListingCommonWhereCreator;


//  Inquiry Check
exports.inquiryGet4Notification = (findJSON) => {
    
    return inquiryModel.findOne(findJSON)
        .populate([
            sellerPopProject4Notifications,
            buyerPopProject4Notifications
        ])
        .lean();
};
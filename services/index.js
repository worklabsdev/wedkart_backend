module.exports = {
    "adminServices": require("./adminServices"),
    "buyerServices": require("./buyerServices"),
    "sellerServices": require("./sellerServices")
};
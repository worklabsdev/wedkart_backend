const { failedActionHandler, validationHandler } = require("../utils/responseHandler");

const { FileNotAvail, BuyerNotAvail, SellerNotAvail, ShopNotAvail, ProductNotAvail, AdminNotAvail } = require("../config/properties/constants").globalMessages.responseMessages;

const { sellerPopProject, creatorSellerPopProject, buyerPopProject, creatorBuyerPopProject, adminPopProject, creatorAdminPopProject, shopPopProject, productPopProject } = require("./commonServices").projectionPopulate;

const Db = require("../models");

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const defaultSort = {"_id": -1};

const projections = {
    filesListProjection: {
        isDeleted: 0,
        __v: 0,
        seller: 0,
        product: 0,
        shop: 0,
        buyer: 0,
        admin: 0,
    },
    detailsProjection: {
        __v: 0
    }
};

const fileMessages = {
};
exports.fileMessages = fileMessages;

const fileTypesSer = {
  "normal": "Normal",
  "bookingForm": "BookingForm"
};
exports.fileTypesSer = fileTypesSer;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

const createFile = (data) => {
    return Db.files.create(data);
};
exports.createFile = createFile;

//  Files Count
const countFiles = (whereJSON) => {
    return Db.files.count(whereJSON);
};
exports.countFiles = countFiles;

//  Files List
const listFiles = (whereJSON, projects=projections.filesListProjection, opts={}) => {

    let options = {  };
    if(opts.skip)
      options.skip = opts.skip;
    if(opts.limit)
      options.limit = opts.limit;

    return Db.files.find(
        whereJSON,
        projects,
        options
    )
    .sort(opts.sort || defaultSort);

};
exports.listFiles = listFiles;

//  File Details
const detailFile = (response, whereJSON, projects=projections.detailsProjection, opts={}) => {

	return new Promise(async (resolve, reject) => {
		try {
			
			let file = await Db.files.findOne(
				whereJSON,
				projects,
				opts
			)
			.populate([
        adminPopProject,
        creatorAdminPopProject,

        buyerPopProject,
        creatorBuyerPopProject,

        sellerPopProject,
        creatorSellerPopProject,
                
        shopPopProject,
        productPopProject
      ]).lean();

      if (!file)
				return failedActionHandler(response, FileNotAvail);

			return resolve(file);
		} catch (error) {
			return reject(error);
		}
	});

};
exports.detailFile = detailFile;

//  Update File
const updateFile = (whereJSON, newData, opts={new: true}) => {
    return Db.files.findOneAndUpdate(whereJSON, newData, opts);
};
exports.updateFile = updateFile;

//  Seller Buyer Product Shop Admin Check
const checkOData4FileUpload = (response, data) => {

	return new Promise(async (resolve, reject) => {
		try {
            
            if(data.buyer) {// Buyer Check
                let buyerCheck = await Db.seller.findOne({
                  _id: data.buyer,
                  isDeleted: false
                });
                if(!buyerCheck)
                  return validationHandler(response, BuyerNotAvail);
              }// Buyer Check
              if(data.seller) {// Seller Check
                  let sellerCheck = await Db.seller.findOne({
                    _id: data.seller,
                    isDeleted: false
                  });
                  if(!sellerCheck)
                    return validationHandler(response, SellerNotAvail);
                }// Seller Check
              if(data.admin) {// Admin Check
                let adminCheck = await Db.admin.findOne({
                  _id: data.admin,
                  isDeleted: false
                });
                if(!adminCheck)
                  return validationHandler(response, AdminNotAvail);
              }// Admin Check
              if(data.product) {// Product Check
                let productCheck = await Db.product.findOne({
                  _id: data.product,
                  isDeleted: false
                });
                if(!productCheck)
                  return validationHandler(response, ProductNotAvail);
              }// Product Check
              if(data.shop) {// Shop Check
                let shopCheck = await Db.shop.findOne({
                  _id: data.shop,
                  isDeleted: false
                });
                if(!shopCheck)
                  return validationHandler(response, ShopNotAvail);
              }// Shop Check

			return resolve();
		} catch (error) {
			return reject(error);
		}
	});

}
exports.checkOData4FileUpload = checkOData4FileUpload;

//      Common Filtering Where Creator
const filesListingCommonWhereCreator = (whereJSON, data) => {

    if(data.sellers) {
        whereJSON["$and"].push({
          "seller": { $in: JSON.parse(data.sellers) }
        }); 
      }
      if(data.buyers) {
        whereJSON["$and"].push({
          "buyer": { $in: JSON.parse(data.buyers) }
        }); 
      }
      if(data.products) {
        whereJSON["$and"].push({
          "product": { $in: JSON.parse(data.products) }
        }); 
      }
      if(data.admins) {
        whereJSON["$and"].push({
          "admin": { $in: JSON.parse(data.admins) }
        }); 
      }
      if(data.shops) {
        whereJSON["$and"].push({
          "shop": { $in: JSON.parse(data.shops) }
        }); 
      }

      if(data.search) {
        whereJSON["$or"] = [
          { name: { $regex: data.search, $options: "i" } },
          { description: { $regex: data.search, $options: "i" } }
        ]
      }

      return whereJSON;

};
exports.filesListingCommonWhereCreator = filesListingCommonWhereCreator;
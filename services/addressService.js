var addressMdl = require("../models/address");

const { failedActionHandler } = require("../utils/responseHandler");
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const addressDefSort = {
    isDefault: -1,
    _id: 1
};

const addressProjectSer = {
    addressListProject: {
        "updatedAt": 0,
        "createdAt": 0,
        "__v": 0,
        "isDeleted": 0,
        "seller": 0,
        "buyer": 0
    }
};
exports.addressProjectSer = addressProjectSer;

const addressMessagesSer = {
    "AddressCreateSuccessMsg": "Address address successfully",
    "AddressNotAvail": "Sorry, this address is currently not available",
    "AddressUpdateSuccessMsg": "Address updated successfully",
    "AddressDeleteSuccessMsg": "Address deleted successfully",
};
exports.addressMessagesSer = addressMessagesSer;

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//  Create 
const createAddressSer = (data) => {
    return addressMdl.create(data);
};
exports.createAddressSer = createAddressSer;


//  Update Address
const updateAddressSer = (whereJSON, newData, opts={new: true}) => {
    return addressMdl.findOneAndUpdate(whereJSON, newData, opts);
};
exports.updateAddressSer = updateAddressSer;

//  List Addresses
const listAddressSer = (whereJSON, projects=addressProjectSer.addressListProject, opts={
sort: addressDefSort
}) => {
    return addressMdl.find(whereJSON, projects, opts);
};
exports.listAddressSer = listAddressSer;

//  Address Check
exports.addressCheck = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
            let address = await addressMdl
                .findOne(findJSON, projections, options)
                .lean();

			if (!address)
				return failedActionHandler(response, addressMessagesSer.AddressNotAvail);

			return resolve(address);
		} catch (error) {
			return reject(error);
		}
	});
};
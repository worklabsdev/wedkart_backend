const Joi = require("@hapi/joi");

const productMdl = require("../models/product");

const { failedActionHandler } = require("../utils/responseHandler");

const { ProductNotAvail } = require("../config/properties/constants").globalMessages.responseMessages;

const { productSellerOpProject, categoryPopProject, productSellerOpProjectFull, categoryPopProjectFull } = require("./commonServices").projectionPopulate;

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////
const defaultSort = {"_id": -1};

const projections = {
	productsListProjection: {
			isDeleted: 0,
			__v: 0,
			buttons: 0,
			color: 0,
			size: 0,
			specifications: 0,
			images: 0,
			updatedAt: 0
	},
	detailsProjection: {
			__v: 0
	}
};

//  Services
const cashOnDelivery = "Cash on delivery is available"; 
const return30Days = "30 Days Return Policy";
const serviceTypes = {
    cashOnDelivery,
    return30Days
};
exports.serviceTypes = serviceTypes;

//      Sizes
const sizeValues = {
    "xxxl": "xxxl", 
    "xxl": "xxl", 
    "xl": "xl", 
    "l": "l", 
    "m": "m", 
    "s": "s"
};
exports.sizeValues = sizeValues;

//      Quantity Types
const Unit = "Unit";
const Meter = "Meter";
const Kilogram = "Kilogram";
const quantityTypes = {
    Unit,
    Meter,
    Kilogram
};
exports.quantityTypes = quantityTypes;

const buttonTypes = {
	payToken: "payToken",
	buyNow: "buyNow"
};
exports.buttonTypes = buttonTypes;

//  Messages
let productServiceMessages = {
    "ProductCreateSuccess": "Product created successfully",
    "ProductDelSuccess": "Product deleted successfully",
	"ProductUpdateSuccess": "Product updated successfully",
	"ProductReplicateSuccess": "Product replicated successfully",

	"ProductSellerNotAvail": "Sorry, this product seller is currently not available",
	"ProductCategoryNotAvail": "Sorry, this product category is currently not available",

	"ImageUpdateSuccess": "Image updated successfully",
	"ImageDelSuccess": "Image deleted successfully",
	"ImageReq": "Image is required"
};
exports.productServiceMessages = productServiceMessages;
///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//	Schemas
const productJoiSchemas = {
	"button": Joi.object().keys({
		enquiry: Joi.object().keys({
			enabled: Joi.boolean().required()
		}),
		buyNow: Joi.object().keys({
			enabled: Joi.boolean().required(),
			amount: Joi.number().min(0).required()
		}),
		payToken: Joi.object().keys({
			enabled: Joi.boolean().required(),
			amount: Joi.number().min(0).required()
		})
	})
};
exports.productJoiSchemas = productJoiSchemas;

//  Product Get
exports.productGetService = (response, findJSON, projections={}, options={}) => {
    return new Promise(async (resolve, reject) => {
		try {
			let product = await productMdl.findOne(findJSON, projections, options);

			if (!product)
				return failedActionHandler(response, ProductNotAvail);

			return resolve(product);
		} catch (error) {
			return reject(error);
		}
	});
};

//	Details For Purchase
exports.productDetails4Purchase = (response, productId) => {
    return new Promise(async (resolve, reject) => {
		try {
			let product = await productMdl.findOne({
				_id: productId,
				isDeleted : false,
				isSellerBlocked : false,
				isAdminBlocked : false,
			})
			.populate([
				productSellerOpProjectFull,
				categoryPopProjectFull
			]).lean();

			if (!product)
				return failedActionHandler(response, ProductNotAvail);
			else if(!product.seller.id || !product.seller.id._id || product.seller.id.isDeleted || product.seller.id.isBlocked)
				return failedActionHandler(response, productServiceMessages.ProductSellerNotAvail);

			return resolve(product);
		} catch (error) {
			return reject(error);
		}
	});
}

//	///	Create
exports.productCreate = (data) => {
	return productMdl.create(data);
}

//	///	Update
exports.productUpdateService = (findJSON, newData, options) => {
	return productMdl.findOneAndUpdate(findJSON, newData, options);
};
exports.productsUpdateSer = (findJSON, newData, options=null) => {
	return productMdl.updateMany(findJSON, newData, options);
};


//	Count
const productsCount = (whereJSON) => {
	return productMdl.count(whereJSON);
};
exports.productsCount = productsCount;

//  Listing
const productsList = (whereJSON, projects=projections.productsListProjection, opts) => {
	return productMdl.find(
		whereJSON,
		projects,
		{ skip: opts.skip, limit: opts.limit }
	)
	.sort(opts.sort || defaultSort)
	.populate([
		productSellerOpProject,
		categoryPopProject,
		//subCategoryPopProject
	]);
};
exports.productsList = productsList;

//	Product Listing No Pagination
const productsListNoPaginate = (whereJSON, projects=projections.productsListProjection, opts) => {
	return productMdl.find(
		whereJSON,
		projects
	)
	.sort(opts.sort || defaultSort);
};
exports.productsListNoPaginate = productsListNoPaginate;


//      Common Filtering Where Creator
const productListingCommonWhereCreator = (whereJSON, data) => {
	//	Seller Filter
	try{
		if(data.sellers) {
			whereJSON["$and"].push({
				"seller.id": { $in: JSON.parse(data.sellers) }
			}); 
		}
	}
	catch(error) {}
	//	Seller Filter

	//	Category Filter
	try{
		if(data.categorys) {
			whereJSON["$and"].push({
				"category": { $in: JSON.parse(data.categorys) }
			}); 
		}	
	}
	catch(error) {}
	//	Category Filter

	//	Subcategory Filter
	try{
		if(data.subCategorys) {
			whereJSON["$and"].push({
				"subCategory": { $in: JSON.parse(data.subCategorys) }
			}); 
		}	
	}
	catch(error) {}
	//	Subcategory Filter

	//	Search Filter
	if(data.search) {
		whereJSON["$or"] = [
			{ name: { $regex: data.search, $options: "i" } },
			{ description: { $regex: data.search, $options: "i" } },
			{ slug: { $regex: data.search, $options: "i" } }
		]
	}
	//	Search Filter

	//	Is Featured Filter
	if(data.isFeatured != undefined) {
		whereJSON["$and"].push({
			isFeatured: data.isFeatured
		}); 
	}
	//	Is Featured Filter

	return whereJSON;
};
exports.productListingCommonWhereCreator = productListingCommonWhereCreator;
const pathPrefix = "/api/v1/admin";

app.use(`${pathPrefix}/`, require("./category"));// Old
app.use(`${pathPrefix}/files/`, require("./files"));
app.use(`${pathPrefix}/inquiry/`, require("./inquiry"));
app.use(`${pathPrefix}/notifications/`, require("./notifications"));
app.use(`${pathPrefix}/payments/`, require("./payments"));
app.use(`${pathPrefix}/products/`, require("./products"));
app.use(`${pathPrefix}/sellers/`, require("./sellers"));
const express = require("express");

const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/add", adminVerifyMiddleware, multipartMiddleware, validators.upload, controllers.upload);

router.post("/list", adminVerifyMiddleware, validators.list, controllers.list);

router.get("/:id", adminVerifyMiddleware, validators.details, controllers.details);

router.put("/:id", adminVerifyMiddleware, multipartMiddleware, validators.update, controllers.update);

router.delete("/:id", adminVerifyMiddleware, multipartMiddleware, validators.deleteFile, controllers.deleteFile);

module.exports = router;

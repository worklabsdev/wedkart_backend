const express = require("express");

const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", adminVerifyMiddleware, validators.listInquiry, controllers.listInquiry);

router.post("/add", adminVerifyMiddleware, validators.addInquiry, controllers.addInquiry);

router.get("/:id", adminVerifyMiddleware, validators.detailInquiry, controllers.detailInquiry);

router.put("/:id", adminVerifyMiddleware, validators.updateInquiry, controllers.updateInquiry);

module.exports = router;
const responseHandler = require("../../../../utils/responseHandler");

const { productGetService } = require("../../../../services/productServices");
const { buyerGetService } = require("../../../../services/buyerServices");

const {
	inquiryMessages, inquiryCheck, inquiryUpdate, createInquiry, inquiryDetails, inquiryStatuses, inquiryListingCommonWhereCreator, listInquirys, countInquiry,
} = require("../../../../services/inquiryServices");
const { pageOffsetCreator } = require("../../../../services/commonServices");

//	List Inquiry
const listInquiry = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		let whereJSON = {
			$and: [
				{ isBuyerDeleted: false },
				{ isSellerDeleted: false },
			],
		};
		whereJSON = inquiryListingCommonWhereCreator(whereJSON, request.body);

		const result = await Promise.all([
			countInquiry(whereJSON),
			listInquirys(whereJSON, undefined, request.body, { seller: 1, buyer: 1, product: 1 }),
		]);

		const count = result[0];
		const inquiries = result[1];

		return responseHandler.successHandler(response, null, { count, inquiries });
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.listInquiry = listInquiry;

//		Inquiry Add
const addInquiry = async (request, response) => {
	try {
		const product = await productGetService(
			response,
			{
				_id: request.body.product,
				isDeleted: false,
			},
		);

		if (request.body.buyer) { // Buyer Validation
			const buyer = await buyerGetService(
				response,
				{
					_id: request.body.buyer,
					isDeleted: false,
				},
			);
		}// Buyer Validation

		const newData = {
			...request.body,
			subCategory: product.subCategory,
			category: product.category,
			seller: product.seller.id,
			creatorAdmin: request.adminDetails._id,
			otp: {
				otp: "",
				otpValidity: null,
			},
		};

		const inquiry = await createInquiry(newData);

		const result = {
			inquiry,
		};

		return responseHandler.createdHandler(response, inquiryMessages.InquiryCreateSuccess, { result });
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.addInquiry = addInquiry;

//		Inquiry Detils
const detailInquiry = async (request, response) => {
	try {
		const inquiry = await inquiryDetails(
			response,
			{
				_id: request.body.id,
				isSellerDeleted: false,
				isBuyerDeleted: false,
			},
		);

		return responseHandler.createdHandler(response, null, { inquiry });
	} catch (error) {
	    return responseHandler.errorHandler(response, error, {});
	}
};
exports.detailInquiry = detailInquiry;

//		Inquiry Update
const updateInquiry = async (request, response) => {
	try {
		let inquiry = await inquiryCheck(
			response,
			{
				_id: request.body.id,
				isBuyerDeleted: false,
			},
		);

		delete request.body.id;
		inquiry = await inquiryUpdate(
			{ _id: inquiry._id },
			{
				$set: {
					...request.body,
					updatedAt: Date.now(),
				},
			},
			{ new: true },
		);

		return responseHandler.createdHandler(response, inquiryMessages.InquiryUpdateSuccess, { inquiry });
	} catch (error) {
	    return responseHandler.errorHandler(response, error, {});
	}
};
exports.updateInquiry = updateInquiry;
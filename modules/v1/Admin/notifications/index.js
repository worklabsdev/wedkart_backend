const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", adminVerifyMiddleware, validators.listing, controllers.listing);

router.put("/update", adminVerifyMiddleware, validators.notsUpdate, controllers.notsUpdate);

module.exports = router;
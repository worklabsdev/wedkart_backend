const responseHandler = require("../../../../utils/responseHandler");

const { sellerGetService, sellerProfileCreatorSer } = require("../../../../services/sellerServices");

//	Seller Details
const sellerDetails = async (request, response) => {
	try {

		let seller = await sellerGetService(
			response,
			{
				_id: request.body.sellerId,
				isDeleted: false
			},
			null,
			{
				lean: true
			}
		);

		seller = sellerProfileCreatorSer(seller);

		return responseHandler.successHandler(response, null, {
			seller
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.sellerDetails = sellerDetails;
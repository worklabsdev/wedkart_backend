const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.get("/:sellerId", adminVerifyMiddleware, validators.sellerDetails, controllers.sellerDetails);

module.exports = router;
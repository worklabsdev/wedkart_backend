const Joi = require("@hapi/joi");

const apiRef = "AdminSeller";

const { joiValidate } = require("../../../../packages/joi");

//	Listing
exports.sellerDetails = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/sellerDetails`;

	const schema = Joi.object().keys({
		sellerId: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
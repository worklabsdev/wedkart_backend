const responseHandler = require("../../../../utils/responseHandler");

const { pageOffsetCreator } = require("../../../../services/commonServices");
const paymentServices = require("../../../../services/paymentServices");

//	List Payments
const listing = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		//	Where Creator
		let whereJSON = {
			$and: [
				{ isDeleted: false },
				{ isBlocked: false },
				{ paymentStatus: { $nin: [paymentServices.paymentStatuses.pending] } },
			],
		};

		whereJSON = paymentServices.paymentListingCommonWhereCreator(whereJSON, request.body);

		let result = await Promise.all([
			paymentServices.countPayments(whereJSON),
			paymentServices.listPayments(whereJSON, undefined, request.body),
		]);

		result = {
			count: result[0],
			payments: result[1],
		};

		return responseHandler.successHandler(response, null, result);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.listing = listing;

//	Details of Product
const details = async (request, response) => {
	try {
		const payment = await paymentServices.paymentDetailsGet({
			$and: [
				{ orderId: request.body.orderId },
				{ isDeleted: false },
				{ paymentStatus: { $nin: [paymentServices.paymentStatuses.pending] } },
			],
		}).lean();
		if (!payment) return responseHandler.failedActionHandler(response, paymentServices.paymentMessages.PaymentNotAvail);



		
		return responseHandler.successHandler(response, null, { payment });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.details = details;
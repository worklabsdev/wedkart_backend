const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares"); 

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", adminVerifyMiddleware, validators.listing, controllers.listing);

router.get("/:orderId", adminVerifyMiddleware, validators.details, controllers.details);

module.exports = router;
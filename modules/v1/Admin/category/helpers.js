const subCatUniqueCheckAggregate = (catId, subCatId = null, name) => {
	const nameMatch = { // Category SubCat Same Name Match
		$match: {
			"subcategories.name": name, // {"$regex": name, "$options": "i"}
			"subcategories.isDeleted": false,
		},
	};// Category SubCat Same Name Match
	if (subCatId) {
		nameMatch.$match["subcategories._id"] = { $ne: ObjectId(subCatId) };
	}

	return [
		{ // Category Type Match
			$match: {
				isDeleted: false,
				_id: ObjectId(catId),
			},
		}, // Category Type Match
		{ $unwind: "$subcategories" },
		nameMatch, // Category SubCat Same Name Match
	];
};
exports.subCatUniqueCheckAggregate = subCatUniqueCheckAggregate;

//      Categories Listings With Sub Cats Filtering
const categoryOnlyValidDetails = (opts) => {
	//      Category Filtering
	const categoryBasicMatch = {
		isDeleted: false,
		isBlocked: { $in: [false] },
	};

	if (opts.catId) {
		categoryBasicMatch._id = ObjectId(opts.catId);
	}
	if (opts.allCatsFilter && opts.allCatsFilter === "0") {
		categoryBasicMatch.isBlocked = { $in: [false, true] };
	}
	//      Category Filtering

	//      Sub Category Filtering
	const subCatBasicMatch = {
		"subcategories.isDeleted": false,
		"subcategories.isBlocked": { $in: [false] },
	};

	if (opts.allSubCatsFilter && opts.allSubCatsFilter === "0") {
		subCatBasicMatch["subcategories.isBlocked"] = { $in: [false, true] };
	}
	//      Sub Category Filtering

	if (!opts.defaultSort) opts.defaultSort = { _id: -1 };

	const basicAggregate = [
		{ $match: categoryBasicMatch },
		{ $unwind: "$subcategories" },
		{ $match: subCatBasicMatch },
	];

	if (opts.appListing) { // App Listing
		basicAggregate.push({
			$project: {
				_id: 1,
				name: 1,
				description: 1,
				commission: 1,
				subcategories: {
					_id: "$subcategories._id",
					name: "$subcategories.name",
				},
			},
		});

		basicAggregate.push({
			$group: {
				_id: "$_id",
				name: { $first: "$name" },
				description: { $first: "$description" },
				commission: { $first: "$commission" },
				subcategories: { $push: "$subcategories" },
			},
		});

		basicAggregate.push({ $sort: { name: 1 } });
	}// App Listing
	else { // Admin Listing
		basicAggregate.push({
			$group: {
				_id: "$_id",
				name: { $first: "$name" },
				isBlocked: { $first: "$isBlocked" },
				description: { $first: "$description" },
				createdAt: { $first: "$createdAt" },
				updatedAt: { $first: "$updatedAt" },
				commission: { $first: "$commission" },
				subcategories: { $push: "$subcategories" },
			},
		});

		basicAggregate.push({ $sort: opts.defaultSort });
	}// Admin Listing


	return basicAggregate;
};
exports.categoryOnlyValidDetails = categoryOnlyValidDetails;
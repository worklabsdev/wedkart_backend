const Joi = require("@hapi/joi");

const apiRef = "AdminCategory";

const { joiValidate } = require("../../../../packages/joi");

//		Get Category Details
exports.getCategory = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/getCategory`;

	const schema = Joi.object().keys({
		catId: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

// ///		Add Category
exports.addCategory = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addCategory`;

	const schema = Joi.object().keys({
		name: Joi.string().required(),
		commission: Joi.number().min(0).max(100).required(),
		sub_cat: Joi.array().items(
			Joi.string().required(),
		).unique().required()
			.label("Sub Category"),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Add SubCat
exports.addSubCat = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addSubCat`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
		name: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//  ///         Update SubCat
exports.updateSubCat = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/updateSubCat`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
		subCatId: Joi.string().required(),
		name: Joi.string().optional(),
		isBlocked: Joi.boolean().optional(),
	}).or("name", "isBlocked");

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///		Delete CHildren
exports.deleteSubCat = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deleteSubCat`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
		subCatId: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
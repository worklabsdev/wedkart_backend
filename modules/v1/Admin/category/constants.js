const projections = {
	categoryProjection: {
		updatedAt: 0,
	},
};
exports.projections = projections;

const messages = {
	CategoryAlreadyTaken: "Sorry, this category is already added",
	CategoryCreateSuccess: "Category created successfully",
	CategoryNotAvail: "Sorry, this category is currently not available",
	NameAlreadyTaken: "Sorry, this category name is already taken",

	SubCatNotAvail: "Sorry, this sub category is currently not available",
	SubCatAlreadyTaken: "Sorry, this sub category is already present in this category",
	SubCatDelSuccess: "Sub Category deleted successfully",
	SubCatUpdateSuccess: "Sub Category updated successfully",
	SubCatCreateSuccess: "Sub Category created successfully",
	SubCatNameAlreadyTaken: "Sorry, this sub category is already present in this category",
};
exports.messages = messages;
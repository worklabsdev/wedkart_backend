require("../../../../plugins/jsend");

const categoryMdl = require("../../../../models/categories");

const services = require("./services");
const constants = require("./constants");
const helpers = require("./helpers");

const responseHandler = require("../../../../utils/responseHandler");

//	Add Category
const addCategory = async (req, res) => {
	try {
		// {"name": "Cat 1", "sub_cat": ["Sub11","sub12","sub13","sub14"], "commission": 12 }

		//	Name Check
		const nameCheck = await categoryMdl.findOne({
			name: req.body.name,
			isDeleted: false,
		});
		if (nameCheck) return responseHandler.failedActionHandler(response, constants.messages.CategoryAlreadyTaken);
		//	Name Check

		const subcategories = [];
		req.body.sub_cat.forEach((sub) => {
			subcategories.push({
				name: sub,
				isDeleted: false,
				isBlocked: false,
			});
		});

		const category = await categoryMdl.create({
			name: req.body.name,
			description: req.body.description || "",
			creator: {
				type: "Admin",
				admin: req.adminDetails._id,
			},
			commission: req.body.commission || 0,
			subcategories
		});

		return responseHandler.successHandler(response, constants.messages.CategoryCreateSuccess);
	} catch (err) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.addCategory = addCategory;

//	Get Categories
const getCategories = async (req, res) => {
	try {
		const aggregate = await helpers.categoryOnlyValidDetails({
			allCatsFilter: "0",	// For Including Blocked Cats As Well
			allSubCatsFilter: "0",	// For Including Blocked Sub Cats as Well
		});

		const category = await services.categoryAggregate(aggregate);

		return res.status(200).success(category);
	} catch (err) {
		return res.status(500).error(err, {});
	}
};
exports.getCategories = getCategories;

//	Single Category Details
const getCategory = async (req, res) => {
	try {
		const aggregate = await helpers.categoryOnlyValidDetails({
			catId: req.body.catId,
		// allCatsFilter: "0",	//For Including Blocked Cats As Well
		});

		const category = await services.categoryAggregate(aggregate);
		if (!category.length) return res.status(400).failure("No category found.");

		return res.status(200).success(category[0]);
	} catch (err) {
		return res.status(500).error(err, {});
	}
};
exports.getCategory = getCategory;

//	Delete Category
const deleteCategory = async (req, res) => {
	try {
		if (!req.body.cat_id) return res.status(400).failure("Category Id is required");

		const category = await categoryMdl.findByIdAndUpdate(
			req.body.cat_id,
			{
				$set: {
					isDeleted: true,
					updatedAt: Date.now(),
				},
			},
			{
				new: true,
			}
		);
		if (!category) return res.status(400).failure("No category found.");

		return res.status(200).success(category, "Category deleted successfully");
	} catch (err) {
		return res.status(500).error(err, {});
	}
};
exports.deleteCategory = deleteCategory;

//	Category Update
const updateCategory = async (req, res) => {
	try {
		if (!req.body.name) return res.status(400).failure("please Enter category");

		let category = await categoryMdl.findOne({
			isDeleted: false,
			_id: req.body.cat_id,
		}).lean();
		if (!category) return res.status(400).failure("No category found.");

		if (req.body.name !== category.name) { //	Name Check
			const nameCheck = await categoryMdl.findOne({
				name: req.body.name,
				isDeleted: false,
			});
			if (nameCheck) return res.status(400).failure("This category is already added");
		}//	Name Check

		category = await categoryMdl.findByIdAndUpdate(
			category._id,
			{
				$set: {
					...req.body,
					updatedAt: Date.now(),
				},
			},
			{
				new: true,
			}
		);

		return res.status(200).success(category, "Category updated successfully");
	} catch (err) {
		return res.status(500).error(err, {});
	}
};
exports.updateCategory = updateCategory;

//      Update SUbCategory
const addSubCat = async (request, response) => {
	try {
		let category = await services.categoryCheck(
			response,
			{ isDeleted: false, _id: request.params.id },
		);

		const aggregate = helpers.subCatUniqueCheckAggregate(request.body.id, null, request.body.name);

		const nameCheck = await services.categoryAggregate(aggregate);
		if (nameCheck.length) return responseHandler.failedActionHandler(response, constants.messages.SubCatAlreadyTaken);

		category = await services.update(
			{ _id: category._id },
			{
				$push: {
					subcategories: {
						name: request.body.name,
					},
				},
			},
			{ new: true },
		);

		return responseHandler.successHandler(response, constants.messages.SubCatCreateSuccess, { category });
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.addSubCat = addSubCat;

//		Update Sub Cat
const updateSubCat = async (request, response) => {
	try {
		const subCat = await services.catWithSubCatDetails(response, request.body.id, request.body.subCatId);

		if (request.body.name && (request.body.name != subCat.subcategory.name)) { // Name Check ///
			const aggregate = helpers.subCatUniqueCheckAggregate(request.body.id, request.body.subCatId, request.body.name);//	Agregate Creator

			const nameCheck = await services.categoryAggregate(aggregate);
			if (nameCheck.length) return responseHandler.failedActionHandler(response, constants.messages.SubCatAlreadyTaken);
		}// Name Check ///

		delete request.body.subCatId;
		delete request.body.id;
		await services.updateSubCat(subCat.subcategory._id, request.body);

		return responseHandler.successHandler(response, constants.messages.SubCatUpdateSuccess, {});
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.updateSubCat = updateSubCat;

//		Delete Sub Cat
const deleteSubCat = async (request, response) => {
	try {
		const subCat = await services.catWithSubCatDetails(response, request.body.id, request.body.subCatId);

		await services.updateSubCat(
			request.body.subCatId,
			{ isDeleted: true, updatedAt: request.body.updatedAt },
		);

		return responseHandler.successHandler(response, constants.messages.SubCatDelSuccess, {});
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.deleteSubCat = deleteSubCat;
const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/addCategory", adminVerifyMiddleware, validators.addCategory, controllers.addCategory);
router.get("/getCategories", adminVerifyMiddleware, controllers.getCategories);
router.get("/getCategory/:catId", adminVerifyMiddleware, validators.getCategory, controllers.getCategory);

router.post("/deleteCategory", adminVerifyMiddleware, controllers.deleteCategory);

router.put("/updateCategory", adminVerifyMiddleware, controllers.updateCategory);

router.post("/:id/subcat", adminVerifyMiddleware, validators.addSubCat, controllers.addSubCat);

router.patch("/:id/subcat", adminVerifyMiddleware, validators.updateSubCat, controllers.updateSubCat);

router.delete("/:id/subcat", adminVerifyMiddleware, validators.deleteSubCat, controllers.deleteSubCat);

module.exports = router;
const categoryMdl = require("../../../../models/categories");

const { messages } = require("./constants");

const { failedActionHandler } = require("../../../../utils/responseHandler");

//	///	Update Category
exports.update = (findJSON, newData, options) => categoryMdl.findOneAndUpdate(findJSON, newData, options);

//	Category Type Avail Check
exports.categoryCheck = (response, findJSON, projections = {}, options = {}) => new Promise(async (resolve, reject) => {
	try {
		const category = await categoryMdl.findOne(findJSON, projections, options);

		if (!category) return failedActionHandler(response, messages.CategoryNotAvail);

		return resolve(category);
	} catch (error) {
		return reject(error);
	}
});

exports.categoryAggregate = (aggregate) => categoryMdl.aggregate(aggregate);

//  Category With Sub Category Details
const catWithSubCatDetailsOpts = {
	subCatBlockReturn: true,
};
exports.catWithSubCatDetails = (response, catId, subCatId, opts = catWithSubCatDetailsOpts) => new Promise(async (resolve, reject) => {
	try {
		let category = await categoryMdl.findOne(
			{ _id: catId, isDeleted: false },
		);
		if (!category) return failedActionHandler(response, messages.CategoryNotAvail);

		let subcategory = null; let
			index = null;

		if (subCatId) { //    SubCategory Present
			category.subcategories.forEach((subcat, key) => {
				if (subcategory) return;

				if ((subcat._id == subCatId) && !(subcat.isDeleted)) {
					if (!opts.subCatBlockReturn && subcat.isBlocked) {
						return;
					}

					index = key;
					subcategory = subcat;
				}
			});
			if (!subcategory) return failedActionHandler(response, messages.SubCatNotAvail);
		}//    SubCategory Present

		category = JSON.parse(JSON.stringify(category));

		category.subcategory = subcategory;
		category.index = index;

		resolve(category);
	} catch (error) {
		return reject(error);
	}
});

//	Update SubCategory
exports.updateSubCat = (subCatId, updatedData, options = { new: true }) => {
	const newSet = {};
	for (key in updatedData) {
		newSet[`subcategories.$.${key}`] = updatedData[key];
	}

	return categoryMdl.findOneAndUpdate(
		{ "subcategories._id": ObjectId(subCatId) },
		{ $set: newSet },
		options,
	);
};
const router = express.Router();

const { adminVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", adminVerifyMiddleware, validators.listing, controllers.listing);

router.put("/:productId", adminVerifyMiddleware, multipartMiddleware, validators.productUpdate, controllers.productUpdate);

router.post("/:productId/replicate", adminVerifyMiddleware, validators.productReplicate, controllers.productReplicate);

module.exports = router;
const Joi = require("@hapi/joi");

const apiRef = "AdminProduct";

const { joiValidate } = require("../../../../packages/joi");

const {
	serviceTypes, sizeValues, quantityTypes, productJoiSchemas,
} = require("../../../../services/productServices");

//	Listing
exports.listing = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/listing`;

	const schema = Joi.object().keys({
		skip: Joi.number().integer().min(0).optional(),
		limit: Joi.number().min(0).optional(),
		search: Joi.string().optional(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Update Product
exports.productUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/productUpdate`;

	const schema = Joi.object().keys({
		productId: Joi.string().required(),

		name: Joi.string().optional(),

		aprice: Joi.number().min(0).optional().label("Actual price"),
		dprice: Joi.number().min(0).optional().label("Discount price"),

		description: Joi.string().optional(),

		size: Joi.array().items(
			Joi.string().required(),
			// Joi.string().valid(sizeValues.string).required()
		).unique().label("Size")
			.optional(),

		service: Joi.array().items(
			Joi.string().valid(serviceTypes.return30Days, serviceTypes.cashOnDelivery).required(),
		).unique().label("Service")
			.optional(),

		color: Joi.array().items(
			Joi.string().required(),
		).unique().label("Color")
			.optional(),

		quantity: Joi.number().min(0).optional(),
		quantityType: Joi.string().valid(quantityTypes.Meter, quantityTypes.Unit, quantityTypes.Kilogram).optional(),

		category: Joi.string().optional(),
		subCategory: Joi.string().optional().label("Sub Category"),

		gender: Joi.string().optional(),

		buttons: productJoiSchemas.button.optional(),

		isApproved: Joi.boolean().optional(),
		isAdminBlocked: Joi.boolean().optional(),

	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Product Replicate
exports.productReplicate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/productReplicate`;

	const schema = Joi.object().keys({
		productId: Joi.string().required(),
		quantity: Joi.number().min(1).max(10).optional(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
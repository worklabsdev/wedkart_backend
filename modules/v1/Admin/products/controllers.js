const responseHandler = require("../../../../utils/responseHandler");
const { slugCreator } = require("../../../../utils/commonFuns");

const { uploadFile } = require(`../../../../packages/upload/${appConfig.upload.name}`);

const { pageOffsetCreator } = require("../../../../services/commonServices");
const {
	productGetService, productsCount, productsList, productListingCommonWhereCreator, productUpdateService, productServiceMessages, productCreate,
} = require("../../../../services/productServices");
const { catWithSubCatDetails } = require("../../Admin/category/services");

//	Listings
const listing = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		//	Where Creator
		let whereJSON = {
	  $and: [
				{ isDeleted: false },
	  ],
		};
		whereJSON = productListingCommonWhereCreator(whereJSON, request.body);// Where Creator

		const result = await Promise.all([
			productsCount(whereJSON),
	  	productsList(whereJSON, undefined, request.body),
		]);

		const count = result[0];
		const products = result[1];

		return responseHandler.successHandler(response, null, { count, products });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.listing = listing;

//	Update Product
const productUpdate = async (request, response) => {
	try {
		let product = await productGetService(
			response,
			{ isDeleted: false },
		);

		if (request.body.category) { //	Category Check
			await catWithSubCatDetails(
				response,
				request.body.category,
				request.body.subCategory,
				{
					subCatBlockReturn: true, // Filtering Blocked Sub Cat
				},
			);	// Category with SubCategory Check
		}//	Category Check

		if (request.body.name) request.body.slug = slugCreator(request.body.name);

		if (request.files && request.files.productPic) { //     Image Upload
			const file = await uploadFile(request.files.productPic, "Product Pic");
			request.body.mainImage = file.file;
		}//     Image Upload

		delete request.body.productId;
		const newData = {
			...request.body,
		};

		product = await productUpdateService(
			{ _id: product._id },
			{ $set: newData },
			{ new: true },
		);

		return responseHandler.successHandler(response, productServiceMessages.ProductUpdateSuccess, { product });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.productUpdate = productUpdate;

//	Replcate Product
const productReplicate = async (request, response) => {
	try {
		let product = await productGetService(
			response,
			{ isDeleted: false, _id: request.body.productId },
		);
		product = JSON.parse(JSON.stringify(product));
		delete product._id;

		const productsMany = [];
		if (!request.body.quantity) request.body.quantity = 1;


		for (i = 0; i < request.body.quantity; i++) {
			productsMany.push({
				...product,
				createdAt: Date.now(),
				upatedAt: Date.now(),
			});
		}

		const products = await productCreate(productsMany);

		return responseHandler.successHandler(response, productServiceMessages.ProductReplicateSuccess, { products });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.productReplicate = productReplicate;
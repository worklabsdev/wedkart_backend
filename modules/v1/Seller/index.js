const pathPrefix = "/api/v1/seller";

app.use(`${pathPrefix}/`, require("./auth"));
app.use(`${pathPrefix}/files/`, require("./files"));
app.use(`${pathPrefix}/inquiry/`, require("./inquiry"));
app.use(`${pathPrefix}/notifications/`, require("./notifications"));
app.use(`${pathPrefix}/product`, require("./products"));
app.use(`${pathPrefix}/payments/`, require("./payments"));

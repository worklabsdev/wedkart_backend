const Joi = require("@hapi/joi");

const apiRef = "SellerProduct";

const { joiValidate } = require("../../../../packages/joi");

const { serviceTypes, quantityTypes, productJoiSchemas } = require("../../../../services/productServices");

//	///			Add Product
exports.addProduct = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addProduct`;

	const schema = Joi.object().keys({
		name: Joi.string().required().label("Product name"),

		aprice: Joi.number().min(0).required().label("Actual price"),
		dprice: Joi.number().min(0).required().label("Discount price"),

		description: Joi.string().required(),

		size: Joi.array().items(
			Joi.string().required(),
			// Joi.string().valid(sizeValues.string).required()
		).unique().label("Size"),

		service: Joi.array().items(
			Joi.string().valid(serviceTypes.return30Days, serviceTypes.cashOnDelivery).required(),
		).unique().label("Service"),

		color: Joi.array().items(
			Joi.string().required(),
		).unique().label("Color"),

		quantity: Joi.number().min(0).required().label("Quantity"),
		quantityType: Joi.string().valid(quantityTypes.Meter, quantityTypes.Unit, quantityTypes.Kilogram).required(),

		category: Joi.string().required(),
		subCategory: Joi.string().optional(),

		gender: Joi.string().optional(),

		isFeatured: Joi.boolean().optional(),

		buttons: productJoiSchemas.button.required(),

	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//		Edit Product
exports.editProduct = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/editProduct`;

	const schema = Joi.object().keys({
		productId: Joi.string().required().label("Product"),

		name: Joi.string().required().label("Product name"),

		aprice: Joi.number().min(0).required().label("Actual price"),
		dprice: Joi.number().min(0).required().label("Discount price"),

		description: Joi.string().required().label("Description"),

		size: Joi.array().items(
			Joi.string().optional(),
		).label("Size").optional(),

		service: Joi.array().items(
			Joi.string().valid(serviceTypes.return30Days, serviceTypes.cashOnDelivery).optional(),
		).unique().label("Service")
			.optional(),

		color: Joi.array().items(
			Joi.string().optional(),
		).unique().label("Color")
			.optional(),

		quantity: Joi.number().min(0).required().label("Quantity"),
		quantityType: Joi.string().valid(quantityTypes.Meter, quantityTypes.Unit, quantityTypes.Kilogram).required(),

		category: Joi.string().optional(),
		subCategory: Joi.string().optional(),

		gender: Joi.string().optional(),

		isFeatured: Joi.boolean().optional(),

		buttons: productJoiSchemas.button.required(),

	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Product Replicate
exports.productReplicate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/productReplicate`;

	const schema = Joi.object().keys({
		productId: Joi.string().required(),
		quantity: Joi.number().min(1).max(10).optional(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Product Main Image Update
exports.mainImage = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/mainImage`;

	const schema = Joi.object().keys({
		productId: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Product Images Upload
exports.addImage = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addImage`;

	const schema = Joi.object().keys({
		productId: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Product Images Delete
exports.deleteImage = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addImage`;

	const schema = Joi.object().keys({
		productId: Joi.string().required(),
		imageIndex: Joi.number().min(0).required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	All Products Listings
exports.allProducts = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/allProducts`;

	const schema = Joi.object().keys({
		isFeatured: Joi.boolean().optional(),
		categorys: Joi.array().items(Joi.string().required()).optional(),
		subCategorys: Joi.array().items(Joi.string().required()).optional(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
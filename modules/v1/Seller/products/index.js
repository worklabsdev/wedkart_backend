const express = require("express");

const router = express.Router();

const { sellerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/", sellerVerifyMiddleware, multipartMiddleware, validators.addProduct, controllers.addProduct);

router.put("/:productId", sellerVerifyMiddleware, validators.editProduct, controllers.editProduct);

router.post("/:productId/replicate", sellerVerifyMiddleware, validators.productReplicate, controllers.productReplicate);

router.post("/:productId/main-image", sellerVerifyMiddleware, multipartMiddleware, validators.mainImage, controllers.mainImage);

router.post("/:productId/image", sellerVerifyMiddleware, multipartMiddleware, validators.addImage, controllers.addImage);
router.post("/:productId/image/:imageIndex", sellerVerifyMiddleware, validators.deleteImage, controllers.deleteImage);

router.post("/all", sellerVerifyMiddleware, validators.allProducts, controllers.allProducts);


module.exports = router;

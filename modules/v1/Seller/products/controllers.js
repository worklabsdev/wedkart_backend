require("../../../../plugins/jsend");

const productMdl = require("../../../../models/product");

const responseHandler = require("../../../../utils/responseHandler");
const { slugCreator, generateUniqueCode } = require("../../../../utils/commonFuns");

const { uploadFile } = require(`../../../../packages/upload/${appConfig.upload.name}`);

const { catWithSubCatDetails } = require("../../Admin/category/services");

const {
	productServiceMessages, productCreate, productUpdateService, productGetService, productListingCommonWhereCreator, productsListNoPaginate
} = require("../../../../services/productServices");

//	Add Product
const addProduct = async (request, response) => {
	try {
		await catWithSubCatDetails(
			response,
			request.body.category,
			request.body.subCategory,
			{
				subCatBlockReturn: false, // Filtering Blocked Sub Cat
			},
		);	// Category with SubCategory Check

		if (request.files && request.files.productImage) { //     Image Upload
			const file = await uploadFile(request.files.productImage, "Product Pic");
			request.body.mainImage = file.file;
		}//     Image Upload

		request.body.slug = slugCreator(request.body.name);
		request.body.replicateId = generateUniqueCode();

		request.body.buttons = JSON.parse(request.body.buttons);
		request.body.buttons.buyNow.amount = request.body.dprice || request.body.aprice;

		const prod = await productCreate({
			...request.body,
			seller: { // Old Code
				id: request.sellerDetails._id,
				email: request.sellerDetails.email,
			}, // Old Code
		});

		return responseHandler.successHandler(response, productServiceMessages.ProductCreateSuccess, { 
			prod
		});

	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.addProduct = addProduct;

//		Update Product
const editProduct = async (request, response) => {
	try {
		const product = await productGetService(
			response,
			{ isDeleted: false, _id: request.body.productId, "seller.id": request.sellerDetails._id },
		);

		delete request.body.productId;

		request.body.buttons = JSON.parse(request.body.buttons);
		request.body.buttons.buyNow.amount = request.body.dprice || request.body.aprice;

		const prod = await productMdl.update(
			{ _id: product._id },
			{
				$set: {
					...request.body,
					updatedAt: Date.now(),
				},
			},
			{
				new: true,
			},
		);

		return responseHandler.successHandler(response, productServiceMessages.ProductUpdateSuccess, { 
			prod,
			data: request.body,
			files: request.files
		});

	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.editProduct = editProduct;

//	Replcate Product
const productReplicate = async (request, response) => {
	try {
		let product = await productGetService(
			response,
			{ isDeleted: false, _id: request.body.productId, "seller.id": request.sellerDetails._id },
		);
		product = JSON.parse(JSON.stringify(product));
		delete product._id;

		const productsMany = [];
		if (!request.body.quantity) request.body.quantity = 1;

		for (i = 0; i < request.body.quantity; i++) {
			productsMany.push({
				...product,
				createdAt: Date.now(),
				upatedAt: Date.now(),
			});
		}

		const products = await productCreate(productsMany);

		return responseHandler.successHandler(response, productServiceMessages.ProductReplicateSuccess, { products });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.productReplicate = productReplicate;

//	Update Main Image
const mainImage = async (request, response) => {
	try {
		let product = await productGetService(
			response,
			{ isDeleted: false, _id: request.body.productId, "seller.id": request.sellerDetails._id },
		);
		product = JSON.parse(JSON.stringify(product));

		if (request.files && request.files.productImage) { //     Image Upload
			const file = await uploadFile(request.files.productImage, "Product Pic");
			request.body.mainImage = file.file;
		}//     Image Upload

		if (!request.body.mainImage) return responseHandler.failedActionHandler(response, productServiceMessages.ImageReq);

		product = await productUpdateService(
			{ _id: product._id },
			{
				$set: {
					mainImage: request.body.mainImage,
					updatedAt: Date.now(),
				},
			},
			{
				new: true,
			},
		);

		return responseHandler.successHandler(response, productServiceMessages.ImageUpdateSuccess, { url: request.body.mainImage });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.mainImage = mainImage;

//	Upload Images
const addImage = async (request, response) => {
	try {
		let product = await productGetService(
			response,
			{ isDeleted: false, _id: request.body.productId, "seller.id": request.sellerDetails._id },
		);
		product = JSON.parse(JSON.stringify(product));

		if (request.files && request.files.productImage) { //     Image Upload
			const file = await uploadFile(request.files.productImage, "Product Pic");
			request.body.productImage = file.file;
		}//     Image Upload

		if (!request.body.productImage) return responseHandler.failedActionHandler(response, productServiceMessages.ImageReq);

		product = await productUpdateService(
			{ _id: product._id },
			{
				$set: {
					updatedAt: Date.now(),
				},
				$push: {
					images: request.body.productImage,
				},
			},
			{
				new: true,
			},
		);

		return responseHandler.successHandler(response, productServiceMessages.ImageUpdateSuccess, { url: request.body.productImage });
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.addImage = addImage;

//	Delete Images
const deleteImage = async (request, response) => {
	try {
		let product = await productGetService(
			response,
			{ isDeleted: false, _id: request.body.productId, "seller.id": request.sellerDetails._id },
		);
		product = JSON.parse(JSON.stringify(product));

		const { images } = product;
		images.splice(request.body.imageIndex, 1);

		product = await productUpdateService(
			{ _id: product._id },
			{
				$set: {
					images,
					updatedAt: Date.now(),
				},
			},
			{
				new: true,
			},
		);

		return responseHandler.successHandler(response, productServiceMessages.ImageDelSuccess, {});
	} catch (error) {
		return responseHandler.errorHandler(response, error);
	}
};
exports.deleteImage = deleteImage;

//		All Products Listings
const allProducts = async (request, response) => {
try {

		//	Where Creator
		let whereJSON = {
			$and: [
				{ "seller.id": request.sellerDetails._id },
				{ isAdminBlocked: false },
				{ isDeleted: false }
			],
		};
		whereJSON = productListingCommonWhereCreator(whereJSON, request.body);// Where Creator

		let products = await productsListNoPaginate(whereJSON, {
			_id: 1,
			name: 1,
			slug: 1,
			aprice: 1,
			dprice: 1,
			buttons: 1
		}, request.body);

		return responseHandler.successHandler(response, null, {
			//whereJSON,
			products
		});

} catch(error) {
	return responseHandler.errorHandler(response, error);
}
}
exports.allProducts = allProducts;

// //		Update Sub Cat
// const allProducts = async (request, response) => {
// try {

// } catch(error) {
// 	return responseHandler.errorHandler(response, error);
// }
// }
// exports.allProducts = allProducts;

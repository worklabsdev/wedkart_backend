const express = require("express");

const router = express.Router();

const { sellerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/add", sellerVerifyMiddleware, multipartMiddleware, validators.upload, controllers.upload);

router.post("/list", sellerVerifyMiddleware, validators.list, controllers.list);

router.get("/:id", sellerVerifyMiddleware, validators.details, controllers.details);

router.put("/:id", sellerVerifyMiddleware, multipartMiddleware, validators.update, controllers.update);

router.delete("/:id", sellerVerifyMiddleware, validators.deleteFile, controllers.deleteFile);

router.post("/bookingForm", multipartMiddleware, sellerVerifyMiddleware, validators.bookingForm, controllers.bookingForm);

module.exports = router;

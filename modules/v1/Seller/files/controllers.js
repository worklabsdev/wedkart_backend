const responseHandler = require("../../../../utils/responseHandler");

const { uploadFile } = require(`../../../../packages/upload/${appConfig.upload.name}`);

const { globalMessages } = require("../../../../config/properties/constants");

const {
	fileTypesSer, createFile, countFiles, listFiles, detailFile, checkOData4FileUpload, filesListingCommonWhereCreator, updateFile,
} = require("../../../../services/fileServices");
const { sellerUpdateSer } = require("../../../../services/sellerServices");
const { pageOffsetCreator } = require("../../../../services/commonServices");

//	////		Upload File
const upload = async (request, response) => {
	try {
		await checkOData4FileUpload(response, request.body);

		const result = {
			...request.body,
			seller: request.sellerDetails._id,
			creatorSeller: request.sellerDetails._id,
		};

		if (!request.files && !request.files.upload) throw (new Error(`File ${globalMessages.responseMessages.FileUploadError}`));

		let file = await uploadFile(request.files.upload, "File");
		result.file = file.file;
		result.fileType = file.fileType;
		result.fileExt = file.fileExt;
		result.fileSize = file.fileSize;
		result.type = fileTypesSer.normal;

		file = await createFile(result);

  		return responseHandler.successHandler(response, `File ${globalMessages.responseMessages.FileUploadSuccess}`, { file });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.upload = upload;

//  Files List
const list = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		let whereJSON = {
			$and: [
				{ type: fileTypesSer.normal },
				{ isDeleted: false },
				{ creatorSeller: request.sellerDetails._id },
			],
		};
		whereJSON = filesListingCommonWhereCreator(whereJSON, request.body);

		const result = await Promise.all([
			countFiles(whereJSON),
			listFiles(whereJSON, undefined, request.body),
		]);

		return responseHandler.successHandler(response, null, {
			count: result[0],
			files: result[1]
		});

	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.list = list;

//  File Details
const details = async (request, response) => {
	try {
		const file = await detailFile(
			response,
			{
				type: fileTypesSer.normal,
				isDeleted: false,
				_id: request.body.id,
				creatorSeller: request.sellerDetails._id,
			},
		);

		return responseHandler.successHandler(response, null, { file });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.details = details;

//  File Update
const update = async (request, response) => {
	try {
		let file = await detailFile(
			response,
			{
				type: fileTypesSer.normal,
				isDeleted: false,
				_id: request.body.id,
				creatorSeller: request.sellerDetails._id,
			},
		);

		await checkOData4FileUpload(response, request.body);

		if (!request.files && !request.files.upload) throw (new Error(`File ${globalMessages.responseMessages.FileUploadError}`));

		const upload = await uploadFile(request.files.upload, "File");

		file = await updateFile(
			{ _id: request.body.id },
			{ 
				$set: {
					...request.body,
					file: upload.file,
					fileType: upload.fileType,
					fileExt: upload.fileExt,
					fileSize: upload.fileSize,
					updatedAt: Date.now(),
				}
			},
		);

		return responseHandler.successHandler(response, null, { file });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.update = update;

//  File Delete
const deleteFile = async (request, response) => {
	try {
		let file = await detailFile(
			response,
			{
				type: fileTypesSer.normal,
				isDeleted: false,
				_id: ObjectId(request.body.id),
				creatorSeller: ObjectId(request.sellerDetails._id),
			},
		);

		await updateFile(
			{ _id: request.body.id },
			{ 
				$set: {
					isDeleted: true,
					updatedAt: Date.now(),
				}
			}
		);

		return responseHandler.successHandler(response, null, {});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.deleteFile = deleteFile;

//		Update Booking Form
const bookingForm = async (request, response) => {
try {
	let elementName = "Booking Form";

	if (!request.files.upload) throw (new Error(`${elementName} ${globalMessages.responseMessages.imageUploadError}`));

	let file = await uploadFile(request.files.upload, elementName);
	request.body.bookingFile = file.file;

	let seller = await sellerUpdateSer(
		{ _id: request.sellerDetails._id },
		{
			$set: {
				bookingFile: file.file,
				updatedAt: Date.now()
			}
		}
	);

	return responseHandler.successHandler(response, `${elementName} ${globalMessages.responseMessages.FileUploadSuccess}`, { file });

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.bookingForm = bookingForm;
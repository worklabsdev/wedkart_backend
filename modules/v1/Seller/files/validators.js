const Joi = require("@hapi/joi");

const apiRef = "SellerFile";

const { joiValidate } = require("../../../../packages/joi");

//	///			Unique Check
exports.upload = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/upload`;

	const schema = Joi.object().keys({
		title: Joi.string().required(),
		description: Joi.string().optional(),
		seller: Joi.string().optional(),
		buyer: Joi.string().optional(),
		product: Joi.string().optional(),
		shop: Joi.string().optional(),
		admin: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Files List
exports.list = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/list`;

	const schema = Joi.object().keys({
		sellers: Joi.array().items(Joi.string().required()).optional(),
		buyers: Joi.array().items(Joi.string().required()).optional(),
		admins: Joi.array().items(Joi.string().required()).optional(),
		products: Joi.array().items(Joi.string().required()).optional(),
		shops: Joi.array().items(Joi.string().required()).optional(),
		skip: Joi.number().integer().min(0).optional(),
		limit: Joi.number().min(0).optional(),
		search: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			File Details
exports.details = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/details`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			File Update
exports.update = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/update`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
		title: Joi.string().required(),
		description: Joi.string().optional(),
		seller: Joi.string().optional(),
		buyer: Joi.string().optional(),
		product: Joi.string().optional(),
		shop: Joi.string().optional(),
		admin: Joi.string().optional(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			File Delete
exports.deleteFile = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deleteFile`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Update Booking Form
exports.bookingForm = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/bookingForm`;

	const schema = Joi.object().keys({
		// upload: Joi.object().keys({
        //     file: Joi.binary().required(),
        //     filename: Joi.string()
        // }).required()
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
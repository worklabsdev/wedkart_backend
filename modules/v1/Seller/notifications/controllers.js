const notificationModel = require("../../../../models/notifications");

const responseHandler = require("../../../../utils/responseHandler");
const mongoCommonFuncs = require("../../../../utils/dbs/mongo");

const { pageOffsetCreator } = require("../../../../services/commonServices");
const { notsCommonWhereCreator, notSenderRecTypes, notsList } = require("../../../../services/notificationServices");

//	Listings
const listing = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		request.body.receiverType = notSenderRecTypes.seller;

		//	Where Creator
		let whereJSON = {
			$and: [
				{ isDeleted: false },
				{ seller: request.sellerDetails._id },
			],
		};

		whereJSON = notsCommonWhereCreator(whereJSON, request.body);// Where Creator

		const unreadUpdateWhereJSON = {
			$and: [
				{ isDeleted: false },
				{ isRead: false },
				{ seller: request.sellerDetails._id },
				{ receiverType: request.body.receiverType },
			],
		};

		const result = await Promise.all([
			mongoCommonFuncs.count(notificationModel, whereJSON),
			notsList(whereJSON, undefined, request.body),
			mongoCommonFuncs.findMultipleAndUpdate(
				notificationModel,
				unreadUpdateWhereJSON,
				{
					$set: {
						isRead: true,
						updatedAt: Date.now(),
					},
				},
			),
		]);

		const count = result[0];
		const notifications = result[1];

		return responseHandler.successHandler(response, null, { data: request.sellerDetails, count, notifications });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.listing = listing;

//	Update
const notsUpdate = async (request, response) => {
	try {
		const unreadUpdateWhereJSON = {
			$and: [
				{ isDeleted: false },
				{ receiverType: notSenderRecTypes.seller },
				{ isRead: false },
				{ seller: request.sellerDetails._id },
			],
		};
		if (request.body.id) {
			unreadUpdateWhereJSON.$and.push({
				_id: request.body.id,
			});
		}
		if (request.body.unreadOnlyFilter) {
			unreadUpdateWhereJSON.$and.push({
				isRead: false,
			});
		}

		const newData = {
			updatedAt: Date.now(),
		};
		if (request.body.isRead) newData.isRead = true;
		if (request.body.isDeleted) newData.isDeleted = true;

		await mongoCommonFuncs.findMultipleAndUpdate(
			notificationModel,
			unreadUpdateWhereJSON,
			{ $set: newData },
		);

		return responseHandler.successHandler(response, null, {});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.notsUpdate = notsUpdate;


// //	Update Product
// const productUpdate = async (request, response) => {
// try {


// } catch(error) {
// 	return responseHandler.errorHandler(response, error, { data: request.body });
// }
// };
// exports.productUpdate = productUpdate;

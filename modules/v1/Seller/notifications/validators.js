const Joi = require("@hapi/joi");

const apiRef = "SellerNotification";

const { joiValidate } = require("../../../../packages/joi");

//	Listing
exports.listing = async (request, response, next) => {
  	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/listing`;

	const schema = Joi.object().keys({
		skip: Joi.number().integer().min(0).optional(),
		limit: Joi.number().min(0).optional(),
		unreadOnlyFilter: Joi.boolean().optional(),
  	});

  	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Update
exports.notsUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/notsUpdate`;

	const schema = Joi.object().keys({
		isRead: Joi.boolean().optional(),
		isDeleted: Joi.boolean().optional(),
		id: Joi.string().optional(),
	}).or("isRead", "isDeleted");

	request.body = {
	  ...request.body,
	  ...request.params,
  	};

  	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
  	if (valCheck === true) next();
};

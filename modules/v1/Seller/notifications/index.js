const router = express.Router();

const { sellerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", sellerVerifyMiddleware, validators.listing, controllers.listing);

router.put("/update", sellerVerifyMiddleware, validators.notsUpdate, controllers.notsUpdate);

module.exports = router;

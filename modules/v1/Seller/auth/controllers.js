const { globalMessages } = require("../../../../config/properties/constants");

const responseHandler = require("../../../../utils/responseHandler");
const { generateRandStr } = require("../../../../utils/commonFuns");
const { generatePassword } = require("../../../../utils/password");
const { sellerMailTypes, sellerSendMailFunc } = require("../../../../utils/Emails/sellerMails");

const { uploadFile } = require(`../../../../packages/upload/${appConfig.upload.name}`);

const sellerMdl = require("../../../../models/seller");

const { sellerMessages, sellerGetService, sellerGet, createSeller, sellerUpdateSer, sellerProfileCreatorSer } = require("../../../../services/sellerServices");
const { productsUpdateSer } = require("../../../../services/productServices");

//	Social Login
const socialLogin = async (request, response) => {
	try {
		const findJSON = {
			$and: [
				{ isDeleted: false },
			],
		};
		if (request.body.facebookId) {
			findJSON.$and.push({
				facebookId: { $ne: "" },
				facebookId: request.body.facebookId,
			});
		} else if (request.body.googleId) {
			findJSON.$and.push({
				googleId: { $ne: "" },
				googleId: request.body.googleId,
			});
		}

		let seller = await sellerGetService(response, findJSON);
		if (!seller) return responseHandler.failedActionHandler(response, sellerMessages.AccountNotFound);
		if (!seller.isConfirmed) return responseHandler.failedActionHandler(response, sellerMessages.VerifyAccountFirst);
		if (seller.isBlocked) return responseHandler.authHandler(response, 3);

		const token = generateRandStr(100);

		const newData = {
	  	$push: {
				token,
				logins: {
		  		token,
					valid: true,
		  		otherData: {},
				},
			},
			$set: {
				updatedAt: Date.now(),
			},
		};

		seller = await sellerMdl.findByIdAndUpdate(seller._id, newData, { new: true });

		return responseHandler.successHandler(response, sellerMessages.LogInSuccess, {
			token, name: seller.name, email: seller.email, _id: seller._id,
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.socialLogin = socialLogin;

//  Register Seller
const register = async (request, response) => {
	try {
		let seller = request.body;

		if (request.body.password) { // If Normal Signup
			const password = await generatePassword(request.body.password);

			seller.password = password.hash;
			seller.salt = password.salt;
		}// If Normal Signup

		const findJSON = {
			$and: [
				{ isDeleted: false },
			],
			$or: [
				{ email: request.body.email }
			],
		};
		if (request.body.facebookId) {
			findJSON.$or.push({
				facebookId: request.body.facebookId,
			});
		}
		if (request.body.googleId) {
			findJSON.$or.push({
				googleId: request.body.googleId,
			});
		}

		const userCheck = await sellerGet(findJSON);
		if (userCheck) {
			if (userCheck.email === request.body.email) return responseHandler.failedActionHandler(response, sellerMessages.EmailAlreadyTaken);
			if ((request.body.facebookId) && (userCheck.facebookId == request.body.facebookId)) return responseHandler.failedActionHandler(response, sellerMessages.FacebookAlreadyTaken);
			if ((request.body.googleId) && (userCheck.googleId == request.body.googleId)) return responseHandler.failedActionHandler(response, sellerMessages.GoogleAlreadyTaken);
		}

		seller.confirmCode = generateRandStr(100);

		seller = await createSeller(seller);
		seller = JSON.parse(JSON.stringify(seller));

		//	Register Email
		sellerSendMailFunc(
			sellerMailTypes.sellerRegisterEmailVerify,
			{
				email: seller.email,
				confirmCode: seller.confirmCode,
				reference: request.apiRefFull,
				requestId: request.requestId
			}
		);
		//	Register Email

		return responseHandler.successHandler(response, sellerMessages.SellerRegisterSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.register = register;

//	Confirm Account
const confirmAccount = async (request, response) => {
	try {
		const data = {
			...request.body,
			...request.params,
		};

		if (!data.confirmCode) {
			throw new Error(globalMessages.responseMessages.LinkExpired);
		}

		const seller = await sellerMdl.findOne({
			confirmCode: data.confirmCode,
		}).lean();
		if (!seller || seller.isConfirmed)
			throw new Error(globalMessages.responseMessages.LinkExpired);

		await sellerUpdateSer(
			{ _id: seller._id },
			{
				$set: {
					confirmCode: "",
					isConfirmed: true,
					updatedAt: Date.now(),
				}
			}
		);

		return responseHandler.successHandlerView(response, sellerMessages.AccountVerifiedSuccess, { appName: appConfig.appName });
	} catch (error) {
		return responseHandler.errorHandlerView(response, error);
	}
};
exports.confirmAccount = confirmAccount;

//	Logout
const logout = async (request, response) => {
	try {
		const token = request.headers["x-access-token"] || request.headers.authorization || request.headers["x-auth-token"];

		await sellerMdl.update(
			{
				_id: request.sellerDetails._id,
				"logins.token": token,
			},
			{
				$set: {
					"logins.$.valid": false,
					"logins.$.socketId": null,
					"logins.$.updatedAt": Date.now(),
					updatedAt: Date.now(),
				},
				$pull: {
					token,
				},
			},
		);

		return responseHandler.successHandler(response, globalMessages.responseMessages.LogoutSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.logout = logout;

//		Company Update
const companyUpdate = async (request, response) => {
try {

	if (request.files && request.files.companyLogo) { //     Company Logo Upload
		const file = await uploadFile(request.files.companyLogo, "Company Logo");
		request.body.companyLogo = file.file;
	}//     Company Logo Upload

	if (request.files && request.files.gstCertificate) { //     Gst Certificate Logo Upload
		const file = await uploadFile(request.files.gstCertificate, "Gst Certificate");
		request.body.gstCertificate = file.file;
	}//     Gst Certificate Logo Upload

	if (request.files && request.files.cancelCheck) { //     Cancel Check Upload
		const file = await uploadFile(request.files.cancelCheck, "Cancel Check");
		request.body.cancelCheck = file.file;
	}//     Cancel Check Upload

	if (request.files && request.files.pancard) { //     Pancard Upload
		const file = await uploadFile(request.files.pancard, "Pancard");
		request.body.pancard = file.file;
	}//     Pancard Upload

	if (request.files && request.files.companyCertificate) { //     Company Certificate Upload
		const file = await uploadFile(request.files.companyCertificate, "Company Certificate");
		request.body.companyCertificate = file.file;
	}//     Company Certificate Upload

	if(!request.body.productVisible) {//	Products Update Make Invisible
		request.body.productUpdate = await productsUpdateSer(
			{
				"seller.id": request.sellerDetails._id,
				isDeleted: false
			},
			{
				$set: {
					isVisible: false,
					updatedAt: Date.now()
				}
			}
		);
	}//	Products Update Make Invisible

	let seller = await sellerUpdateSer(
		{ _id: request.sellerDetails._id },
		{
			$set: {
				bussiness: {
					...request.sellerDetails.bussiness,
					...request.body
				},
				updatedAt: Date.now()
			}
		}
	);

	return responseHandler.successHandler(response, sellerMessages.BussinessInfoUpdatedMsg, {
		data: request.body,
		seller
	});

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.companyUpdate = companyUpdate;

//	Get Profile
const getProfile = async (request, response) => {
try {

	let seller = sellerProfileCreatorSer(request.sellerDetails);

	return responseHandler.successHandler(response, null, {
		seller
	});

} catch (error) {
	return responseHandler.errorHandler(response, error, { data: request.body });
}
};
exports.getProfile = getProfile;
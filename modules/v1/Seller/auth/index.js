const router = express.Router();

const { sellerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/register", validators.register, controllers.register, controllers.register);

router.post("/socialLogin", validators.socialLogin, controllers.socialLogin);

router.get("/confirmAccount/:confirmCode", controllers.confirmAccount);

router.post("/logout", sellerVerifyMiddleware, validators.logout, controllers.logout);

router.post("/companyUpdate", multipartMiddleware, sellerVerifyMiddleware, validators.companyUpdate, controllers.companyUpdate);

router.get("/getProfile", sellerVerifyMiddleware, validators.getProfile, controllers.getProfile);

module.exports = router;

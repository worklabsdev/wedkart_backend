const Joi = require("@hapi/joi");

const apiRef = "SellerAuth";

const { joiValidate } = require("../../../../packages/joi");
const { geoJoiSchema } = require("../../../../config/properties/schemas");

//	Register
exports.register = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/register`;

	const schema = Joi.object().keys({
		name: Joi.string().required(),
		email: Joi.string().email().required(),
		password: Joi.string().optional(),
		facebookId: Joi.string().optional(),
		googleId: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Social Login
exports.socialLogin = async (request, response, next) => {
  	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/socialLogin`;

	const schema = Joi.object().keys({
		facebookId: Joi.string().optional(),
		googleId: Joi.string().optional(),

  	}).or("facebookId", "googleId");

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Logout
exports.logout = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/logout`;

  	const schema = Joi.object().keys({
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Company Update
exports.companyUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/companyUpdate`;

  	const schema = Joi.object().keys({
		name: Joi.string().required(),
		email: Joi.string().email().required(),

		phoneCode: Joi.string().max(5).optional(),
		phoneNumber: Joi.string().min(8).max(12).optional(),

		description: Joi.string().optional(),

		ownerName: Joi.string().required(),
		
		panNumber: Joi.string().optional(),
		personalNumber: Joi.string().optional(),
		licenceNumber: Joi.string().optional(),
		gstNumber: Joi.string().optional(),

		startTime: Joi.string().optional(),
		endTime: Joi.string().optional(),

		banner: Joi.string().optional(),
		productVisible: Joi.boolean().optional(),

		address: Joi.string().optional(),
		city: Joi.string().optional(),
		state: Joi.string().optional(),
		country: Joi.string().optional(),
		countryCode: Joi.string().optional(),
		pincode: Joi.string().optional(),
		geo: geoJoiSchema.optional()
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//		Get Profile
exports.getProfile = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/getProfile`;

  	const schema = Joi.object().keys({
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

const Joi = require("@hapi/joi");

const apiRef = "SellerInquiry";

const { joiValidate } = require("../../../../packages/joi");

const { inquiryStatuses } = require("../../../../services/inquiryServices");

//	///			List Inquiry
exports.listInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/listInquiry`;

	const schema = Joi.object().keys({
		skip: Joi.number().integer().min(0).optional(),
		limit: Joi.number().min(0).optional(),
		search: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Add Inquiry
exports.addInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addInquiry`;

	const schema = Joi.object().keys({

		product: Joi.string().required().label("Product"),

		name: Joi.string().required().label("Name"),
		email: Joi.string().required().label("Email"),
		phoneCode: Joi.string().required().label("Phone Code"),
		phoneNumber: Joi.string().required().label("Phone Number"),

		description: Joi.string().optional().label("Description"),

		status: Joi.string().optional().valid(inquiryStatuses.actionTaken, inquiryStatuses.completedStatus, inquiryStatuses.onHold, inquiryStatuses.cancelled, inquiryStatuses.pendingStatus).label("Description"),

	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Details Inquiry
exports.detailInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/detailInquiry`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Update Inquiry
exports.updateInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/updateInquiry`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
		status: Joi.string().valid(inquiryStatuses.actionTaken, inquiryStatuses.completedStatus, inquiryStatuses.onHold, inquiryStatuses.cancelled, inquiryStatuses.pendingStatus).required(),
		description: Joi.string().optional(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

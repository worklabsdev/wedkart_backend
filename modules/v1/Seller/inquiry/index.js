const express = require("express");

const router = express.Router();

const { sellerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", sellerVerifyMiddleware, validators.listInquiry, controllers.listInquiry);

router.post("/add", sellerVerifyMiddleware, validators.addInquiry, controllers.addInquiry);

router.get("/:id", sellerVerifyMiddleware, validators.detailInquiry, controllers.detailInquiry);

router.put("/:id", sellerVerifyMiddleware, validators.updateInquiry, controllers.updateInquiry);

module.exports = router;

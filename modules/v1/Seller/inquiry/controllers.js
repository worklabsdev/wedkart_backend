const helpers = require("./helpers");

const responseHandler = require("../../../../utils/responseHandler");

const { buyerMailTypes, buyerSendMailFunc } = require("../../../../utils/Emails/buyerMail");

const { buyerGetOrCreatePlusMail, buyerCreatorTypes } = require("../../../../services/buyerServices");
const { productGetService } = require("../../../../services/productServices");
const {
	inquiryMessages, inquiryCheck, inquiryUpdate, createInquiry, inquiryDetails, inquiryListingCommonWhereCreator, listInquirys, countInquiry, inquiryStatuses
} = require("../../../../services/inquiryServices");
const { pageOffsetCreator } = require("../../../../services/commonServices");

//	List Inquiry
const listInquiry = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		let whereJSON = {
			$and: [
				{ isBuyerDeleted: false },
				{ isSellerDeleted: false },
				{ seller: request.sellerDetails._id },
				{ "otp.otp": "" },
			],
		};
		whereJSON = inquiryListingCommonWhereCreator(whereJSON, request.body);

		const result = await Promise.all([
			countInquiry(whereJSON),
			listInquirys(whereJSON, undefined, request.body, { seller: 0, buyer: 1, product: 1 }),
		]);

		return responseHandler.successHandler(response, null, { 
			count: result[0],
			inquiries: result[1]
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.listInquiry = listInquiry;

//		Inquiry Add
const addInquiry = async (request, response) => {
	try {
		const product = await productGetService(
			response,
			{
				_id: request.body.product,
				"seller.id": request.sellerDetails._id,
				isAdminBlocked: false,
				isSellerBlocked: false,
				isDeleted: false,
			}
		);

		//	Buyer Details Get
		request.body.whereJSON = {
			isDeleted: false,
			email: request.body.email
		};
		request.body.projections = { _id: 1, email: 1 };
		request.body.seller = request.sellerDetails._id;
		let buyer = await buyerGetOrCreatePlusMail(
			request.body,
			buyerCreatorTypes.sellerInquiry
		);
		request.body.buyer = buyer._id;
		//	Buyer Details Get

		const newData = {
			...request.body,
			subCategory: product.subCategory,
			category: product.category,
			seller: product.seller.id,
			creatorSeller: request.sellerDetails._id,
			otp: {
				otp: "",
				otpValidity: null,
			},
			otherData: {
				newBuyer: buyer.newBuyer || false
			},
			status: inquiryStatuses.completedStatus
		};
		const inquiry = await createInquiry(newData);

		if(buyer.newBuyer) {//	Send Email To Buyer With Password
			buyerSendMailFunc(
				buyerMailTypes.buyerRegisterPassword,
				{
					email: request.body.email,
					password: request.body.decodedPass,
					reference: request.apiRefFull,
					requestId: request.requestId
				}
			);
		}//	Send Email To Buyer With Password

		return responseHandler.createdHandler(response, inquiryMessages.InquiryCreateSuccess, {
			inquiry
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.addInquiry = addInquiry;

//		Inquiry Detils
const detailInquiry = async (request, response) => {
	try {
		const inquiry = await inquiryDetails(
			response,
			{
				_id: request.body.id,
				isAdminBlocked: false,
				isSellerDeleted: false,
				isBuyerDeleted: false,
				seller: request.sellerDetails._id,
			},
		);

		return responseHandler.createdHandler(response, null, { inquiry });
	} catch (error) {
	    return responseHandler.errorHandler(response, error, {});
	}
};
exports.detailInquiry = detailInquiry;

//		Inquiry Update
const updateInquiry = async (request, response) => {
	try {
		let inquiry = await inquiryCheck(
			response,
			{
				_id: request.body.id,
				isAdminBlocked: false,
				isSellerDeleted: false,
				isBuyerDeleted: false,
				seller: request.sellerDetails._id,
			},
		);

		delete request.body.id;
		inquiry = await inquiryUpdate(
			{ _id: inquiry._id },
			{
				$set: {
					...request.body,
					updatedAt: Date.now(),
				},
			},
			{ new: true },
		);

		helpers.sendSocketNotification4Updation(inquiry._id);// Socket Notifiactions

		return responseHandler.createdHandler(response, inquiryMessages.InquiryUpdateSuccess, { inquiry });
	} catch (error) {
	    return responseHandler.errorHandler(response, error, {});
	}
};
exports.updateInquiry = updateInquiry;

// //		Update Sub Cat
// const detailInquiry = async (request, response) => {
// try {

// } catch(error) {
// 	    return responseHandler.errorHandler(response, error, {});
// }
// }
// exports.detailInquiry = detailInquiry;

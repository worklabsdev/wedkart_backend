const { inquiryGet4Notification } = require("../../../../services/inquiryServices");

const { globalSocketActionTypes, globalSocketEvents, globalSocketNameSpaces } = require("../../../../packages/sockets/constantSockets");
const { socketSellerBuyerEmit, socketAdminEmit } = require("../../../../packages/sockets/socketsFuncs");

//  Send Inquiry Socket Notifications
const sendSocketNotification4Updation = async (inquiryId) => {
	try {
		const inquiry = await inquiryGet4Notification({ _id: inquiryId });

		const socketData = { //	Socket Data
			inquiryId: inquiry._id,
			actionType: globalSocketActionTypes.enquiryUpdated,
			name: inquiry.name,
			email: inquiry.email,
			phoneNumber: inquiry.phoneNumber,
			status: inquiry.status,
		};

		socketSellerBuyerEmit(
			globalSocketEvents.notification,
			socketData, //	Socket Data
			globalSocketNameSpaces.buyer,
			{ //	OData
				detailId: inquiry.buyer._id,
			}, //	OData
		);

		socketAdminEmit(
			globalSocketEvents.notification,
			socketData,
		);
	} catch (error) {
		socketErrorLogger(
			null,
			null,
			error,
		);
	}
};
exports.sendSocketNotification4Updation = sendSocketNotification4Updation;

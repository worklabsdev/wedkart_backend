const { notTypes, notSenderRecTypes } = require("../../../../services/notificationServices");

//  Create Payment Notifications
const paymentCreateNotifications = (payment) => {
	const inquirysArray = [
		{ // Admin Notification
			admin: null,
			buyer: payment.buyer,
			seller: payment.seller,
			shop: payment.shop,
			product: payment.product,
			inquiry: null,
			payment: payment._id,
			category: payment.category,
			subCategory: payment.subCategory,

			message: "Payment created",
			senderType: notSenderRecTypes.buyer,
			receiverType: notSenderRecTypes.admin,
			notificationType: notTypes.admin.paymentCreate,
		}, // Admin Notification
		{ // Seller Notification
			admin: null,
			buyer: payment.buyer,
			seller: payment.seller,
			shop: payment.shop,
			product: payment.product,
			inquiry: null,
			payment: payment._id,
			category: payment.category,
			subCategory: payment.subCategory,

			message: "Payment created",
			senderType: notSenderRecTypes.buyer,
			receiverType: notSenderRecTypes.seller,
			notificationType: notTypes.admin.paymentCreate,
		}, // Seller Notification
	];

	return inquirysArray;
};
exports.paymentCreateNotifications = paymentCreateNotifications;

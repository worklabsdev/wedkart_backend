const Joi = require("@hapi/joi");

const apiRef = "SellerPayment";

const { joiValidate } = require("../../../../packages/joi");

const { paymentTypes, paymentStatuses } = require("../../../../services/paymentServices");
const { buttonTypes } = require("../../../../services/productServices");

//	Listing
exports.listing = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/listing`;

  	const schema = Joi.object().keys({
		skip: Joi.number().integer().min(0).optional(),
	  	limit: Joi.number().min(0).optional(),

		buyers: Joi.array().items(Joi.string().required()).optional(),

		categorys: Joi.array().items(Joi.string().required()).optional(),
		subCategorys: Joi.array().items(Joi.string().required()).optional(),

		products: Joi.array().items(Joi.string().required()).optional(),
		shops: Joi.array().items(Joi.string().required()).optional(),

		statuses: Joi.array().items(Joi.string().valid(paymentStatuses.success, paymentStatuses.disputed, paymentStatuses.refunded, paymentStatuses.partialRefunded).required()).optional(),

		paymentMethods: Joi.array().items(Joi.string().valid(paymentTypes.razorPay, paymentTypes.cash).required()).optional(),
	});

	request.body = {
	  ...request.body,
	  ...request.params,
  	};

  	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
  	if (valCheck === true) next();
};

//	Details
exports.details = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/details`;

  	const schema = Joi.object().keys({
		orderId: Joi.string().required(),
	});

	request.body = {
	  ...request.body,
	  ...request.params,
  	};

  	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
  	if (valCheck === true) next();
};

const express = require("express");

const router = express.Router();

const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/add", buyerVerifyMiddleware, multipartMiddleware, validators.upload, controllers.upload);

router.post("/list", buyerVerifyMiddleware, validators.list, controllers.list);

router.get("/:id", buyerVerifyMiddleware, validators.details, controllers.details);

router.put("/:id", buyerVerifyMiddleware, multipartMiddleware, validators.update, controllers.update);

router.delete("/:id", buyerVerifyMiddleware, multipartMiddleware, validators.deleteFile, controllers.deleteFile);

module.exports = router;
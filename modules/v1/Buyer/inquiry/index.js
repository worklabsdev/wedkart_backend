const express = require("express");

const router = express.Router();

const { buyerVerifyMiddleware, buyerVerifyMiddlewareSkip } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/add", buyerVerifyMiddlewareSkip, validators.addInquiry, controllers.addInquiry);

router.patch("/:id", buyerVerifyMiddlewareSkip, validators.updateInquiry, controllers.updateInquiry);

router.post("/:id/verifyOtp", buyerVerifyMiddlewareSkip, validators.verifyOtp, controllers.verifyOtp);

router.post("/:id/resendOtp", buyerVerifyMiddlewareSkip, validators.resendOtp, controllers.resendOtp);

router.get("/:id", buyerVerifyMiddlewareSkip, validators.detailInquiry, controllers.detailInquiry);

module.exports = router;
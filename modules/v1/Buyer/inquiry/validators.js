const Joi = require("@hapi/joi");

const apiRef = "BuyerInquiry";

const { joiValidate } = require("../../../../packages/joi");

//	///			Add Inquiry
exports.addInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addInquiry`;

	const schema = Joi.object().keys({
		product: Joi.string().required().label("Product"),

		name: Joi.when("jwtToken", {
			is: Joi.exist(),
			then: Joi.forbidden(),
			otherwise: Joi.string().required(),
		}).label("Name"),

		jwtToken: Joi.string().optional().label("Token"),

		email: Joi.string().optional().label("Email"),
		phoneCode: Joi.string().optional().label("Phone Code"),
		phoneNumber: Joi.string().optional().label("Phone Number"),

		description: Joi.string().optional().label("Description"),

	}).or("jwtToken", "name").with("name", ["email", "phoneCode", "phoneNumber"]);

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Update Inquiry
exports.updateInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/updateInquiry`;

	request.body = {
		...request.body,
		...request.params,
	};

	const schema = Joi.object().keys({
		id: Joi.string().required().label("Inquiry Id"),

		name: Joi.string().optional().label("Name"),
		email: Joi.string().optional().label("Email"),
		phoneCode: Joi.string().optional().label("Phone Code"),
		phoneNumber: Joi.string().optional().label("Phone Number"),
		description: Joi.string().optional().label("Description"),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Verify Otp
exports.verifyOtp = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/verifyOtp`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
		otp: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Resend Otp
exports.resendOtp = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/resendOtp`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Details Inquiry
exports.detailInquiry = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/detailInquiry`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
	});

	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
const inquiryModel = require("../../../../models/inquirys");
const notificationModel = require("../../../../models/notifications");

const constants = require("./constants");
const helpers = require("./helpers");

const responseHandler = require("../../../../utils/responseHandler");
const { buyerMailTypes, buyerSendMailFunc } = require("../../../../utils/Emails/buyerMail");
const { generateOTP, futureDtCheck } = require("../../../../utils/commonFuns");
const { generateJWT, decodeJWT } = require("../../../../utils/auth/auth");
const mongoCommonFuncs = require("../../../../utils/dbs/mongo");

const { buyerGetOrCreatePlusMail, buyerCreatorTypes } = require("../../../../services/buyerServices");
const { productGetService } = require("../../../../services/productServices");
const {
	inquiryMessages, createInquiry, inquiryDetails, inquiryCheck, inquiryUpdate, inquiryStatuses,
} = require("../../../../services/inquiryServices");

//	Add Inquiry
const addInquiry = async (request, response) => {
	try {
		const product = await productGetService(
			response,
			{
				_id: request.body.product,
				isAdminBlocked: false,
				isSellerBlocked: false,
				isDeleted: false,
			}
		);

		let sendNewPasswordMail = false;
		let skipOTP = 0;
		let oldInquiry = {};
		let message;
		if (request.body.jwtToken) {//	Token Already Present
			const validateJWT = decodeJWT(request.body.jwtToken);
			if (!validateJWT.success) return responseHandler.failedActionHandler(response, constants.messages.DetailsRequired, { skipOTP });

			oldInquiry = await inquiryModel.findOne({
				jwtToken: request.body.jwtToken,
			}).lean();
			if (!oldInquiry) return responseHandler.failedActionHandler(response, constants.messages.DetailsRequired, { skipOTP });

			delete oldInquiry._id;
			skipOTP = 1;

			message = inquiryMessages.InquiryCreateSuccess;
		}//	Token Already Present
		else {//	Token Not Present
			request.body.status = inquiryStatuses.pendingStatus;

			request.body.subCategory = product.subCategory;
			request.body.category = product.category;
			request.body.seller = product.seller.id;

			if(!request.buyerDetails) {//	Buyer Details Get
				request.body.whereJSON = {
					isDeleted: false,
					email: request.body.email
				};
				request.body.projections = { _id: 1, email: 1, name: 1 };
				request.buyerDetails = await buyerGetOrCreatePlusMail(
					request.body,
					buyerCreatorTypes.buyerInquiry
				);

				sendNewPasswordMail = request.buyerDetails.newBuyer;
			}//	Buyer Details Get

			request.body.buyer = request.buyerDetails._id;
			request.body.creatorBuyer = request.buyerDetails._id;
			request.body.email = request.buyerDetails.email;

			message = inquiryMessages.OTPVerifcationRequired;
		}//	Token Not Present

		let inquirysArray = [];
		if (skipOTP) {
			request.body.otp = {
				otp: "",
				otpValidity: null,
			};
		} else {
			request.body.otp = generateOTP();
		}

		const newData = {
			...request.body,
			...oldInquiry,
			createdAt: Date.now(),
			updatedAt: Date.now(),
		};

		let inquiry = await createInquiry(newData);

		if (skipOTP) { //     Notification Create
			inquiry = JSON.parse(JSON.stringify(inquiry));
			inquirysArray = helpers.inquiryCreateNotifications(inquiry);

			await mongoCommonFuncs.saveData(notificationModel, inquirysArray);
		}//     Notification Create

		inquiry = JSON.parse(JSON.stringify(inquiry));
		delete inquiry.otp;

		if (inquiry.jwtToken) {
			helpers.sendSocketNotification4Creation(inquiry._id);// Socket Notifiactions
		}

		if(sendNewPasswordMail) {//	Send Email To Buyer With Password
			buyerSendMailFunc(
				buyerMailTypes.buyerRegisterPassword,
				{
					email: request.body.email,
					password: request.body.decodedPass,
					reference: request.apiRefFull,
					requestId: request.requestId
				}
			);
		}//	Send Email To Buyer With Password


		return responseHandler.createdHandler(response, message, {
			_id: inquiry._id,
			jwtToken: inquiry.jwtToken,
			inquiry,
		});

	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.addInquiry = addInquiry;

//  Update Inquiry
const updateInquiry = async (request, response) => {
	try {
		let inquiry = await inquiryCheck(
			response,
			{
				_id: request.body.id,
				isAdminBlocked: false,
				isSellerDeleted: false,
				isBuyerDeleted: false,
				buyer: request.buyerDetails._id,
			},
		);
		if (inquiry.status !== inquiryStatuses.pendingStatus) return responseHandler.failedActionHandler(response, constants.messages.InquiryNoLongerUpdatedMsg);

		if (request.body.phoneNumber !== inquiry.phoneNumber) {
			request.body.otp = generateOTP();
		} else {
			request.body.otp = inquiry.otp;
		}

		inquiry = await inquiryUpdate(
			{ _id: inquiry._id },
			{
				name: request.body.name || inquiry.name,
				email: request.body.email || inquiry.email,
				phoneCode: request.body.phoneCode || inquiry.phoneCode,
				phoneNumber: request.body.phoneNumber || inquiry.phoneNumber,
				description: request.body.description || inquiry.description,
				otp: request.body.otp,
				updatedAt: Date.now(),
			},
			{
				new: true,
			},
		);
		inquiry = JSON.parse(JSON.stringify(inquiry));
		delete inquiry.otp;

		return responseHandler.successHandler(response, inquiryMessages.InquiryUpdateSuccess, {
			inquiry,
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.updateInquiry = updateInquiry;

//		Verify OTP
const verifyOtp = async (request, response) => {
	try {
		let inquiry = await inquiryCheck(
			response,
			{
				_id: request.body.id,
				isAdminBlocked: false,
				isSellerDeleted: false,
				isBuyerDeleted: false,
				// buyer: request.buyerDetails._id
			},
		);

		if (inquiry.otp.pendingStatus) return responseHandler.failedActionHandler(response, constants.messages.OTPAlreadyVerif);
		if (inquiry.otp.otp === "") return responseHandler.failedActionHandler(response, constants.messages.OTPVerifNotRequired);

		// ///       Expiry Check
		if (request.body.otp !== inquiry.otp.otp) return responseHandler.failedActionHandler(response, constants.messages.IncorrectOTP);

		const checkOTPValidity = futureDtCheck(inquiry.otp.otpValidity);
		if (!checkOTPValidity) return responseHandler.failedActionHandler(response, constants.messages.OtpExpired);
		// ///       Expiry Check

		request.body.jwtToken = generateJWT({
			parentInquiryId: inquiry._id,
		});

		inquiry = await inquiryUpdate(
			{ _id: inquiry._id },
			{
				otp: {
					otp: "",
					otpValidity: null,
					otpVerified: true,
				},
				jwtToken: request.body.jwtToken,
				status: inquiryStatuses.pendingStatus,
				updatedAt: Date.now(),
			},
			{
				new: true,
			},
		);

		//  Notification
		inquiry = JSON.parse(JSON.stringify(inquiry));
		const inquirysArray = helpers.inquiryCreateNotifications(inquiry);

		await mongoCommonFuncs.saveData(notificationModel, inquirysArray);

		helpers.sendSocketNotification4Creation(inquiry._id);// Socket Notifiactions
		//  Notification

		return responseHandler.successHandler(response, inquiryMessages.InquiryCreateSuccess, { jwtToken: request.body.jwtToken });
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.verifyOtp = verifyOtp;

//		Resend OTP
const resendOtp = async (request, response) => {
	try {
		let inquiry = await inquiryCheck(
			response,
			{
				_id: request.body.id,
				isAdminBlocked: false,
				isSellerDeleted: false,
				isBuyerDeleted: false,
				// /buyer: request.buyerDetails._id
			},
		);

		otp = generateOTP();

		inquiry = await inquiryUpdate(
			{ _id: inquiry._id },
			{
				$set: {
					otp,
					updatedAt: Date.now(),
				},
			},
		);

		return responseHandler.createdHandler(response, constants.messages.OTPSentSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.resendOtp = resendOtp;


//		Inquiry Detils
const detailInquiry = async (request, response) => {
	try {
		const inquiry = await inquiryDetails(
			response,
			{
				_id: request.body.id,
				isAdminBlocked: false,
				isSellerDeleted: false,
				isBuyerDeleted: false,
				buyer: request.buyerDetails._id,
			},
		);

		return responseHandler.createdHandler(response, null, { inquiry });
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.detailInquiry = detailInquiry;
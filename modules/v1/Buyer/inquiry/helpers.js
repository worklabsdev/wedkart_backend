const { notTypes, notSenderRecTypes } = require("../../../../services/notificationServices");

const { inquiryGet4Notification } = require("../../../../services/inquiryServices");

const { globalSocketActionTypes, globalSocketEvents, globalSocketNameSpaces } = require("../../../../packages/sockets/constantSockets");
const { socketSellerBuyerEmit, socketAdminEmit } = require("../../../../packages/sockets/socketsFuncs");

const { socketErrorLogger, socketInfoLogger } = require("../../../../packages/logging/consoleLogger");

//      Inquiry Create Notification
const inquiryCreateNotifications = (inquiry, opts) => {
	const inquirysArray = [
		{ // Admin Notification
			admin: inquiry.creatorAdmin,
			buyer: inquiry.buyer,
			seller: inquiry.seller,

			shop: inquiry.shop,
			product: inquiry.product,
			inquiry: inquiry._id,

			category: inquiry.category,
			subCategory: inquiry.subCategory,

			message: "Inquiry created",

			senderType: notSenderRecTypes.buyer,
			receiverType: notSenderRecTypes.admin,
			notificationType: notTypes.admin.inquiryCreate,
		}, // Admin Notification
		{ // Seller Notification
			admin: inquiry.creatorAdmin,
			buyer: inquiry.buyer,
			seller: inquiry.seller,

			shop: inquiry.shop,
			product: inquiry.product,
			inquiry: inquiry._id,

			category: inquiry.category,
			subCategory: inquiry.subCategory,

			message: "Inquiry created",

			senderType: notSenderRecTypes.buyer,
			receiverType: notSenderRecTypes.seller,
			notificationType: notTypes.admin.inquiryCreate,
		}, // Seller Notification
	];

	return inquirysArray;
};
exports.inquiryCreateNotifications = inquiryCreateNotifications;

//  Send Inquiry Socket Notifications
const sendSocketNotification4Creation = async (inquiryId) => {
	try {
		const inquiry = await inquiryGet4Notification({ _id: inquiryId });

		const socketData = { //	Socket Data
			inquiryId: inquiry._id,
			actionType: globalSocketActionTypes.enquiryCreated,
			name: inquiry.name,
			email: inquiry.email,
			phoneNumber: inquiry.phoneNumber,
			status: inquiry.status,
		};

		socketSellerBuyerEmit(
			globalSocketEvents.notification,
			socketData, //	Socket Data
			globalSocketNameSpaces.seller,
			{ //	OData
				detailId: inquiry.seller._id,
			}, //	OData
		);

		socketAdminEmit(
			globalSocketEvents.notification,
			socketData,
		);
	} catch (error) {
		socketErrorLogger(
			null,
			null,
			error,
		);
	}
};
exports.sendSocketNotification4Creation = sendSocketNotification4Creation;
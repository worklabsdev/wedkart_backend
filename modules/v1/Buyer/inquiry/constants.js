const projections = {
};
exports.projections = projections;

//  Messages
const messages = {
	IncorrectOTP: "Invalid OTP",
	OtpExpired: "This OTP has expired",
	OTPSentSuccess: "OTP sent successfully",
	OTPVerifSuccess: "OTP verified successfully",
	OTPAlreadyVerif: "OTP already verified",
	OTPVerifNotRequired: "OTP veification not required",
	DetailsRequired: "Details are required",
	InquiryNoLongerUpdatedMsg: "Sorry, this inquiry can only be updated before OTP verification",
};
exports.messages = messages;

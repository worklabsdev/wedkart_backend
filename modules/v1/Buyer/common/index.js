const router = express.Router();

const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.get("/getcategory", validators.getCategories, controllers.getCategories);

module.exports = router;

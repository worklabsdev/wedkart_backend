const Joi = require("@hapi/joi");

const apiRef = "BuyerCommon";

const { joiValidate } = require("../../../../packages/joi");

//	Listing
exports.getCategories = async (request, response, next) => {
  	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/getCategories`;

	next();
};

const crypto = require("crypto");

const notificationModel = require("../../../../models/notifications");

const helpers = require("./helpers");

const { appConstants } = require("../../../../config/properties/constants");

const responseHandler = require("../../../../utils/responseHandler");
const { startOfDt, endOfDt, diffInDts } = require("../../../../utils/moment");
const { generateUniqueCode } = require("../../../../utils/commonFuns");
const mongoCommonFuncs = require("../../../../utils/dbs/mongo");

//	Socket Necessary
const { globalSocketEvents, globalSocketActionTypes, globalSocketNameSpaces } = require("../../../../packages/sockets/constantSockets");
const { socketSellerBuyerEmit, socketAdminEmit } = require("../../../../packages/sockets/socketsFuncs");
//	Socket Necessary

const { getOneAdmin, adminFeeTypes, defaultAdminFee } = require("../../../../services/adminServices");
const { pageOffsetCreator } = require("../../../../services/commonServices");
const { productDetails4Purchase } = require("../../../../services/productServices");
const { listFiles } = require("../../../../services/fileServices");
const paymentServices = require("../../../../services/paymentServices");

const paymentGateway = require(`../../../../packages/payments/${appConfig.payment.name}`);

//	Initiate Payment
const initiate = async (request, response) => {
	try {
		const product = await productDetails4Purchase(response, request.body.product);
		const admin = await getOneAdmin({});

		// return response.json({
		// 	admin,
		// 	product
		// });s

		//	Start End DateTime Diff Check
		request.body.startDt = startOfDt(request.body.startDt);
		request.body.endDt = endOfDt(request.body.endDt);
		request.body.startEndDiffEnd = diffInDts(request.body.startDt, request.body.endDt, "months");
		// if(request.body.startEndDiffEnd < paymentServices.paymentConstants.minMonths)
		// 	return responseHandler.failedActionHandler(response, paymentServices.paymentMessages.MinMonthError, {
		// 		data: request.body
		// 	});
		//	Start End DateTime Diff Check

		//	Buyer Files Get
		const buyerDocs = await listFiles(
			{
				isBlocked: false,
				isDeleted: false,
				creatorBuyer: request.buyerDetails._id,
			},
			{
				_id: 1,
				file: 1,
				fileType: 1,
				fileExt: 1,
				fileSize: 1,
				title: 1,
				description: 1,
			},
		);
		//	Buyer Files Get

		const tempBtn = product.buttons[request.body.buttonType];

		request.body.subAmount = tempBtn.amount;
		request.body.enabled = tempBtn.enabled;

		request.body.subAmountComplete = request.body.subAmount * request.body.startEndDiffEnd;

		request.body.shippingCharges = 0;
		request.body.tax = {
			amount: 0,
			types: [],
		};

		//	Admin Fee
		if (admin && admin.adminFee) {
			request.body.adminFee = admin.adminFee;
		} else {
			request.body.adminFee = defaultAdminFee;
		}
		//	Admin Fee

		if (request.body.adminFee.value && request.body.subAmountComplete) {
			let amount = 0;
			if (request.body.adminFee.type === adminFeeTypes.value) { //	Value Type
				amount = request.body.adminFee.value;
			}//	Value Type
			else { //	Percentage Type
				const tax = (request.body.subAmountComplete * request.body.adminFee.value) / 100;
				amount = Math.round(tax, 3);
			}//	Percentage Type

			request.body.adminFee = {
				...request.body.adminFee,
				amount,
			};
			delete request.body.adminFee.$init;
		}

		request.body.totalAmount = request.body.subAmountComplete + request.body.adminFee.amount + request.body.tax.amount;

		request.body.buyer = request.buyerDetails._id;
		request.body.seller = product.seller.id._id;
		request.body.category = product.category._id;
		request.body.subCategory = product.subCategory;
		request.body.buyerDetails = {
			address: {
				address: request.body.address || "",
				city: request.body.city || "",
				state: request.body.state || "",
				country: request.body.country || "",
				pincode: request.body.pincode || "",
			},
			contactDetails: {
				name: request.body.name || request.buyerDetails.name,
				email: request.body.email || request.buyerDetails.email,
				phoneCode: request.body.phoneCode || request.buyerDetails.phoneCode,
				phoneNumber: request.body.phoneNumber || request.buyerDetails.phone,
			},
			buyerDocs,
		};

		let sendNotifications = 0;
		request.body.orderId = generateUniqueCode();

		if (request.body.paymentMethod === paymentServices.paymentTypes.razorPay) { //	Razor Payment
			message = paymentServices.paymentMessages.ProductPurchaseSuccess;

			const razorOptions = {
				amount: request.body.totalAmount * 100, // In Paise
				currency: "INR",
				receipt: request.body.orderId,
				payment_capture: "1",
				notes: {
					orderId: request.body.orderId,
				},
			};

			const razorPayOrder = await paymentGateway.createRazorPayOrder(response, razorOptions);

			request.body.gatewayLogs = [
				{
					entity: "createOrder",
					request: razorOptions,
					response: razorPayOrder,
					odata: {
						ip: request.connection.remoteAddress || request.headers["x-forwarded-for"],
					},
				},
			];

			request.body.gatewayDetails = {
				[appConfig.payment.name]: {
					orderId: razorPayOrder.id,
				},
			};
		}//	Razor Payment
		else { //	Cash Payment
			sendNotifications = 1;
			message = paymentServices.paymentMessages.ProductPurchaseSuccess;

			request.body.gatewayDetails = {};
		}//	Cash Payment

		const payment = await paymentServices.createPayment(request.body);

		// result.buyerDetails = request.buyerDetails;
		// result.data = request.body;
		// result.product = product;
		// result.buyerDocs = buyerDocs;
		return responseHandler.successHandler(response, message, {
			orderId: payment.orderId,
			payment
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.initiate = initiate;

// Payment View
const payView = async (request, response) => {
	try {
		const payment = await paymentServices.paymentDetailsGet({
			orderId: request.params.orderId,
			// buyer: request.buyerDetails._id,
			isDeleted: false,
		});

		config = {
			appName: appConstants.name,
			razorPayKey: process.env.RAZOR_KEY,
		};

		// return responseHandler.successHandler(response, null, { payment });
		return response.render("Buyer/payView", { payment, config });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.payView = payView;

//	Update Payment
const payUpdate = async (request, response) => {
	try {
		const payments = await Promise.all([
			paymentServices.paymentDetailsGet(
				{
					orderId: request.body.orderId,
					buyer: request.buyerDetails._id,
					isDeleted: false,
				},
				{
					fullPops: 1,
				},
			),
			paymentGateway.getPaymentDetails(request.body.paymentId),
		]);

		payment = payments[0];//	Local Payment Details
		razorPayment = payments[1];//	Razor Payment Details

		if (!payment || !razorPayment) return responseHandler.failedActionHandler(response, paymentServices.paymentMessages.PaymentNotAvail);

		const updatedData = {
			updatedAt: Date.now(),
			paymentStatus: request.body.paymentStatus,
		};

		let message = "Payment updated successfully";
		let sendNotifications = 0;

		updatedData["gatewayDetails.razorPay.invoiceId"] = razorPayment.invoice_id;
		updatedData["gatewayDetails.razorPay.paymentId"] = razorPayment.id;

		if (request.body.paymentStatus === paymentServices.paymentStatuses.success) { //	Successfully Payment
			request.body.hash = crypto.createHmac("SHA256", paymentGateway.razorConfig.key_secret).update(`${payment.gatewayDetails.razorPay.orderId}|${request.body.paymentId}`)
				.digest("hex");//	Signature Match

			if (request.body.signature !== request.body.hash) { //	Hash Not Verified
				updatedData.gatewayFee = razorPayment.fee / 100;

				updatedData.paymentStatus = paymentServices.paymentStatuses.failedSignature;
				updatedData.paymentDetails = razorPayment;
				updatedData.paymentError = {
					code: razorPayment.error_code,
					description: razorPayment.error_description,
					odata: {},
				};

				message = paymentServices.paymentMessages.InvalidSignature;
			}//	Hash Not Verified
			else { //	Hash Verified
				sendNotifications = 1;
			}//	Hash Verified
		}//	Successfully Payment

		payment = await paymentServices.paymentUpdate(
			{ _id: payment._id },
			{ $set: updatedData },
			{ new: true, lean: true },
		);

		if (sendNotifications) { //  Socket Notification
			payment = JSON.parse(JSON.stringify(payment));
			const paymentsArray = helpers.paymentCreateNotifications(payment);

			await mongoCommonFuncs.saveData(notificationModel, paymentsArray);

			const socketData = { //	Socket Data
				orderId: payment.orderId,
				actionType: globalSocketActionTypes.productPurchase,
				paymentMethod: payment.paymentMethod,
				paymentStatus: payment.paymentStatus,
				currency: payment.currency,
				buyerDetails: payment.buyerDetails,
				totalAmount: payment.totalAmount,
			};

			socketSellerBuyerEmit(
				globalSocketEvents.notification,
				socketData, //	Socket Data
				globalSocketNameSpaces.seller,
				{ //	OData
					detailId: payment.seller,
				}, //	OData
			);

			socketAdminEmit(
				globalSocketEvents.notification,
				socketData,
			);
		}//  Socket Notification

		const result = {
			orderId: payment.orderId,
			paymentStatus: payment.paymentStatus,
		// payment
		};

		return responseHandler.successHandler(response, message, result);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.payUpdate = payUpdate;

//	List Payments
const listing = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		//	Where Creator
		let whereJSON = {
			$and: [
				{ isDeleted: false },
				{ isBlocked: false },
				{ buyer: request.buyerDetails._id },
			],
		};

		whereJSON = paymentServices.paymentListingCommonWhereCreator(whereJSON, request.body);

		let result = await Promise.all([
			paymentServices.countPayments(whereJSON),
			paymentServices.listPayments(whereJSON, undefined, request.body),
		]);

		result = {
			count: result[0],
			payments: result[1],
		};

		return responseHandler.successHandler(response, null, result);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.listing = listing;

//	Details of Product
const details = async (request, response) => {
	try {
		const payment = await paymentServices.paymentDetailsGet({
			orderId: request.body.orderId,
			buyer: request.buyerDetails._id,
			isDeleted: false,
		}).lean();
		if (!payment) return responseHandler.failedActionHandler(response, paymentServices.paymentMessages.PaymentNotAvail);

		return responseHandler.successHandler(response, null, { payment });
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.details = details;
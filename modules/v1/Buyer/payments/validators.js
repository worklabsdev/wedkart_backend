const Joi = require("@hapi/joi");

const apiRef = "BuyerPayment";

const { joiValidate } = require("../../../../packages/joi");

const { paymentTypes, paymentStatuses } = require("../../../../services/paymentServices");
const { buttonTypes } = require("../../../../services/productServices");

//	Initiate Payment
exports.initiate = async (request, response, next) => {
  	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/initiate`;

	const schema = Joi.object().keys({
		product: Joi.string().required(),
		paymentMethod: Joi.string().valid(paymentTypes.razorPay, paymentTypes.cash).required(),
		buttonType: Joi.string().valid(buttonTypes.payToken, buttonTypes.buyNow).required(),

		startDt: Joi.date().greater(Date.now()).required(),
		endDt: Joi.date().greater(Joi.ref("startDt")).required(),

		address: Joi.string().optional(),
		city: Joi.string().optional(),
		state: Joi.string().optional(),
		country: Joi.string().optional(),
		pincode: Joi.string().optional(),
	}).with("address", ["city", "state", "country", "pincode"]);

  	request.body = {
		...request.body,
		...request.params,
	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Update Payment
exports.payUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/payUpdate`;

	const schema = Joi.object().keys({
		paymentStatus: Joi.string().valid(paymentStatuses.success, paymentStatuses.failed).required(),

		orderId: Joi.string().required(),
		paymentId: Joi.string().required(),

		signature: Joi.when("paymentStatus", {
			is: Joi.exist().equal(paymentStatuses.success),
			then: Joi.string().required(),
			otherwise: Joi.forbidden(),
		}),

	});

	request.body = {
	  ...request.body,
	  ...request.params,
  	};

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Listing
exports.listing = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/listing`;

  	const schema = Joi.object().keys({
		skip: Joi.number().integer().min(0).optional(),
	  	limit: Joi.number().min(0).optional(),

		sellers: Joi.array().items(Joi.string().required()).optional(),

		categorys: Joi.array().items(Joi.string().required()).optional(),
		subCategorys: Joi.array().items(Joi.string().required()).optional(),

		products: Joi.array().items(Joi.string().required()).optional(),
		shops: Joi.array().items(Joi.string().required()).optional(),

		statuses: Joi.array().items(Joi.string().valid(paymentStatuses.success, paymentStatuses.failed, paymentStatuses.cancelled, paymentStatuses.disputed, paymentStatuses.refunded, paymentStatuses.partialRefunded).required()).optional(),

		paymentMethods: Joi.array().items(Joi.string().valid(paymentTypes.razorPay, paymentTypes.cash).required()).optional(),
	});

	request.body = {
	  ...request.body,
	  ...request.params,
  	};

  	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
  	if (valCheck === true) next();
};

//	Details
exports.details = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/details`;

  	const schema = Joi.object().keys({
		orderId: Joi.string().required(),
	});

	request.body = {
	  ...request.body,
	  ...request.params,
  	};

  	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
  	if (valCheck === true) next();
};
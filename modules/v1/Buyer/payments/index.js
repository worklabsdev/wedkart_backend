const router = express.Router();

const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/initiate", buyerVerifyMiddleware, validators.initiate, controllers.initiate);

router.get("/pay/:orderId", controllers.payView);// For Razor Pay Test Payment

router.post("/update", buyerVerifyMiddleware, validators.payUpdate, controllers.payUpdate);

router.post("/list", buyerVerifyMiddleware, validators.listing, controllers.listing);

router.get("/:orderId", buyerVerifyMiddleware, validators.details, controllers.details);

module.exports = router;
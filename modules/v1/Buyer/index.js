const pathPrefix = "/api/v1/buyer";

app.use(`${pathPrefix}/address/`, require("./address"));
app.use(`${pathPrefix}/`, require("./auth"));
app.use(`${pathPrefix}/`, require("./common"));
app.use(`${pathPrefix}/files/`, require("./files"));
app.use(`${pathPrefix}/inquiry/`, require("./inquiry"));
app.use(`${pathPrefix}/notifications/`, require("./notifications"));
app.use(`${pathPrefix}/payments/`, require("./payments"));
app.use(`${pathPrefix}/products/`, require("./products"));

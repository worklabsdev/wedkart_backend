const productMdl = require("../../../../models/product");

const homeListingProductsAggregate = (whereJSON) => productMdl.aggregate([
	{
		$match: whereJSON,
	},
	{
		$sort: { _id: -1 },
	},
	{
		$group: {
			_id: "$category",
			productId: { $addToSet: "$_id" },
			name: { $addToSet: "$name" },
			description: { $addToSet: "$description" },
			category: { $addToSet: "$category" },
			slug: { $addToSet: "$slug" },
			images: { $addToSet: "$images" },
			aprice: { $addToSet: "$aprice" },
			dprice: { $addToSet: "$dprice" },
			buttons: { $addToSet: "$buttons" },
		},
	},
	{
		$project: {

			_id: 1,
			productDetails: {
				_id: { $arrayElemAt: ["$productId", 0] },
				name: { $arrayElemAt: ["$name", 0] },
				description: { $arrayElemAt: ["$description", 0] },
				category: { $arrayElemAt: ["$category", 0] },
				slug: { $arrayElemAt: ["$slug", 0] },
				images: { $arrayElemAt: ["$images", 0] },
				aprice: { $arrayElemAt: ["$aprice", 0] },
				dprice: { $arrayElemAt: ["$dprice", 0] },
				buttons: { $arrayElemAt: ["$buttons", 0] },
			},
		},
	},
	{
		$lookup: {
			from: "categories",
			localField: "_id",
			foreignField: "_id",
			as: "categoryDetails",
		},
	},
	{
		$unwind: "$categoryDetails",
	},
	{
		$project: {
			_id: 1,
			productDetails: 1,
			"categoryDetails.name": 1,
			"categoryDetails._id": 1,
			"categoryDetails.description": 1,
		},
	},
]);
exports.homeListingProductsAggregate = homeListingProductsAggregate;
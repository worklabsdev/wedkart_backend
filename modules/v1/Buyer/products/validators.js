const Joi = require("@hapi/joi");

const apiRef = "BuyerProduct";

const { joiValidate } = require("../../../../packages/joi");

//	///			Cat Products
exports.homeListing = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/homeListing`;

	const schema = Joi.object().keys({
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Products Filtering
exports.filterListing = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/filterListing`;

	const schema = Joi.object().keys({
		skip: Joi.number().integer().min(0).optional(),
		limit: Joi.number().min(0).optional(),

		search: Joi.string().optional(),
		isFeatured: Joi.boolean().optional(),
		sellers: Joi.array().items(Joi.string().required()).optional(),
		categorys: Joi.array().items(Joi.string().required()).optional(),
		subCategorys: Joi.array().items(Joi.string().required()).optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};
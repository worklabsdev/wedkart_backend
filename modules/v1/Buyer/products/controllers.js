const responseHandler = require("../../../../utils/responseHandler");

const services = require("./services");
const { pageOffsetCreator } = require("../../../../services/commonServices");
const { productsCount, productsList, productListingCommonWhereCreator } = require("../../../../services/productServices");

const { categoryOnlyValidDetails } = require("../../Admin/category/helpers");
const { categoryAggregate } = require("../../Admin/category/services");

//	Categories with Featured Products Listings
const homeListing = async (request, response) => {
	try {
		const aggregate = await categoryOnlyValidDetails({
			allCatsFilter: "0",	// For Including Blocked Cats As Well
			allSubCatsFilter: "0",	// For Including Blocked Sub Cats as Well
			appListing: 1,
		});
		const categories = await categoryAggregate(aggregate);

		if (!categories.length) {
			return responseHandler.successHandler(response, null, {
				data: [],
			});
		}

		const categoryArray = categories.map(({ _id }) => _id);

		request.body.whereJSON = {
			isAdminBlocked: false,
			isSellerBlocked: false,
			isFeatured: request.body.isFeatured || true,
			isDeleted: false,
			isVisible: true,
			category: {
				$in: categoryArray,
			},
		};
		const data = await services.homeListingProductsAggregate(request.body.whereJSON);

		return responseHandler.successHandler(response, null, {
			data,
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.homeListing = homeListing;

//  Filter Product Listing
const filterListing = async (request, response) => {
	try {
		request.body = pageOffsetCreator(request.body);

		//	Where Creator
		let whereJSON = {
			$and: [
				{ isAdminBlocked: false },
				{ isSellerBlocked: false },
				{ isDeleted: false },
				{ isVisible: false }
			],
		};
		whereJSON = productListingCommonWhereCreator(whereJSON, request.body);// Where Creator

		const result = await Promise.all([
			productsCount(whereJSON),
			productsList(whereJSON, undefined, request.body),
		]);

		return responseHandler.successHandler(response, null, {
			// whereJSON,
			count: result[0],
			products: result[1],
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.filterListing = filterListing;
const router = express.Router();

const { buyerVerifyMiddlewareSkip } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/homeListing", buyerVerifyMiddlewareSkip, validators.homeListing, controllers.homeListing);

router.post("/filterListing", buyerVerifyMiddlewareSkip, validators.filterListing, controllers.filterListing);

module.exports = router;
const Joi = require("@hapi/joi");

const apiRef = "BuyerAuth";

const { joiValidate } = require("../../../../packages/joi");

const { genderTypes } = require("../../../../services/commonServices");

//	Register
exports.register = async (request, response, next) => {
	request.apiRef = apiRef;
  	request.apiRefFull = `${apiRef}/register`;

	const schema = Joi.object().keys({
		name: Joi.string().required(),
		email: Joi.string().email().required(),
		password: Joi.string().optional(),
		facebookId: Joi.string().optional(),
		googleId: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Social Login
exports.socialLogin = async (request, response, next) => {
  	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/socialLogin`;

	const schema = Joi.object().keys({
		facebookId: Joi.string().optional(),
		googleId: Joi.string().optional(),
  	}).or("facebookId", "googleId");

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Update Address
exports.updateAddress = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/updateAddress`;

	const schema = Joi.object().keys({
	  facebookId: Joi.string().optional(),
	  googleId: Joi.string().optional(),

	}).or("facebookId", "googleId");

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	Update Profile
exports.profileUpdate = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/profileUpdate`;
	
	let schema = Joi.object().keys({
		"name": Joi.string().required(),
		//"email": Joi.string().email().optional(),
		"gender": Joi.string().valid(genderTypes.male, genderTypes.female, genderTypes.notDisclosed).required(),
		"phoneCode": Joi.string().min(4).optional(),
		"phone": Joi.string().min(8).max(12).required(),
		"dob": Joi.date().iso().required() 
	});
  
	let valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

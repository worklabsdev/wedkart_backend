const { globalMessages } = require("../../../../config/properties/constants");

const responseHandler = require("../../../../utils/responseHandler");
const { generateRandStr } = require("../../../../utils/commonFuns");
const { generatePassword } = require("../../../../utils/password");
const { buyerMailTypes, buyerSendMailFunc } = require("../../../../utils/Emails/buyerMail");

const buyerServices  = require("../../../../services/buyerServices");

const buyerMdl = require("../../../../models/buyer");

//	Social Login
const socialLogin = async (request, response) => {
	try {
		const findJSON = {
			$and: [
				{ isDeleted: false },
			],
		};
		if (request.body.facebookId) {
			findJSON.$and.push({
				facebookId: { $ne: "" },
				facebookId: request.body.facebookId,
			});
		} else if (request.body.googleId) {
			findJSON.$and.push({
				googleId: { $ne: "" },
				googleId: request.body.googleId,
			});
		}

		let buyer = await buyerServices.buyerGetService(response, findJSON);
		if (!buyer) return responseHandler.failedActionHandler(response, buyerServices.buyerMessages.AccountNotFound);
		if (!buyer.isConfirmed) return responseHandler.failedActionHandler(response, buyerServices.buyerMessages.VerifyAccountFirst);
		if (buyer.isBlocked) return responseHandler.authHandler(response, 3);

		const token = generateRandStr(100);

		const newData = {
	  	$push: {
				token,
				logins: {
		  		token,
					valid: true,
		  		otherData: {},
				},
			},
			$set: {
				updatedAt: Date.now(),
			},
		};

		buyer = await buyerMdl.findByIdAndUpdate(buyer._id, newData, { new: true });

		return responseHandler.successHandler(response, buyerServices.buyerMessages.LogInSuccess, {
			token, name: buyer.name, email: buyer.email, _id: buyer._id,
		});
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.socialLogin = socialLogin;

//  Register Buyer
const register = async (request, response) => {
	try {
		let buyer = request.body;

		if (request.body.password) { // If Normal Signup
			const password = await generatePassword(request.body.password);

			buyer.password = password.hash;
		  	buyer.salt = password.salt;
		}// If Normal Signup

		const findJSON = {
			$and: [
				{ isDeleted: false },
			],
			$or: [
				{ email: request.body.email },
			],
		};
		if (request.body.facebookId) {
			findJSON.$or.push({
				facebookId: request.body.facebookId,
			});
		}
		if (request.body.googleId) {
			findJSON.$or.push({
				googleId: request.body.googleId,
			});
		}
		const userCheck = await buyerServices.buyerGet(findJSON);
		if (userCheck) {
			if (userCheck.email === request.body.email) return responseHandler.failedActionHandler(response, buyerServices.buyerMessages.EmailAlreadyTaken);

			if ((request.body.facebookId) && (userCheck.facebookId == request.body.facebookId)) return responseHandler.failedActionHandler(response, buyerServices.buyerMessages.FacebookAlreadyTaken);

			if ((request.body.googleId) && (userCheck.googleId == request.body.googleId)) return responseHandler.failedActionHandler(response, buyerServices.buyerMessages.GoogleAlreadyTaken);
		}

		buyer.confirmCode = generateRandStr(100);

		buyer = await buyerServices.createBuyer(buyer);
		buyer = JSON.parse(JSON.stringify(buyer));

		//	Register Email
		buyerSendMailFunc(
			buyerMailTypes.buyerRegisterEmailVerify,
			{
				email: buyer.email,
				confirmCode: buyer.confirmCode,
				reference: request.apiRefFull,
				requestId: request.requestId
			}
		);
		//	Register Email

		return responseHandler.successHandler(response, buyerServices.buyerMessages.BuyerRegisterSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.register = register;

//	Confirm Account
const confirmAccount = async (request, response) => {
	try {
		const data = {
			...request.body,
			...request.params,
		};

		if (!data.confirmCode) {
			return responseHandler.errorHandlerView(response, new Error(globalMessages.responseMessages.LinkExpired));
		}

		const buyer = await buyerMdl.findOne({
			confirmCode: data.confirmCode,
		}).lean();
		if (!buyer || buyer.isConfirmed)
			throw new Error(globalMessages.responseMessages.LinkExpired);

		await buyerServices.buyerUpdateSer(
			{_id: buyer._id},
			{
				$set: {
					confirmCode: "",
					isConfirmed: true,
					updatedAt: Date.now()					
				}
			}
		);

		return responseHandler.successHandlerView(response, buyerServices.buyerMessages.AccountVerifiedSuccess, { appName: appConfig.appName });
	} catch (error) {
		return responseHandler.errorHandlerView(response, error);
	}
};
exports.confirmAccount = confirmAccount;

//	Logout
const logout = async (request, response) => {
	try {
		const token = request.headers["x-access-token"] || request.headers.authorization || request.headers["x-auth-token"];

		await buyerMdl.update(
			{
				_id: request.buyerDetails._id,
				"logins.token": token,
			},
			{
				$set: {
					"logins.$.valid": false,
					"logins.$.socketId": null,
					"logins.$.updatedAt": Date.now(),
					updatedAt: Date.now(),
				},
				$pull: {
					token,
				},
			},
		);

		return responseHandler.successHandler(response, globalMessages.responseMessages.LogoutSuccess);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.logout = logout;

/* Profile Update function Starts here */
const profileUpdate = async (request, response) => {
	try {

		await buyerServices.buyerUpdateSer(
			{ _id: request.buyerDetails._id },
			{
				...request.body,
				updatedAt: Date.now()
			}
		);

		return responseHandler.successHandler(response, buyerServices.buyerMessages.ProfileUpdateSuccess);

		// let profileUpdateSuccessMsg = '';
		// if ( request.body.email && request.buyerDetails.email != request.body.email )
		// {			
		// 	const findJSON = { 
		// 		isDeleted: false,
		// 		email: request.body.email   
		// 	};
		// 	// Check if email is already taken
		// 	const userCheck = await buyerServices.buyerGet(findJSON);
		// 	if (userCheck)
		// 		return responseHandler.failedActionHandler(response, buyerServices.buyerMessages.EmailAlreadyTaken);  
			
		// 	const confirmCode = generateRandStr(100); 

		// 	var profile = await buyerServices.buyerUpdateSer(
		// 		{ _id: request.buyerDetails._id },
		// 		{
		// 			...request.body,
		// 			isConfirmed: false,
		// 			confirmCode: confirmCode,
		// 			token:[],
		// 			updatedAt: Date.now()
		// 		}
		// 	);

		// 	await buyerMdl.update(
		// 		{
		// 			_id: request.buyerDetails._id,
		// 			"logins.valid": true,
		// 		},
		// 		{
		// 			$set: {
		// 				"logins.$[].valid": false,						
		// 				"logins.$[].socketId": null,
		// 				"logins.$[].updatedAt": Date.now(), 
		// 				updatedAt: Date.now(),
		// 			}
		// 		},
		// 		{ multi: true }
		// 	);
		// 	profileUpdateSuccessMsg = buyerServices.buyerMessages.ProfileUpdateNewEmailSuccess;
		// 	buyerSendMailFunc(
		// 		buyerMailTypes.buyerRegisterEmailVerify,
		// 		{
		// 			email: request.body.email,
		// 			confirmCode: confirmCode,
		// 			reference: request.apiRefFull,
		// 			requestId: request.requestId
		// 		}
		// 	); 
		// }
		// else
		// {
		// 	await buyerServices.buyerUpdateSer(
		// 		{ _id: request.buyerDetails._id },
		// 		{
		// 			...request.body,
		// 			updatedAt: Date.now()
		// 		}
		// 	);
		// // }	

		// return responseHandler.successHandler(response, buyerServices.buyerMessages.ProfileUpdateSuccess);
	}
	catch(error)
	{
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.profileUpdate = profileUpdate;

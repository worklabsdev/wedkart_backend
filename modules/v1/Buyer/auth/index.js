const router = express.Router();

const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/register", validators.register, controllers.register, controllers.register);

router.post("/socialLogin", validators.socialLogin, controllers.socialLogin);

router.get("/confirmAccount/:confirmCode", controllers.confirmAccount);

router.post("/logout", buyerVerifyMiddleware, controllers.logout);

router.patch("/profileUpdate", buyerVerifyMiddleware, validators.profileUpdate, controllers.profileUpdate);

module.exports = router;

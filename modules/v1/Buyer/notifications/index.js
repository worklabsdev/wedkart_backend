const router = express.Router();

const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/list", buyerVerifyMiddleware, validators.listing, controllers.listing);

router.put("/update", buyerVerifyMiddleware, validators.notsUpdate, controllers.notsUpdate);

module.exports = router;

const responseHandler = require("../../../../utils/responseHandler");

const addressService = require("../../../../services/addressService");

//	////		Add Address
const addAddress = async (request, response) => {
	try {
		if (request.body.isDefault == "true") { //	Unfeature a address
			await addressService.updateAddressSer(
				{
					buyer: request.buyerDetails._id,
					isDefault: true,
					isDeleted: false,
				},
				{
					isDefault: false,
					updatedAt: Date.now(),
				},
			);
		}//	Unfeature a address

		request.body.buyer = request.buyerDetails._id;
		const address = await addressService.createAddressSer(request.body);

		return responseHandler.successHandler(
			response,
			addressService.addressMessagesSer.AddressCreateSuccessMsg,
			{
			// data: request.body,
				address,
			},
		);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.addAddress = addAddress;

//	////		List Addresses
const listAddress = async (request, response) => {
	try {
		request.body.whereJSON = {
			buyer: request.buyerDetails._id,
			isDeleted: false,
		};

		const addresses = await addressService.listAddressSer(request.body.whereJSON);

		return responseHandler.successHandler(
			response,
			null,
			{
			// data: request.body,
				addresses,
			},
		);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.listAddress = listAddress;

//	////		Update Address
const updateAddress = async (request, response) => {
	try {
		let address = await addressService.addressCheck(
			response,
			{
				buyer: request.buyerDetails._id,
				_id: request.body.id,
				isDeleted: false,
			},
		);

		if (request.body.isDefault == "true") { //	Unfeature a address
			await addressService.updateAddressSer(
				{
					buyer: request.buyerDetails._id,
					isDefault: true,
					isDeleted: false,
				},
				{
					isDefault: false,
					updatedAt: Date.now(),
				},
			);
		}//	Unfeature a address

		request.body.updatedAt = Date.now();
		address = await addressService.updateAddressSer(
			{
				_id: request.body.id,
			},
			request.body,
		);

		return responseHandler.successHandler(
			response,
			addressService.addressMessagesSer.AddressUpdateSuccessMsg,
			{
			// data: request.body,
				address,
			},
		);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.updateAddress = updateAddress;

//	Delete Address
const deleteAddress = async (request, response) => {
	try {
		let address = await addressService.addressCheck(
			response,
			{
				buyer: request.buyerDetails._id,
				_id: request.body.id,
				isDeleted: false,
			},
		);

		request.body.updatedAt = Date.now();
		address = await addressService.updateAddressSer(
			{
				_id: request.body.id,
			},
			{
				isDeleted: true,
				isDefault: false,
				updatedAt: Date.now(),
			},
		);

		return responseHandler.successHandler(
			response,
			addressService.addressMessagesSer.AddressDeleteSuccessMsg,
		);
	} catch (error) {
		return responseHandler.errorHandler(response, error, { data: request.body });
	}
};
exports.deleteAddress = deleteAddress;

const Joi = require("@hapi/joi");

const apiRef = "BuyerAddress";

const { joiValidate } = require("../../../../packages/joi");
const { geoJoiSchema } = require("../../../../config/properties/schemas");

//	///			Add Address
exports.addAddress = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/addAddress`;

	const schema = Joi.object().keys({
		name: Joi.string().optional(),
		address: Joi.string().required(),

		city: Joi.string().optional(),
		state: Joi.string().optional(),

		country: Joi.string().optional(),
		countryCode: Joi.string().optional(),

		phoneCode: Joi.string().optional(),
		phone: Joi.string().optional(),

		pincode: Joi.string().optional(),

		isDefault: Joi.boolean().required(),

		geo: geoJoiSchema.optional(),

		description: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			List Address
exports.listAddress = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/listAddress`;

	const schema = Joi.object().keys({
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Update Address
exports.updateAddress = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/updateAddress`;

	const schema = Joi.object().keys({

		id: Joi.string().required(),

		name: Joi.string().optional(),
		address: Joi.string().required(),

		city: Joi.string().optional(),
		state: Joi.string().optional(),

		country: Joi.string().optional(),
		countryCode: Joi.string().optional(),

		phoneCode: Joi.string().optional(),
		phone: Joi.string().optional(),

		pincode: Joi.string().optional(),

		isDefault: Joi.boolean().required(),

		geo: geoJoiSchema.optional(),

		description: Joi.string().optional(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

//	///			Delete Address
exports.deleteAddress = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/deleteAddress`;

	const schema = Joi.object().keys({
		id: Joi.string().required(),
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

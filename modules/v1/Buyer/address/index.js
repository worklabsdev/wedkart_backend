const express = require("express");

const router = express.Router();

const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/", buyerVerifyMiddleware, validators.addAddress, controllers.addAddress);

router.get("/", buyerVerifyMiddleware, validators.listAddress, controllers.listAddress);

router.patch("/", buyerVerifyMiddleware, validators.updateAddress, controllers.updateAddress);

router.delete("/", buyerVerifyMiddleware, validators.deleteAddress, controllers.deleteAddress);

module.exports = router;

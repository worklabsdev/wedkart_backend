const responseHandler = require("../../../utils/responseHandler");

const { categoryOnlyValidDetails } = require("../Admin/category/helpers");
const { categoryAggregate } = require("../Admin/category/services");

// const Db = require("../../../models");

//	////		Upload File
const socketClient = async (request, response) => {
	const result = {
		namespace: request.query.namespace || "seller",
		token: request.query.token || "t2",
	};

	return response.render("Test/socketClient", result);
};
exports.socketClient = socketClient;

//	///		Get Categories
const getCategories = async (request, response) => {
	try {
		const aggregate = await categoryOnlyValidDetails({
			allCatsFilter: "0",	// For Including Blocked Cats As Well
			allSubCatsFilter: "0",	// For Including Blocked Sub Cats as Well
			appListing: 1,
		});

		const category = await categoryAggregate(aggregate);

		return responseHandler.successHandler(response, null, { category });
	} catch (error) {
		return responseHandler.errorHandler(response, error, {});
	}
};
exports.getCategories = getCategories;

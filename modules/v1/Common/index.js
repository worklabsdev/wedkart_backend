const pathPrefix = "/api/v1/common";

app.use(`${pathPrefix}/`, require("./routes"));

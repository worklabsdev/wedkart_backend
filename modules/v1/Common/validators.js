const Joi = require("@hapi/joi");

const apiRef = "Common";

const { joiValidate } = require("../../../packages/joi");

//	///			Get Categories
exports.getCategories = async (request, response, next) => {
	request.apiRef = apiRef;
	request.apiRefFull = `${apiRef}/getCategories`;

	const schema = Joi.object().keys({
	});

	const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body);
	if (valCheck === true) next();
};

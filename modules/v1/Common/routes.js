const express = require("express");

const router = express.Router();

const controllers = require("./controllers");
const validators = require("./validators");

router.get("/socketClient", controllers.socketClient);

router.get("/getCategories", validators.getCategories, controllers.getCategories);

module.exports = router;

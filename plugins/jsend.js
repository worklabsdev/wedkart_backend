module.exports = ()=>{
    return function(req,res,next){

        res.success = function(data,message=''){
            res.json({'status':'success','data':data,'message':message})
        }
    
        res.failure = function(message='',data={}){
            res.json({'status':'failure','data':data,'message':message})
        }

        res.error = function(error, data={}, opts){
            console.log("Error", error, opts);
            res.json({'status':'error','data':data,'message':error.message})
        }

        next()
    }
} 
var mongoose = require("mongoose");
var { Schema } = mongoose;

var notificationSchema = new Schema(
    {
        admin: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "admin",
            required: false,
            default: null
        },
        buyer: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "buyers",
            required: false,
            default: null
        },
        seller: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "sellers",
            required: false,
            default: null
        },

        shop : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "shopManagements",
            required: false,
            default: null
        },
        product : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "products",
            required: false,
            default: null
        },
        inquiry : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "inquirys",
            required: false,
            default: null
        },

        category : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "categories",
            required: false,
            default: null
        },
        subCategory : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "categories.subcategories._id",
            required: false,
            default: null
        },

        payment : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "payments",
            required: false,
            default: null
        },

        message: { type: String, required: false, trim: true, default: "" },
        //description: { type: String, required: false, trim: true, default: "" },

        senderType: { type: String, required: true, trim: true },
        receiverType: { type: String, required: true, trim: true },
        notificationType: { type: String, required: true, trim: true },

        isRead: { type: Boolean, default: false },
        isDeleted: { type: Boolean, default: false },
    },
    {
        timestamps: true,
        //collection: "messages"
    }
);

const notificationModel = mongoose.model("notifications", notificationSchema);
module.exports = notificationModel;
var mongoose = require("mongoose");
var { Schema } = mongoose;

var adminSchema = new Schema(
    {
        email: { type:String, email:true, required: true, trim: true, index: true, unique: true },

        adminFee: {
            amount: { type: Number, required: true, default: 0 },
            value: { type: Number, required: true, default: 0 },
            type: { type: String, required: false, trim: true, default: "Value" },
            description: { type: String, required: false, trim: true, default: "" },
        },

        firstname: { type:String },
        lastname: { type:String, trim: true },

        password: { type: String, required: true, trim: true },
        salt: { type: String, required: true, trim: true },

        category: { type: String, required: true, ref: "categories" },
        subcategory: { type: String, required: true, ref: "subcategories" },

        logins: [
            {
                token: { type:String, required:false, default: null },
                createdAt: { type: Date, default: Date.now },
                updatedAt: { type: Date, default: Date.now },
            }
        ],
        token: { type:String, required:false, default: null },

        socketId: { type: String, default: null },
        //socketIds: { type: Array, default: [] },

        isDeleted: { type:Boolean, required:false, default: false }

    },
    {
        //collection: "admin",
        timestamps: true
    }
);

const adminModel = mongoose.model("admin", adminSchema);
module.exports = adminModel;

// adminModel.create({
// {
//     "_id" : ObjectId("5d31b15e406c8063921c3fe5"),
//     "updatedAt" : ISODate("2019-07-23T04:44:55.731Z"),
//     "createdAt" : ISODate("2019-07-19T12:02:38.926Z"),
//     "email" : "dalal.rohit2019@gmail.com",
//     "password" : "$2b$10$Oj/8My4BSfoxW3Y.XrNIFu2LUpU7v.vlYDxTjglBtxLgVwS8Gud1a",
//     "salt" : "$2b$10$Oj/8My4BSfoxW3Y.XrNIFu",
//     "firstname" : "Admin",
//     "lastname" : "Panel",
//     "category" : "category",
//     "subcategory" : "subcategory",
//     "token" : "hIYikGmCp7HVRAahgEd6pzMeA3877lSnNw1gV8m8S79m5lfc1n37aBOtQCMmuaJ3b528RWFFVaBw22Bt3oBcQH6RnuWEJ9iVivha",
//     "isDeleted" : false,
//     "__v" : 0
// }
// }, (error, result) => {
//     console.log("error, result", error, result);
// });
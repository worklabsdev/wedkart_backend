var mongoose = require("mongoose");
var { Schema } = mongoose;

var paymentSchema = new Schema(
    {

        seller: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "sellers",
            required: true,
            default: null,
            index: true,
            unique: false
        },

        product : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "products",
            required: true,
            default: null,
            index: true,
            unique: false
        },

        category: {
            type: Schema.Types.ObjectId,
            ref: "categories",
            required: true,
            index: true,
            unique: false
        },
        subCategory: {
            type: Schema.Types.ObjectId,
            required: false,
            default: null,
            index: true,
            unique: false
        },

        shop : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "shopManagements",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        buyer: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "buyers",
            required: true,
            default: null,
            index: true,
            unique: false
        },
        buyerDetails: {
            address: {
                address: { type: String, required: false, trim: true, default: "" },
                city: { type: String, required: false, trim: true, default: "" },
                state: { type: String, required: false, trim: true, default: "" },
                country: { type: String, required: false, trim: true, default: "" },
                pincode: { type: String, required: false, trim: true, default: "" },
            },
            contactDetails: {
                name: { type: String, required: false, trim: true, default: "" },
                email: { type: String, required: false, email: true, trim: true, default: "" },
                phoneCode: { type: String, required: false, trim: true, default: "+91" },
                phoneNumber: { type: String, required: false, trim: true },            
            },
            buyerDocs: {
                type: Array,
                default: []
            }
        },

        currency: { type: String, maxlength: 3, uppercase: true, required: true, default: "INR" },

        subAmount: { type: Number, required: true, default: 0 },
        subAmountComplete: { type: Number, required: true, default: 0 },
        shippingCharges: { type: Number, required: true, default: 0 },
        discount: {
            code: { type: String, required: false, trim: true, default: null },
            amount: { type: Number, required: false, default: 0 },
            type: { type: String, required: false, trim: true, default: "Value" },
        },
        taxes: new Schema({
            amount: { type: Number, required: true, default: 0 },
            types: [
                {
                    name: { type: String, required: true, trim: true, default: "" },
                    amount: { type: Number, required: true, default: 0 },
                    type: { type: String, required: false, trim: true, default: "Value" },
                    description: { type: String, required: false, trim: true, default: "" },
                }
            ]
        }),
        adminFee: new Schema({
            amount: { type: Number, required: true, default: 0 },
            value: { type: Number, required: true, default: 0 },
            type: { type: String, required: false, trim: true, default: "Value" },
            description: { type: String, required: false, trim: true, default: "" }
        }),

        sellerAmount: new Schema({
            amount: { type: Schema.Types.Number, required: true, default: 0 },
            status: { type: Schema.Types.String, required: false, default: "NotRequired" },
            details: { type: Schema.Types.Mixed, default: {} },
            description: { type: String, required: false, trim: true, default: "" },
            odata: { type: Schema.Types.Mixed, default: {} }
        }),
        totalAmount: { type: Number, required: true, default: 0 },
        
        refund: {
            amount: { type: Number, required: true, default: 0 },
            status: { type: String, required: false, default: "NotRequired" },
            refundId: { type: String, required: false, default: null }            
        },

        paymentStatus: { type: String, required: false, default: "Pending" },
        paymentMethod: { type: String, required: false , default: "Cash" },
        paymentDetails: { type: Schema.Types.Mixed, default: {} },
        paymentError: {
            code: { type: String, required: false , default: "" },
            description: { type: String, required: false , default: "" },
            odata: { type: Schema.Types.Mixed, default: {} }
        },

        gatewayDetails: {
            "razorPay": {
                orderId: { type: String, index: true, default: null, trim: true },
                customerId: { type: String, default: null, trim: true },
                paymentId: { type: String, index: true, default: "", trim: true },
                invoiceId: { type: String, default: "", trim: true },
                odata: { type: Schema.Types.Mixed, default: {} }
            }
        },
        gatewayFee: { type: Number, default: 0 },

        orderId: { type: String, index: true, unique: true }, 

        startDt: { type: Date, default: Date.now },
        endDt: { type: Date, default: Date.now },
        startEndDiffEnd: { type: Number, default: 1, },//Number of Months

        isDeleted: { type: Boolean, default: false },
        isBlocked: { type: Boolean, default: false },

        gatewayLogs: {
            type: [
                {
                    entity: { type: String, required: true },
                    request: { type: Schema.Types.Mixed, default: {} },
                    response: { type: Schema.Types.Mixed, default: {} },
                    odata: { type: Schema.Types.Mixed, default: {} }
                }
            ],
            default: []
        },

    },
    { 
        timestamps: true,
        //collection: "messages"
    }
);

const paymentModel = mongoose.model("payments", paymentSchema);
module.exports = paymentModel;
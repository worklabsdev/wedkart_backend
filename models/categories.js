var mongoose = require("mongoose");
var { Schema } = mongoose;

var categorySchema = new Schema(
    {
        name: { type: String, required: true, trim: true },
        order: { type: Number },
        description: { type: String, trim: true },
        
        subcategories: [
            new Schema({
                name: { type: String, required: true, trim: true },
                isDeleted: { type: Boolean, trim: true, default: false },
                isBlocked: { type: Boolean, trim: true, default: false },
                createdAt: { type: Date, default: Date.now },
                updatedAt: { type: Date, default: Date.now }
            })
        ],

        creator: {
            type: { type: String,  trim: true, default: "Self" },
            admin: { 
                type: mongoose.Schema.Types.ObjectId,
                ref: "admin",
                required: false,
                default: null
            }
        },
        

        isDeleted: { type: Boolean, trim: true, default: false },
        isBlocked: { type: Boolean, trim: true, default: false },
        
        commission: { type: Number, default: 0 }
    }, 
    {
        timestamps: true,
        //collection: "categories" 
    }
);

const categoryModel = mongoose.model("categories", categorySchema);
module.exports = categoryModel;
var mongoose = require("mongoose");
var { Schema } = mongoose;

var mongoose = require("mongoose");
var { Schema } = mongoose;

var inquirySchema = new Schema(
  {
        buyer: {
            type: Schema.Types.ObjectId,
            ref: "buyers",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        seller: {
            type: Schema.Types.ObjectId,
            ref: "sellers",
            required: true,
            index: true,
            unique: false
        },

        shop: {
            type: Schema.Types.ObjectId,
            ref: "shops",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        product: {
            type: Schema.Types.ObjectId,
            ref: "products",
            required: true,
            index: true,
            unique: false
        },

        category: {
            type: Schema.Types.ObjectId,
            ref: "categories",
            required: true,
            index: true,
            unique: false
        },
        subCategory: {
            type: Schema.Types.ObjectId,
            required: false,
            default: null,
            index: true,
            unique: false
        },

        name: { type: String, required: false, trim: true },
        email: { type: String, required: false, email: true, trim: true },

        phoneCode: { type: String, required: false, trim: true, default: "+91" },
        phoneNumber: { type: String, required: false, trim: true },

        jwtToken: { type: String, index: true, unique: false, required: false, default:null },

        description: { type: String, required: false, trim: true, default: "" },

        otp: {
            otp: { type: String, required: false, trim: true },
            otpValidity: { type: Date, required: false, default:null },
            otpVerified: { type: Boolean, default: false }
        },

        response: { type: Array, default: [] },
        
        isAdminBlocked: { type: Boolean, default: false, trim: true },
        isSellerDeleted: { type: Boolean, default: false, trim: true },
        isBuyerDeleted: { type: Boolean, default: false, trim: true },

        creatorAdmin: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "admin",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        creatorBuyer: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "buyers",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        creatorSeller: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "sellers",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        otherData: { type: Schema.Types.Mixed, default: {} },
        
        status: { type: String, required: true, default: "otpPending" }

  }, 
  {
    timestamps: true,
    //collection: "orders"
  }
);

const inquiryModel = mongoose.model("inquirys", inquirySchema);
module.exports = inquiryModel;
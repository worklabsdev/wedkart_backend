var mongoose = require("mongoose");
var { Schema } = mongoose;

const { geoMongooseSchema } = require("../config/properties/schemas");

var addressSchema = new Schema(
    {
        name: { type: String, required: false, trim: true, default: null },

        buyer: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "buyers",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        seller: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "sellers",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        address: { type: String, required: false, trim: true, default: null },

        city: { type: String, required: false, trim: true, default: null },

        state: { type: String, required: false, trim: true, default: null },

        country: { type: String, required: false, trim: true, default: null },
        countryCode: { type: String, required: false, trim: true, default: null },

        phoneCode: { type: String, required: false, trim: true, default: "+91" },
        phone: { type: String, required: false, trim: true, default: null },

        pincode: { type: String, required: false, trim: true, default: null },

		geo: {
			type: geoMongooseSchema,
			required: false
		},

        description: { type: String, default: null },
        
        isDefault: { type: Boolean, required: true, default: false },

        isDeleted: { type: Boolean, required: true, default: false },
    }, 
    {
        timestamps: true,
        //collection: "buyers" 
    }
);

const buyerModel = mongoose.model("address", addressSchema);
module.exports = buyerModel;
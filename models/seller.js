var mongoose = require("mongoose");
var { Schema } = mongoose;

const { geoMongooseSchema } = require("../config/properties/schemas");

var sellerSchema = new Schema(
    {
        name: { type: String, required: true, trim: true },

        facebookId: { type: String, default: "", index: true },
        googleId: { type: String, default: "", index: true },

        email: { type: String, required: true, trim: true, index: true, email: true, unique: false },

        password: { type: String, required: false, trim: true },
        salt: { type: String, required: false, trim: true },

        phone: { type: String, required: false, trim: true },
        image: { type: String, required: false, trim: true},
        confirmCode: { type: String, required: false },
        recoverCode: { type: String, required: false },
        token: { type: Array, required: false , default:[] },
        address: { type: String, required: false, trim: true },
        city: { type: String, required: false, trim: true },
        state: { type: String, required: false, trim: true },
        country: { type: String, required: false, trim: true },
        pincode: { type: String, required: false, trim: true },

        base64: { type: String,  trim: true },

        gender: { type: String,  trim: true, default: null },

        logins: [
            {
                token: { type: String, required: false, default: null },
                valid: { type: Boolean, required: true, trim: true, default: true },
                otherData: { type: Schema.Types.Mixed, default: {} },
                socketId: { type: String, default: null },
                //socketIds: { type: Array, default: [] },
                createdAt: { type: Date, default: Date.now },
                updatedAt: { type: Date, default: Date.now },
            }
        ],
        token: { type: Array, required: false , default:[] },

        creator: {
            type: { type: String,  trim: true, default: "Self" },
            admin: { 
                type: mongoose.Schema.Types.ObjectId,
                ref: "admin",
                required: false,
                default: null
            }
        },

        socketIds: { type: Array, default: [] },

		bussiness: {
			type: Schema.Types.Mixed,
			default: {
                name: null,
                email: null,

                phoneCode: null,
                phoneNumber: null,

                description: null,
                ownerName: null,
                
                panNumber: null,
                personalNumber: null,
                licenceNumber: null,
                gstNumber: null,

                startTime: null,
                endTime: null,

                banner: null,

                productVisible: true,

                address: null,
                city: null,
                state: null,
                country: null,
                countryCode: null,
                pincode: null,
                // geo: {
                //     type: geoMongooseSchema,
                //     required: false
                // },
			}
        },
        bookingFile: { type: String, default: null },

        isApproved: { type: Boolean, default: false },
        isConfirmed: { type: Boolean, default: false },
        isBlocked: { type: Boolean, default: false },

        isDeleted: { type: Boolean, default: false },
    },
    {
        timestamps: true,
        //collection: "sellers"
    }
);


sellerSchema.statics.findByToken = function (token) {
    return this.findOne({ "token": {$in:[token]}, isDeleted:false }).lean();
}
sellerSchema.statics.newFindByToken = function (token) {
    return this.findOne({ "logins.token": token, isDeleted: false }).lean();
}

sellerSchema.statics.findByMail = function (email, phone) {
    return this.findOne({ "email": new RegExp(email, "i"), isDeleted:false }).lean();
}

sellerSchema.statics.findExistence = function (ep, password) {
    return this.findOne(
        {
            // $or: [
            //     { "email": ep },
            //     { "phone": ep }
            // ],
            email: ep,
            isDeleted: false
        }
    ).lean();
}

sellerSchema.statics.removeimage=function(id,url){
    this.update({_id:id},{$set:{"image":url}}).exec(function(err,data){
    if(err) throw err;
    });
}

const sellerModel = mongoose.model("sellers", sellerSchema);
module.exports = sellerModel;
var mongoose = require("mongoose");
var { Schema } = mongoose;

var orderSchema = new Schema(
  {
    sellerId: { type: String, required: true, trim: true },
    buyerId: { type: String, required: true, trim: true },
    address: { type: String, required: false, trim: true },
    city: { type: String, required: false, trim: true },
    state: { type: String, required: false, trim: true },
    country: { type: String, required: false, trim: true },
    pincode: { type: String, required: false, trim: true },
    contact_no: { type: String, required: false , default:null },
    contact_name: { type: String, required: false , default:null },
    sub_amount:{ type: Number, required: false , default:null },
    mode_of_payment:{ type: String, required: false , default:null },
    shipping_charges:{ type: Number, required: false , default:null },
    total_amount:{ type: Number, required: false , default:null },
    status:{type: String, required: false , default:"Pending"},
    reviewed:{type: Boolean, required: false , default:false },
    products:[{
        product_id:{type:Schema.Types.ObjectId , ref:"products"},
        quantity:{type:Number},
        size:{type:String},
        color:{type:String},
    }]
  }, 
  {
    timestamps: true,
    //collection: "orders"
  }
);

const orderModel = mongoose.model("orders", orderSchema);
module.exports = orderModel;
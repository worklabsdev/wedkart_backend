var mongoose = require("mongoose");
var { Schema } = mongoose;

var buyerSchema = new Schema(
    {
        name: { type: String, required: true, trim: true },

        facebookId: { type: String, default: "", index: true },
        googleId: { type: String, default: "", index: true },

        email: { type: String, required: true, trim: true, index: true, email: true, unique: false },

        password: { type: String, required: false, trim: true },
        salt: { type: String, required: false, trim: true },

        phoneCode: { type: String, required: false, trim: true, default: "+91" },
        phone: { type: String, required: false, trim: true },

        image: { type: String, required: false, trim: true },
        confirmCode: { type: String, required: false, default: "" },
        recoverCode: { type: String, required: false, default: "" },

        gender: { type: String,  trim: true, default: null },

        cart:[{
            product_id:{ type: Schema.ObjectId , ref:"products" },
            quantity:{ type: Number },
            size:{ type: String },
            color:{ type: String },
        }],
        wishlist:[{
            product_id:{ type: Schema.ObjectId , ref:"products" },
            quantity:{ type: Number },
            size:{ type: String },
            color:{ type: String },
        }],

        logins: [
            {
                token: { type: String, required: false, default: null },
                valid: { type: Boolean, required: true, trim: true, default: true },
                otherData: { type: Schema.Types.Mixed, default: {} },
                socketId: { type: String, default: null },
                //socketIds: { type: Array, default: [] },
                createdAt: { type: Date, default: Date.now },
                updatedAt: { type: Date, default: Date.now },
            }
        ],
        token: { type: Array, required: false , default:[] },

        creator: {
            type: { type: String,  trim: true, default: "Self" },
            admin: { 
                type: mongoose.Schema.Types.ObjectId,
                ref: "admin",
                required: false,
                default: null
            },
            otherData: { type: Schema.Types.Mixed, default: {} },
        },
        dob: { type: String, trim: true, default: null },

        isApproved: { type: Boolean, default: false },
        isConfirmed: { type: Boolean, default: false },
        isBlocked: { type: Boolean, default: false },
        isDeleted: { type: Boolean, required: true }
    }, 
    {
        timestamps: true,
        //collection: "buyers" 
    }
);

buyerSchema.statics.findByToken = function (token) {
    return this.findOne({ "token": {$in: [token]}, isDeleted: false }).lean();
}
buyerSchema.statics.newFindByToken = function (token) {
    return this.findOne({ "token": {$in: [token]}, isDeleted: false }, { token: 0, logins: 0, wishlist: 0, cart: 0 }).lean();
}

buyerSchema.statics.findByMail = function (email, phone) {
    return this.findOne({ "email": new RegExp(email, "i"), isDeleted: false }).lean();
}

buyerSchema.statics.findExistence = function (ep) {
    return this.findOne(
        {
            // $or: [
            //     { "email": ep },
            //     { "phone": ep }
            // ],
            email: ep,
            isDeleted: false
        }
    ).lean();
}

buyerSchema.statics.removeimage = (id,url) => {
    this.update({_id:id},{$set:{image: url}}).exec(function(err,data){
        if(err) throw err;
    });
};

const buyerModel = mongoose.model("buyers", buyerSchema);
module.exports = buyerModel;
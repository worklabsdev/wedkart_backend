var mongoose = require("mongoose");
var { Schema } = mongoose;

var msgSchema = new Schema(
    {
        buyerId: {type: mongoose.Schema.Types.ObjectId, ref: "buyers"},
        productId : {type:String, required:true},
        sellerId: {type:String, required:true},
        message: { type: String, required: false, trim: true },
        sender: {type: String, required:true, default:"buyer"}
    },
    { 
        timestamps: true,
        //collection: "messages"
    }
);

const messageModel = mongoose.model("messages", msgSchema);
module.exports = messageModel;
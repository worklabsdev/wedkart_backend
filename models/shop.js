var mongoose = require("mongoose");
var { Schema } = mongoose;

var shopSchema = new Schema(
    {
        name: { type: String, required: true, trim: true },
        email: { type: String, required: true, trim: true },
        description: { type: String, required: true },
        banner: { type: String, required: false, trim: true },
        status: { type: String, required: false, trim: true},
        address: { type: String, required: false },
        country: { type: String, required: false },
        state: { type: String, required: false },
        postal_code: { type: String, required: false, trim: true },
        city: { type: String, required: false, trim: true },
        license_no: { type: String, required: false, trim: true },
        gst_no: { type: String, required: false, trim: true },
        contact_no: { type: String, required: true, trim: true },
        mainImage: { type: String, required: false, trim: true },
        images: { type: Array, required: false, trim: true, default: [] },
        user_id: { type: String, required: false, trim: true },
        shopManagementPic: { type: Array, required: false, trim: true, default: [] },
        id: { type: String, required: false, trim: true },
        // shop_certificate_incorporation: { type: Array, required: false, default: [] },
        // gst_certificate: { type: Array, required: true, trim: true, default: [] },
        // cancel_check: { type: Array, required: true, trim: true, default: [] },
        // shop_logo: { type: Array, required: true, trim: true, default: [] },
        pancard_no: { type: String, required: true, trim: true},
        // pancard: { type: Array, required: true, trim: true, default: [] },
        shopkeeper_name: { type: String, required: true, trim: true },
        shopkeeper_no: { type: String, required: true, trim: true },
        open_time: { type: String, required: true, trim: true, },
        close_time: { type: String, required: true, trim: true, },

    }, 
    { 
        timestamps: true,
        //collection: "shopManagements"
    }
);

shopSchema.statics.findByToken = function (token) {
    return this.findOne({ "token": {$in:[token]}, isDeleted:false }).lean();
}

shopSchema.statics.findByMail = function (license_no) {
    return this.findOne({ "license_no": new RegExp(license_no, "i"), isDeleted:false }).lean();
}

shopSchema.statics.addImage = function (id, url) {
    this.update({ "_id": id }, { $push: { "images": url } }).exec(function (err, data) {
        if (err) throw err;
    })

}

// shopSchema.statics.findExistence = function (ep, password) {
//     return this.findOne(
//         {
//             $or: [
//                 { "email": new RegExp(ep, "i"), "password": password },
//                 { "phone": new RegExp(ep, "i"), "password": password }
//             ],
//             isDeleted:false
//         }
//     ).lean();
// }

shopSchema.statics.removeimage=function(id,url){
 this.update({_id: id},{$set:{image: url}}).exec(function(err,data){
   if(err) throw err;
 })
}

const shopModel = mongoose.model("shopManagements", shopSchema);
module.exports = shopModel;
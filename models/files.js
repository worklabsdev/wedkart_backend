var mongoose = require("mongoose");
var { Schema } = mongoose;

var fileSchema = new Schema(
    {
        admin: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "admin",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        creatorAdmin: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "admin",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        buyer: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "buyers",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        creatorBuyer: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "buyers",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        shop : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "shopManagements",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        product : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "products",
            required: false,
            default: null,
            index: true,
            unique: false
        },

        seller: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "sellers",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        creatorSeller: { 
            type: mongoose.Schema.Types.ObjectId,
            ref: "sellers",
            required: false,
            default: null,
            index: true,
            unique: false
        },
        
        file: { type: String, required: true, trim: true },
        fileType: { type: String, required: true, trim: true },
        fileExt: { type: String, required: true, trim: true },
        fileSize: { type: Number, required: true, default: 0 },

        title: { type: String, required: false, trim: true, default: "" },
        description: { type: String, required: false, trim: true, default: "" },

        type: { type: String, index: true, default: "Normal" },

        isDeleted: { type: Boolean, default: false },
        isBlocked: { type: Boolean, default: false }
    },
    { 
        timestamps: true,
        //collection: "messages"
    }
);

const fileModel = mongoose.model("files", fileSchema);
module.exports = fileModel;
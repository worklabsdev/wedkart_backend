module.exports = {
    admin: require("./admin"),
    buyer: require("./buyer"),
    categories: require("./categories"),
    files: require("./files"),
    inquirys: require("./inquirys"),
    messages: require("./messages"),
    order: require("./order"),
    product: require("./product"),
    reviews: require("./reviews"),
    seller: require("./seller"),
    shop: require("./shop"),
    subcategories: require("./subcategories")
};
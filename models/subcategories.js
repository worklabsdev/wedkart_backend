var mongoose = require("mongoose");
var { Schema } = mongoose;

var subCategorySchema = new Schema(
    {
        name: {type:String, required: true, trim: true},
        cat_id: {type:Schema.Types.ObjectId,  trim: true},
        status: {type:Boolean, trim: true, default:true},
        is_deleted: {type:Boolean, trim: true, default:false}
    },
    {
        timestamps: true,
        collection: "subcategories"
    }
);

const subCategoryModel = mongoose.model("subcategories", subCategorySchema);
module.exports = subCategoryModel;
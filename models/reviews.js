var mongoose = require("mongoose");
var { Schema } = mongoose;

var reviewSchema = new Schema(
    {
        product_id: { type: Schema.Types.ObjectId, required: true, trim: true },
        buyerId: { type: Schema.Types.ObjectId, required: true, trim: true, ref:"buyers" },
        title: { type: String, required: false, trim: true },
        message: { type: String, required: false, trim: true },
        star: { type: String, required: false, trim: true },
    }, 
    { 
        timestamps: true,
        //collection: "reviews"
    }
);

const reviewModel = mongoose.model("reviews", reviewSchema);
module.exports = reviewModel;
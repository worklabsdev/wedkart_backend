var mongoose = require("mongoose");
var { Schema } = mongoose;

var productSchema = new Schema(
    {
        name: { type: String, required: true, trim: true },
        gender: { type: String, trim: true },

        slug: { type: String, trim: true },
        replicateId: { type: String, trim: true },

        mainImage: { type: String, required: false, trim: true },

        aprice: { type: Number, required: true, default: 0.0, min: 0 },
        dprice: { type: Number, required: true, default: 0.0, min: 0 },

        description: { type: String, required: true },

        size: { type: Array, required: false, trim: true, default: [] },
        color: { type: Array, required: false, trim: true, default: [] },
        service: { type: Array, required: false, trim: true, default: [] },
        images: { type: Array, required: false, trim: true, default: [] },
        specifications: { type: Array, required: false, trim: true, default: [] },

        quantity: { type: Number, required: true, trim: true, min: 0 },
        quantityType: { type: String, required: true, trim: true },

        category: {
            type: Schema.Types.ObjectId,
            ref: "categories",
            required: true
        },
        subCategory: {
            type: Schema.Types.ObjectId,
            ref: "categories.subcategories",
            required: false,
            default: null
        },

        //base64 : { type: String, trim: true  },

        seller: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "sellers"
            },
            email: { type: String, required: true, trim: true }
        },

        buttons: {
            enquiry: {
                enabled: { type: Boolean, required: true, trim: true, default: false } 
            },
            buyNow: {
                enabled: { type: Boolean, required: true, trim: true, default: false },
                amount: { type: Number, required: true, default: 0.0 }
            },
            payToken: {
                enabled: { type: Boolean, required: true, trim: true, default: false },
                amount: { type: Number, required: true, default: 0.0 }
            }
        },

        isVisible: { type: Boolean, default: true },

        isFeatured: { type: Boolean, default: false },
        isApproved: { type: Boolean, default: false },

        isAdminBlocked: { type: Boolean, default: false },
        isSellerBlocked: { type: Boolean, default: false },

        isDeleted: { type: Boolean, default: false },
    },
    { 
        timestamps: true
    }
);

productSchema.statics.addImage = (id, url) => {
    this.update({ "_id": id }, { $push: { "images": url } }).exec(function (err, data) {
        if (err) throw err;
    });
}

const productModel = mongoose.model("products", productSchema);
module.exports = productModel;
require("dotenv").config();

appConfig = require(`./config/envs/${process.env.NODE_ENV}`);// App Config

express = require("express");
const bodyparser = require("body-parser");
const expressValidator = require("express-validator");
const cors = require("cors");
const multipart = require("connect-multiparty");
const helmet = require("helmet");
const uuidv4 = require('uuid/v4');
const logger = require("morgan");
const path = require("path");
var multer = require("multer");
var jsend = require("./plugins/jsend");

ObjectId = require("mongoose").Types.ObjectId;

app = express();

require("./packages");//i18 Other Necessary Packages
multipartMiddleware = multipart();

var upload = multer({dest: "uploads/"});
const router = require("./routes");

require("./config/database");//     Db

// middlewares
app.use(helmet());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(cors());
app.use(logger("dev"));//   Logs Basics
app.use(bodyparser.json({limit: "500mb"}));
app.use(bodyparser.urlencoded({ extended:true, limit: "500mb", parameterLimit:50000 }));
app.use(expressValidator());
app.use(jsend());
app.use("/uploads", express.static("uploads"));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
	req.requestId = uuidv4();

	res["x-api-request-reference"] = req.requestId;
    next();
});

app.use("/", router);// Old Routes

require("./modules/v1/");


// catch 404 and forward to error handler
const { pageNotFound, errorHandler } = require("./utils/responseHandler");

app.use((req, res, next) => {
	return pageNotFound(res);
});
// Error Handler
app.use((err, req, res, next) => {
	return errorHandler(res, err);
});

module.exports = app;